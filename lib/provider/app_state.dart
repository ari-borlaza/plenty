import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:plenty/Network/network_connection.dart';
import 'package:plenty/model/food_category.dart';
import 'package:plenty/model/user_model.dart';
//import 'package:shared_preferences/shared_preferences.dart';

class AppState extends ChangeNotifier {
  UserModel _model;
  int selectedAddress;

  List<MainCategories> mainCategories;
  bool _isLoading = true;

  AppState() {
    selectedAddress = -1;
    mainCategories = [];
    fetchMaiNCategories();
  }

  setIndex(int val) {
    selectedAddress = val;
    notifyListeners();
  }

  clearIndex() {
    selectedAddress = -1;
    notifyListeners();
  }

  UserModel get model => _model;

  set model(UserModel value) {
    _model = value;
  }

  bool get isLoading => _isLoading;

  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  void fetchMaiNCategories() async {
    Response res = await AppApi.instance.apiGet("categories");
    if (res.statusCode == 200) {
      for (Map<String, dynamic> data in res.data) {
        mainCategories.add(MainCategories.fromJson(data));
      }
      isLoading = false;
    }
    /*   final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey("token")) {
      Response res = await AppApi.instance.apiGet("categories");
      if (res.statusCode == 200) {
        for (Map<String, dynamic> data in res.data) {
          mainCategories.add(MainCategories.fromJson(data));
        }
        isLoading = false;
      }
    } */
  }
}
