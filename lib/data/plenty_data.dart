import 'package:flutter/material.dart';

class PlentyData {
  List<PlentyModel> plentyList;

  PlentyData() {
    plentyList = List();
    plentyList.add(
      PlentyModel(
          id: 0,
          category: 'Fine Dining',
          description:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit. Ipsum a arcu cursus vitae congue mauris rhoncus aenean vel.",
          image: "assets/image/dine.png",
          image1: 'assets/image/dinetab_6sec.gif',
          video: 'assets/videos/dine_compressed.mp4'),
    );
    plentyList.add(
      PlentyModel(
          id: 1,
          category: 'Beauty',
          description:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit. Ipsum a arcu cursus vitae congue mauris rhoncus aenean vel.",
          image: "assets/image/beauty.png",
          image1: 'assets/image/beautytab_6sec.gif',
          video: 'assets/videos/beauty_compressed.mp4'),
    );
    plentyList.add(
      PlentyModel(
          id: 2,
          category: 'Fashion',
          description:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis convallis tellus id interdum velit. Ipsum a arcu cursus vitae congue mauris rhoncus aenean vel.",
          image: "assets/image/fashion.png",
          image1: 'assets/image/fashiontab_6sec.gif',
          video: 'assets/videos/fashion_compressed.mp4'),
    );
  }
}

class PlentyModel {
  int id;
  String restoname;
  String category;
  List<String> genre;
  double rating;
  String description;
  String image;
  String imageText;
  String videoClipPath;
  String videoClipReflectionPath;
  List<PlentyRestModel> restList = List();
  List<PlentyItemModel> itemList = List();

  String image1;
  String video;

  PlentyModel({
    this.id,
    this.genre,
    this.restoname,
    this.category,
    this.rating,
    this.description,
    this.image,
    this.imageText,
    this.restList,
    this.videoClipPath,
    this.videoClipReflectionPath,
    this.itemList,
    this.image1,
    this.video,
  });
}

class PlentyRestModel {
  String restoname;
  Image photo;

  PlentyRestModel({this.restoname, this.photo, List<PlentyItemModel> itemList});
}

class PlentyItemModel {
  String itemname;
  Image photo;

  PlentyItemModel({this.itemname, this.photo});
}

final List<Map<String, dynamic>> deliveryAddress = [
  {
    "index": 0,
    "address": [
      {
        "num": 0,
        "country": "KSA",
        "city": "Riyadh",
        "address": "Address Here 1",
        "contact_number": "05012345671"
      },
      {
        "num": 1,
        "country": "KSA",
        "city": "Jiddah",
        "address": "Address Here 2",
        "contact_number": "05012345672"
      },
    ],
  },
];
