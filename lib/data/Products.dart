class Product {
  int id;
  bool isactive;
  int price;
  bool hasoffer;
  bool ispercentage;
  int offerPrice;
  String weight;
  int onstock;
  String extra;
  int superId;
  int categoryId;
  int shopId;
  bool special;
  bool bestseller;
  String rating;
  int popularity;
  String createdAt;
  String updatedAt;
  String colors;
  String sizes;
  String name;
  String description;
  int sales;
  List<Images> images;
  Shop shop;
  List<Map<String, dynamic>> properties = [];

  Product(
      {this.id,
      this.isactive,
      this.price,
      this.hasoffer,
      this.ispercentage,
      this.offerPrice,
      this.weight,
      this.onstock,
      this.extra,
      this.superId,
      this.categoryId,
      this.shopId,
      this.special,
      this.bestseller,
      this.rating,
      this.popularity,
      this.createdAt,
      this.updatedAt,
      this.colors,
      this.sizes,
      this.name,
      this.description,
      this.sales,
      this.images,
      this.shop});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    isactive = json['isactive'];
    price = json['price'];
    hasoffer = json['hasoffer'];
    ispercentage = json['ispercentage'];
    offerPrice = json['offer_price'];
    weight = json['weight'];
    onstock = json['onstock'];
    extra = json['extra'];
    superId = json['super_id'];
    categoryId = json['category_id'];
    shopId = json['shop_id'];
    special = json['special'];
    bestseller = json['bestseller'];
    rating = json['rating'];
    popularity = json['popularity'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    colors = json['colors'];
    sizes = json['sizes'];
    name = json['name'];
    description = json['description'];
    sales = json['sales'];
    if (json['images'] != null) {
      images = new List<Images>();
      json['images'].forEach((v) {
        images.add(new Images.fromJson(v));
      });
    }

    if (json['extra'] != null) {
      final temp = (json['extra'] as String).split(",");
      temp.forEach((v) {
        final p = v.split(":");
        properties.add({p.first: p.last.replaceAll(";", ",")});
      });
    }
    shop = json['shop'] != null ? new Shop.fromJson(json['shop']) : null;
    if (hasoffer) {
      price = offerPrice;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['isactive'] = this.isactive;
    data['price'] = this.price;
    data['hasoffer'] = this.hasoffer;
    data['ispercentage'] = this.ispercentage;
    data['offer_price'] = this.offerPrice;
    data['weight'] = this.weight;
    data['onstock'] = this.onstock;
    data['extra'] = this.extra;
    data['super_id'] = this.superId;
    data['category_id'] = this.categoryId;
    data['shop_id'] = this.shopId;
    data['special'] = this.special;
    data['bestseller'] = this.bestseller;
    data['rating'] = this.rating;
    data['popularity'] = this.popularity;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['colors'] = this.colors;
    data['sizes'] = this.sizes;
    data['name'] = this.name;
    data['description'] = this.description;
    data['sales'] = this.sales;
    if (this.images != null) {
      data['images'] = this.images.map((v) => v.toJson()).toList();
    }
    if (this.shop != null) {
      data['shop'] = this.shop.toJson();
    }
    return data;
  }
}

class Images {
  int id;
  String url;
  String fullurl;

  Images({this.id, this.url, this.fullurl});

  Images.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    url = json['url'];
    fullurl = json['fullurl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['url'] = this.url;
    data['fullurl'] = this.fullurl;
    return data;
  }
}

class Shop {
  int id;
  String shopCoverImage;
  String shopTel;
  String shopMob;
  String shopEmail;
  String shopWebsite;
  int status;
  String tax;
  bool verified;
  bool featured;
  bool other;
  int productsCount;
  String shopName;
  String shopAddress;
  String fullurl;
  Emirate emirate;

  Shop(
      {this.id,
      this.shopCoverImage,
      this.shopTel,
      this.shopMob,
      this.shopEmail,
      this.shopWebsite,
      this.status,
      this.tax,
      this.verified,
      this.featured,
      this.other,
      this.productsCount,
      this.shopName,
      this.shopAddress,
      this.fullurl,
      this.emirate});

  Shop.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shopCoverImage = json['shop_coverImage'];
    shopTel = json['shop_tel'];
    shopMob = json['shop_mob'];
    shopEmail = json['shop_email'];
    shopWebsite = json['shop_website'];
    status = json['status'];
    tax = json['tax'];
    verified = json['verified'];
    featured = json['featured'];
    other = json['other'];
    productsCount = json['products_count'];
    shopName = json['shop_name'];
    shopAddress = json['shop_address'];
    fullurl = json['fullurl'];
    emirate =
        json['emirate'] != null ? new Emirate.fromJson(json['emirate']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['shop_coverImage'] = this.shopCoverImage;
    data['shop_tel'] = this.shopTel;
    data['shop_mob'] = this.shopMob;
    data['shop_email'] = this.shopEmail;
    data['shop_website'] = this.shopWebsite;
    data['status'] = this.status;
    data['tax'] = this.tax;
    data['verified'] = this.verified;
    data['featured'] = this.featured;
    data['other'] = this.other;
    data['products_count'] = this.productsCount;
    data['shop_name'] = this.shopName;
    data['shop_address'] = this.shopAddress;
    data['fullurl'] = this.fullurl;
    if (this.emirate != null) {
      data['emirate'] = this.emirate.toJson();
    }
    return data;
  }
}

class Emirate {
  int id;
  String emirateEnName;
  String emirateArName;

  Emirate({this.id, this.emirateEnName, this.emirateArName});

  Emirate.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    emirateEnName = json['emirate_en_name'];
    emirateArName = json['emirate_ar_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['emirate_en_name'] = this.emirateEnName;
    data['emirate_ar_name'] = this.emirateArName;
    return data;
  }
}

class Slider {
  int id;
  String url;
  int superId;
  int pModelsId;
  int categoryId;
  int shopId;
  int hasoffer;
  String location;
  String fullurl;

  Slider(
      {this.id,
      this.url,
      this.superId,
      this.pModelsId,
      this.categoryId,
      this.shopId,
      this.hasoffer,
      this.location,
      this.fullurl});

  Slider.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    url = json['url'];
    superId = json['super_id'];
    pModelsId = json['p_models_id'];
    categoryId = json['category_id'];
    shopId = json['shop_id'];
    hasoffer = json['hasoffer'];
    location = json['location'];
    fullurl = json['fullurl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['url'] = this.url;
    data['super_id'] = this.superId;
    data['p_models_id'] = this.pModelsId;
    data['category_id'] = this.categoryId;
    data['shop_id'] = this.shopId;
    data['hasoffer'] = this.hasoffer;
    data['location'] = this.location;
    data['fullurl'] = this.fullurl;
    return data;
  }
}
