import 'package:plenty/config/colors.dart';

final List<dynamic> imgListVertFineDining = [
  'assets/brandsposter/sada2.png',
  'assets/brandsposter/goka sakana2.png',
  'assets/brandsposter/hypen2.png',
  'assets/brandsposter/Linen2.png',
  'assets/brandsposter/mikroulii2.png',
  'assets/brandsposter/goka sakana2.png',
  'assets/brandsposter/Perimeter2.png',
  'assets/brandsposter/Portion2.png',
  'assets/brandsposter/solid2.png',
];
final List<dynamic> imgListVertBeauty = [
  'assets/brandsposter/Skirts.png',
  'assets/brandsposter/manners2.png',
  'assets/brandsposter/noise2.png',
];
final List<dynamic> imgListVertFashion = [
  'assets/brandsposter/design ala carte2.png',
  'assets/brandsposter/Rendezvous2.png',
];

final List<dynamic> gifFineDining = [
  'assets/videos/linen_compressed.mp4',
  'assets/videos/sada_compressed.mp4',
  'assets/videos/dine_compressed.mp4',
  'assets/videos/mikro_compressed.mp4',
  'assets/videos/goka_compressed.mp4',
  'assets/videos/perimeter_compressed.mp4',
  'assets/videos/hyphen_sompressed.mp4',
  'assets/videos/portion_compressed.mp4',
  'assets/videos/solid_compressed.mp4',
];
final List<dynamic> gifBeauty = [
  'assets/videos/skirts_compressed.mp4',
  'assets/videos/manners_compressed.mp4',
  'assets/videos/noise_compressed.mp4',
];
final List<dynamic> gifFashion = [
  'assets/videos/Design_compressed.mp4',
  'assets/videos/rendezvous_compressed.mp4',
];
final List<Map<String, List<MostPopularItems>>> itemFineDining = [
  {
    "0": [
      MostPopularItems(
        image: 'assets/brandsposter/01.png',
        title: "Cappuccino",
        desc: "Espresso-based coffee drink",
        price: "30",
      ),
      MostPopularItems(
        image: 'assets/brandsposter/02.png',
        title: "Cafe Latte",
        desc: "Freshly made with espresso and steamed milk",
        price: "35",
      ),
      MostPopularItems(
        image: 'assets/brandsposter/03.png',
        title: "Brewed Coffee",
        desc: "finally ground, brewed coffee",
        price: "25",
      ),
    ],
  },
  {
    "1": [
      MostPopularItems(
        image: 'assets/brandsposter/02.png',
        title: "Cappuccino",
        desc: "Espresso-based coffee drink",
        price: "30",
      ),
      MostPopularItems(
        image: 'assets/brandsposter/01.png',
        title: "Cafe Latte",
        desc: "Freshly made with espresso and steamed milk",
        price: "35",
      ),
      MostPopularItems(
        image: 'assets/brandsposter/03.png',
        title: "Brewed Coffee",
        desc: "finally ground, brewed coffee",
        price: "25",
      ),
    ],
  },
  {
    "2": [
      MostPopularItems(
        image: 'assets/brandsposter/03.png',
        title: "Cappuccino",
        desc: "Espresso-based coffee drink",
        price: "30",
      ),
      MostPopularItems(
        image: 'assets/brandsposter/01.png',
        title: "Cafe Latte",
        desc: "Freshly made with espresso and steamed milk",
        price: "35",
      ),
      MostPopularItems(
        image: 'assets/brandsposter/02.png',
        title: "Brewed Coffee",
        desc: "finally ground, brewed coffee",
        price: "25",
      ),
    ],
  },
  {
    "3": [
      MostPopularItems(
        image: 'assets/brandsposter/03.png',
        title: "Cappuccino",
        desc: "Espresso-based coffee drink",
        price: "30",
      ),
      MostPopularItems(
        image: 'assets/brandsposter/01.png',
        title: "Cafe Latte",
        desc: "Freshly made with espresso and steamed milk",
        price: "35",
      ),
      MostPopularItems(
        image: 'assets/brandsposter/02.png',
        title: "Brewed Coffee",
        desc: "finally ground, brewed coffee",
        price: "25",
      ),
    ],
  },
  {
    "4": [
      MostPopularItems(
        image: 'assets/brandsposter/03.png',
        title: "Cappuccino",
        desc: "Espresso-based coffee drink",
        price: "30",
      ),
      MostPopularItems(
        image: 'assets/brandsposter/01.png',
        title: "Cafe Latte",
        desc: "Freshly made with espresso and steamed milk",
        price: "35",
      ),
      MostPopularItems(
        image: 'assets/brandsposter/02.png',
        title: "Brewed Coffee",
        desc: "finally ground, brewed coffee",
        price: "25",
      ),
    ],
  },
  {
    "5": [
      MostPopularItems(
          image: 'assets/brandsposter/02.png',
          title: "Cappuccino",
          desc: "Espresso-based coffee drink",
          price: "30"),
      MostPopularItems(
          image: 'assets/brandsposter/01.png',
          title: "Cafe Latte",
          desc: "Freshly made with espresso and steamed milk",
          price: "35"),
      MostPopularItems(
          image: 'assets/brandsposter/03.png',
          title: "Brewed Coffee",
          desc: "finally ground, brewed coffee",
          price: "25"),
    ],
  },
  {
    "6": [
      MostPopularItems(
          image: 'assets/brandsposter/01.png',
          title: "Cappuccino",
          desc: "Espresso-based coffee drink",
          price: "30"),
      MostPopularItems(
          image: 'assets/brandsposter/03.png',
          title: "Cafe Latte",
          desc: "Freshly made with espresso and steamed milk",
          price: "35"),
      MostPopularItems(
          image: 'assets/brandsposter/02.png',
          title: "Brewed Coffee",
          desc: "finally ground, brewed coffee",
          price: "25"),
    ],
  },
  {
    "7": [
      MostPopularItems(
          image: 'assets/brandsposter/01.png',
          title: "Cappuccino",
          desc: "Espresso-based coffee drink",
          price: "30"),
      MostPopularItems(
          image: 'assets/brandsposter/02.png',
          title: "Cafe Latte",
          desc: "Freshly made with espresso and steamed milk",
          price: "35"),
      MostPopularItems(
          image: 'assets/brandsposter/03.png',
          title: "Brewed Coffee",
          desc: "finally ground, brewed coffee",
          price: "25"),
    ],
  },
  {
    "8": [
      MostPopularItems(
          image: 'assets/brandsposter/02.png',
          title: "Cappuccino",
          desc: "Espresso-based coffee drink",
          price: "30"),
      MostPopularItems(
          image: 'assets/brandsposter/01.png',
          title: "Cafe Latte",
          desc: "Freshly made with espresso and steamed milk",
          price: "35"),
      MostPopularItems(
          image: 'assets/brandsposter/03.png',
          title: "Brewed Coffee",
          desc: "finally ground, brewed coffee",
          price: "25"),
    ],
  }
];
final List<Map<String, List<MostPopularItems>>> itemBeauty = [
  {
    "0": [
      MostPopularItems(
          image: 'assets/brandsposter/01.png',
          title: "Cappuccino",
          desc: "Espresso-based coffee drink",
          price: "30"),
      MostPopularItems(
          image: 'assets/brandsposter/02.png',
          title: "Cafe Latte",
          desc: "Freshly made with espresso and steamed milk",
          price: "35"),
      MostPopularItems(
          image: 'assets/brandsposter/03.png',
          title: "Brewed Coffee",
          desc: "finally ground, brewed coffee",
          price: "25"),
    ],
  },
  {
    "1": [
      MostPopularItems(
          image: 'assets/brandsposter/02.png',
          title: "Cappuccino",
          desc: "Espresso-based coffee drink",
          price: "30"),
      MostPopularItems(
          image: 'assets/brandsposter/01.png',
          title: "Cafe Latte",
          desc: "Freshly made with espresso and steamed milk",
          price: "35"),
      MostPopularItems(
          image: 'assets/brandsposter/03.png',
          title: "Brewed Coffee",
          desc: "finally ground, brewed coffee",
          price: "25"),
    ],
  },
  {
    "2": [
      MostPopularItems(
          image: 'assets/brandsposter/03.png',
          title: "Cappuccino",
          desc: "Espresso-based coffee drink",
          price: "30"),
      MostPopularItems(
          image: 'assets/brandsposter/02.png',
          title: "Cafe Latte",
          desc: "Freshly made with espresso and steamed milk",
          price: "35"),
      MostPopularItems(
          image: 'assets/brandsposter/01.png',
          title: "Brewed Coffee",
          desc: "finally ground, brewed coffee",
          price: "25"),
    ],
  },
];
final List<Map<String, List<MostPopularItems>>> itemFashion = [
  {
    "0": [
      MostPopularItems(
          image: 'assets/brandsposter/01.png',
          title: "Cappuccino",
          desc: "Espresso-based coffee drink",
          price: "30"),
      MostPopularItems(
          image: 'assets/brandsposter/02.png',
          title: "Cafe Latte",
          desc: "Freshly made with espresso and steamed milk",
          price: "35"),
      MostPopularItems(
          image: 'assets/brandsposter/03.png',
          title: "Brewed Coffee",
          desc: "finally ground, brewed coffee",
          price: "25"),
    ],
  },
  {
    "1": [
      MostPopularItems(
          image: 'assets/brandsposter/02.png',
          title: "Cappuccino",
          desc: "Espresso-based coffee drink",
          price: "30"),
      MostPopularItems(
          image: 'assets/brandsposter/01.png',
          title: "Cafe Latte",
          desc: "Freshly made with espresso and steamed milk",
          price: "35"),
      MostPopularItems(
          image: 'assets/brandsposter/03.png',
          title: "Brewed Coffee",
          desc: "finally ground, brewed coffee",
          price: "25"),
    ],
  },
];

class MostPopularItems {
  final String image;
  final String title;
  final String desc;
  final String price;

  MostPopularItems({this.image, this.title, this.desc, this.price});
}

final List<Map<String, dynamic>> foodStore = [
  {
    "index": 0,
    "color": AppColors.lightGreen,
    "storename": "Store 1",
    "item": [
      {
        "id": 0,
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee101",
        "item_desc":
            "Convallis convallis tellus  dljglkj dfgjkldfj gjdfkljkljfdklfjhklg fjhfgkl dfjkg dhgdfhkjgdk",
        "item_price": "10",
        "item_size": "Small"
      },
      {
        "id": 1,
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee102",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "20",
        "item_size": "Medium"
      },
      {
        "id": 2,
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee103",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "30",
        "item_size": "Large"
      },
      {
        "id": 3,
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee104",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "40"
      },
      {
        "id": 4,
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee105",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "50"
      },
      {
        "id": 5,
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee105",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "60"
      },
    ],
  },
  {
    "index": 1,
    "color": AppColors.lightBlue,
    "storename": "Store 2",
    "item": [
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
    ],
  },
  {
    "index": 2,
    "color": AppColors.lightYellow,
    "storename": "Store 3",
    "item": [
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
    ],
  },
  {
    "index": 3,
    "color": AppColors.lightPink,
    "storename": "Store 4",
    "item": [
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
    ],
  },
  {
    "index": 4,
    "color": AppColors.lightRed,
    "storename": "Store 5",
    "item": [
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
      {
        "image":
            "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "item_name": "Coffee1",
        "item_desc": "Convallis convallis tellus  ",
        "item_price": "10"
      },
    ],
  },
];

final List<String> foodAddOns = [
  'https://images.unsplash.com/photo-1512568400610-62da28bc8a13?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Y29mZmVlfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
  'https://images.unsplash.com/photo-1511920170033-f8396924c348?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8Y29mZmVlfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
  'https://images.unsplash.com/photo-1507133750040-4a8f57021571?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8Y29mZmVlfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
  'https://images.unsplash.com/photo-1482049016688-2d3e1b311543?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
  'https://images.unsplash.com/photo-1476718406336-bb5a9690ee2a?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NXx8Zm9vZHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
];
