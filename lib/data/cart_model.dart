

class CartListModel {
  int id;
  String image;
  String title;
  String price;
  int quantity;
  int shopid;
  bool isAvailable;
  CartListModel(
      {this.id,
      this.image,
      this.title,
      this.price,
      this.quantity,
      this.shopid,
      this.isAvailable});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['title'] = this.title;
    data['price'] = this.price;
    data['qty'] = this.quantity;
    data['isAvailable'] = this.isAvailable;

    return data;
  }
}
