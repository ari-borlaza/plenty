import 'package:plenty/data/Products.dart';

class Coupon {
  int id;
  String code;
  int shopId;
  bool ispercentage;
  String value;
  bool isexpired;
  Null expiredAt;
  bool deliveryonly;
  String description;
  Shop shop;

  Coupon(
      {this.id,
      this.code,
      this.shopId,
      this.ispercentage,
      this.value,
      this.isexpired,
      this.expiredAt,
      this.deliveryonly,
      this.description,
      this.shop});

  Coupon.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    shopId = json['shop_id'];
    ispercentage = json['ispercentage'];
    value = json['value'];
    isexpired = json['isexpired'];
    expiredAt = json['expired_at'];
    deliveryonly = json['deliveryonly'];
    description = json['description'];
    shop = json['shop'] != null ? new Shop.fromJson(json['shop']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['shop_id'] = this.shopId;
    data['ispercentage'] = this.ispercentage;
    data['value'] = this.value;
    data['isexpired'] = this.isexpired;
    data['expired_at'] = this.expiredAt;
    data['deliveryonly'] = this.deliveryonly;
    data['description'] = this.description;
    if (this.shop != null) {
      data['shop'] = this.shop.toJson();
    }
    return data;
  }
}
