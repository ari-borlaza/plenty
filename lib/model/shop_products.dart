class ShopProducts {
  int id;
  String nameEn;
  String nameAr;
  String descEn;
  String descAr;
  String shopId;
  String createdAt;
  String updatedAt;
  List<Products> products;

  ShopProducts(
      {this.id,
      this.nameEn,
      this.nameAr,
      this.descEn,
      this.descAr,
      this.shopId,
      this.createdAt,
      this.updatedAt,
      this.products});

  ShopProducts.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['name_en'];
    nameAr = json['name_ar'];
    descEn = json['desc_en'];
    descAr = json['desc_ar'];
    shopId = json['shop_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['products'] != null) {
      products = new List<Products>();
      json['products'].forEach((v) {
        products.add(new Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_en'] = this.nameEn;
    data['name_ar'] = this.nameAr;
    data['desc_en'] = this.descEn;
    data['desc_ar'] = this.descAr;
    data['shop_id'] = this.shopId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Products {
  int id;
  int shopId;
  String nameEn;
  String nameAr;
  String descEn;
  String descAr;
  String price;
  String offerprice;
  String isoffer;
  String stocks;
  String createdAt;
  String updatedAt;
  String prodcatId;
  String featured;
  String popular;
  List<Sizes> sizes;
  List<MyColor> colors;

  List<Addons> addons;
  List<Images> images;
  Products(
      {this.id,
      this.nameEn,
      this.nameAr,
      this.descEn,
      this.descAr,
      this.price,
      this.offerprice,
      this.isoffer,
      this.stocks,
      this.createdAt,
      this.updatedAt,
      this.prodcatId,
      this.featured,
      this.popular,
      this.sizes,
      this.addons,
      this.images,
      this.colors,
      this.shopId});

  Products.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['name_en'];
    nameAr = json['name_ar'];
    descEn = json['desc_en'];
    descAr = json['desc_ar'];
    price = json['price'];
    offerprice = json['offerprice'];
    isoffer = json['isoffer'];
    stocks = json['stocks'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    prodcatId = json['prodcat_id'];
    featured = json['featured'];
    popular = json['popular'];
    shopId = int.parse(json['shop_id']);
    if (json['sizes'] != null) {
      sizes = new List<Sizes>();
      json['sizes'].forEach((v) {
        sizes.add(new Sizes.fromJson(v));
      });
    }
    if (json['colors'] != null) {
      colors = new List<MyColor>();
      json['colors'].forEach((v) {
        colors.add(new MyColor.fromJson(v));
      });
    }

    if (json['addons'] != null) {
      addons = new List<Addons>();
      json['addons'].forEach((v) {
        addons.add(new Addons.fromJson(v));
      });
    }
    if (json['images'] != null) {
      images = new List<Images>();
      json['images'].forEach((v) {
        images.add(new Images.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_en'] = this.nameEn;
    data['name_ar'] = this.nameAr;
    data['desc_en'] = this.descEn;
    data['desc_ar'] = this.descAr;
    data['price'] = this.price;
    data['offerprice'] = this.offerprice;
    data['isoffer'] = this.isoffer;
    data['stocks'] = this.stocks;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['prodcat_id'] = this.prodcatId;
    data['featured'] = this.featured;
    data['popular'] = this.popular;
    data['shop_id'] = this.shopId;
    if (this.sizes != null) {
      data['sizes'] = this.sizes.map((v) => v.toJson()).toList();
    }

    if (this.addons != null) {
      data['addons'] = this.addons.map((v) => v.toJson()).toList();
    }
    if (this.images != null) {
      data['images'] = this.images.map((v) => v.toJson()).toList();
    }
    if (this.colors != null) {
      data['colors'] = this.colors.map((v) => v.toJson()).toList();
    }

    return data;
  }
}

class MyColor {
  int id;
  String productId;
  String value;
  String others;
  Null createdAt;
  Null updatedAt;

  MyColor(
      {this.id,
      this.productId,
      this.value,
      this.others,
      this.createdAt,
      this.updatedAt});

  MyColor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    value = json['value'];
    others = json['others'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['product_id'] = this.productId;
    data['value'] = this.value;
    data['others'] = this.others;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Sizes {
  int id;
  String productId;
  String value;
  String others;
  String price;
  String createdAt;
  String updatedAt;

  Sizes(
      {this.id,
      this.productId,
      this.value,
      this.others,
      this.price,
      this.createdAt,
      this.updatedAt});

  Sizes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    value = json['value'];
    others = json['others'];
    price = json['price'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['product_id'] = this.productId;
    data['value'] = this.value;
    data['others'] = this.others;
    data['price'] = this.price;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Addons {
  int id;
  String nameEn;
  String nameAr;
  String descEn;
  String descAr;
  String others;
  String price;
  String productId;
  String createdAt;
  String updatedAt;

  Addons(
      {this.id,
      this.nameEn,
      this.nameAr,
      this.descEn,
      this.descAr,
      this.others,
      this.price,
      this.productId,
      this.createdAt,
      this.updatedAt});

  Addons.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['name_en'];
    nameAr = json['name_ar'];
    descEn = json['desc_en'];
    descAr = json['desc_ar'];
    others = json['others'];
    price = json['price'];
    productId = json['product_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_en'] = this.nameEn;
    data['name_ar'] = this.nameAr;
    data['desc_en'] = this.descEn;
    data['desc_ar'] = this.descAr;
    data['others'] = this.others;
    data['price'] = this.price;
    data['product_id'] = this.productId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Images {
  int id;
  String productId;
  String imgurl;

  Images({this.id, this.productId, this.imgurl});

  Images.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    imgurl = json['imgurl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['product_id'] = this.productId;
    data['imgurl'] = this.imgurl;
    return data;
  }
}
