class AddOnModel {
  String title;
  String subtitle;

  AddOnModel({this.title, this.subtitle});

  static List<AddOnModel> addOns() {
    return [
      AddOnModel(title: "Add Caramel Drizzle", subtitle: "( + SAR 5.25 )"),
      AddOnModel(title: "Add Caramel Drizzle", subtitle: "( + SAR 5.25 )"),
      AddOnModel(title: "Add Caramel Drizzle", subtitle: "( + SAR 5.25 )"),
      AddOnModel(title: "Add Caramel Drizzle", subtitle: "( + SAR 5.25 )"),
      AddOnModel(title: "Add Caramel Drizzle", subtitle: "( + SAR 5.25 )"),
      AddOnModel(title: "Add Caramel Drizzle", subtitle: "( + SAR 5.25 )"),
      AddOnModel(title: "Add Caramel Drizzle", subtitle: "( + SAR 5.25 )"),
      AddOnModel(title: "Add Caramel Drizzle", subtitle: "( + SAR 5.25 )"),
      AddOnModel(title: "Add Caramel Drizzle", subtitle: "( + SAR 5.25 )"),
    ];
  }
}
