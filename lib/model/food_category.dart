class MainCategories {
  int id;
  String nameEn;
  String nameAr;
  String descEn;
  String descAr;
  String imgurl;
  List<Shops> shops;

  MainCategories(
      {this.id,
      this.nameEn,
      this.nameAr,
      this.descEn,
      this.descAr,
      this.imgurl,
      this.shops});

  MainCategories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['name_en'];
    nameAr = json['name_ar'];
    descEn = json['desc_en'];
    descAr = json['desc_ar'];
    imgurl = json['imgurl'];
    if (json['shops'] != null) {
      shops = new List<Shops>();
      json['shops'].forEach((v) {
        shops.add(new Shops.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_en'] = this.nameEn;
    data['name_ar'] = this.nameAr;
    data['desc_en'] = this.descEn;
    data['desc_ar'] = this.descAr;
    data['imgurl'] = this.imgurl;
    if (this.shops != null) {
      data['shops'] = this.shops.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Shops {
  int id;
  String nameEn;
  String nameAr;
  String descEn;
  String descAr;
  int catId;
  int active;
  int status;
  Style style;

  Shops(
      {this.id,
      this.nameEn,
      this.nameAr,
      this.descEn,
      this.descAr,
      this.catId,
      this.active,
      this.status,
      this.style});

  Shops.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['name_en'];
    nameAr = json['name_ar'];
    descEn = json['desc_en'];
    descAr = json['desc_ar'];
    catId = int.parse(json['cat_id'].toString());
    active = int.parse(json['active']);
    status = int.parse(json['status']);
    style = json['style'] != null ? new Style.fromJson(json['style']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_en'] = this.nameEn;
    data['name_ar'] = this.nameAr;
    data['desc_en'] = this.descEn;
    data['desc_ar'] = this.descAr;
    data['cat_id'] = this.catId;
    data['active'] = this.active;
    data['status'] = this.status;
    if (this.style != null) {
      data['style'] = this.style.toJson();
    }
    return data;
  }
}

class Style {
  int id;
  String primary;
  String secondary;
  int shopId;
  String txtcolor1;
  String txtcolor2;
  String txtcolor3;
  String txtcolor4;
  int updated;
  String poster;
  String header;
  String bg;
  String bannerimg;
  String bgvid;

  Style(
      {this.id,
      this.primary,
      this.secondary,
      this.shopId,
      this.txtcolor1,
      this.txtcolor2,
      this.txtcolor3,
      this.txtcolor4,
      this.updated,
      this.poster,
      this.header,
      this.bg,
      this.bannerimg,
      this.bgvid});

  Style.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    primary = json['primary'];
    secondary = json['secondary'];
    shopId = int.parse(json['shop_id']);
    txtcolor1 = json['txtcolor1'];
    txtcolor2 = json['txtcolor2'];
    txtcolor3 = json['txtcolor3'];
    txtcolor4 = json['txtcolor4'];
    updated = int.parse(json['updated']);
    poster = json['poster'];
    header = json['header'];
    bg = json['bg'];
    bannerimg = json['bannerimg'];
    bgvid = json['bgvid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['primary'] = this.primary;
    data['secondary'] = this.secondary;
    data['shop_id'] = this.shopId;
    data['txtcolor1'] = this.txtcolor1;
    data['txtcolor2'] = this.txtcolor2;
    data['txtcolor3'] = this.txtcolor3;
    data['txtcolor4'] = this.txtcolor4;
    data['updated'] = this.updated;
    data['poster'] = this.poster;
    data['header'] = this.header;
    data['bg'] = this.bg;
    data['bannerimg'] = this.bannerimg;
    data['bgvid'] = this.bgvid;
    return data;
  }
}
