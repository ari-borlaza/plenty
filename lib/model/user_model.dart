class UserModel {
  int id;
  String name;
  String email;
  String emailVerifiedAt;
  String contact;
  String eid;
  String passport;
  String others;
  String appleId;
  String googleId;
  int active;
  int verified;
  String typeofuser;
  String gender;
  String bday;
  String invitationCode;
  int invites;
  int points;
  String createdAt;
  String updatedAt;

  UserModel(
      {this.id,
      this.name,
      this.email,
      this.emailVerifiedAt,
      this.contact,
      this.eid,
      this.passport,
      this.others,
      this.appleId,
      this.googleId,
      this.active,
      this.verified,
      this.typeofuser,
      this.gender,
      this.bday,
      this.invitationCode,
      this.invites,
      this.points,
      this.createdAt,
      this.updatedAt});

  UserModel.fromJson(Map<String, dynamic> json) {
    print("myjson: ${json["id"]}");
    id = int.parse(json['id'].toString());
    name = json['name'].toString();
    email = json['email'].toString();
    emailVerifiedAt = json['email_verified_at'].toString();
    contact = json['contact'].toString();
    eid = json['eid'].toString();
    passport = json['passport'].toString();
    others = json['others'].toString();
    appleId = json['apple_id'].toString();
    googleId = json['google_id'].toString();
    active = int.parse(json['active'].toString());
    verified = int.parse(json['verified'].toString());
    typeofuser = json['typeofuser'].toString();
    gender = json['gender'].toString();
    bday = json['bday'].toString();
    invitationCode = json['invitation_code'].toString();
    invites = int.parse(json['invites'].toString());
    points = int.parse(json['points'].toString());
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['contact'] = this.contact;
    data['eid'] = this.eid;
    data['passport'] = this.passport;
    data['others'] = this.others;
    data['apple_id'] = this.appleId;
    data['google_id'] = this.googleId;
    data['active'] = this.active;
    data['verified'] = this.verified;
    data['typeofuser'] = this.typeofuser;
    data['gender'] = this.gender;
    data['bday'] = this.bday;
    data['invitation_code'] = this.invitationCode;
    data['invites'] = this.invites;
    data['points'] = this.points;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
