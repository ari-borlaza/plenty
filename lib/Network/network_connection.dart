import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppApi {
  static final instance = AppApi._internal();
  factory AppApi() => instance;
  AppApi._internal();
  final String baseURl =
          "https://plentyapp.mvp-apps.ae/", //http://192.168.1.98:8000/
      appKey = "U6GcqT9i71NQC3xp2cUuvP9190LEFiT7",
      appValue = "cpB7PMB0EF6PCJFjrMFM8zDObOPqNrQI";

  Future<Response> apiGet(String path,
      {Map<String, dynamic> queryParameters}) async {
    Map<String, String> myHeaders = {appKey: appValue};
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("token") != null) {
      final token = prefs.getString("token");
      print("token: $token");
      myHeaders['Authorization'] = "Bearer $token";
    }
    String lang = prefs.getString('language') ?? "en";
    myHeaders['Accept-Language'] = lang;

    final _baseConnection =
        Dio(BaseOptions(baseUrl: baseURl + 'api/', headers: myHeaders));

    return (await _baseConnection.get(
      path,
      queryParameters: queryParameters,
    ));
  }

  Future<Response> apiPost(String path, data) async {
    Map<String, String> myHeaders = {appKey: appValue};
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("token") != null) {
      final token = prefs.getString("token");
      myHeaders['Authorization'] = "Bearer $token";
    }
    String lang = prefs.getString('language') ?? "en";
    myHeaders['Accept-Language'] = lang;
    final _baseConnection =
        Dio(BaseOptions(baseUrl: baseURl + 'api/', headers: myHeaders));

    return (await _baseConnection.post(path, data: data));
  }

  String imageURl(String url) {
    return baseURl + 'storage/' + url;
  }
}
