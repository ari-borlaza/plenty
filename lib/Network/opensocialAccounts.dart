import 'package:url_launcher/url_launcher.dart';

class OpenSocialAcct {
  openWhatsapp() async {
    if (await canLaunch(
        "whatsapp://send?phone=971509510730&text=&source=&data=&app_absent=")) {
      await launch(
          "whatsapp://send?phone=971509510730&text=&source=&data=&app_absent=");
    } else if (await canLaunch("https://wa.me/971509510730")) {
      await launch("https://wa.me/971509510730");
    } else {
      throw 'Could not launch ';
    }
  }

  sendEmail() async {
    final Uri _emailLaunchUri = Uri(
      scheme: 'mailto',
      path: 'info@mvp-apps.com',
    );
    if (await canLaunch(_emailLaunchUri.toString())) {
      await launch(_emailLaunchUri.toString());
    } else if (await canLaunch(_emailLaunchUri.toString())) {
      await launch(_emailLaunchUri.toString());
    } else {
      throw 'Could not launch ';
    }
  }
}
