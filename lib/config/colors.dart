import 'package:flutter/material.dart';

class AppColors {
  renderColor<Color>(String color) {
    switch (color) {
      case 'AMBER':
        return Colors.amber;
        break;
      case 'AMBERACCENT':
        return Colors.amberAccent;
        break;
      case 'BLACK':
        return Colors.black;
        break;
      case 'BLACK12':
        return Colors.black12;
        break;
      case 'BLACK26':
        return Colors.black26;
        break;
      case 'BLACK38':
        return Colors.black38;
        break;
      case 'BLACK45':
        return Colors.black45;
        break;
      case 'BLACK54':
        return Colors.black54;
        break;
      case 'BLACK87':
        return Colors.black87;
        break;
      case 'BLUE':
        return Colors.blue;
        break;
      case 'BLUEACCENT':
        return Colors.blueAccent;
        break;
      case 'BLUEGREY':
        return Colors.blueGrey;
        break;
      case 'BROWN':
        return Colors.brown;
        break;
      case 'CYAN':
        return Colors.cyan;
        break;
      case 'CYANACCENT':
        return Colors.cyanAccent;
        break;
      case 'DEEPORANGE':
        return Colors.deepOrange;
        break;
      case 'DEEPORANGEACCENT':
        return Colors.deepOrangeAccent;
        break;
      case 'DEEPPURPLE':
        return Colors.deepPurple;
        break;
      case 'DEEPPURPLEACCENT':
        return Colors.deepPurpleAccent;
        break;
      case 'GREEN':
        return Colors.green;
        break;
      case 'GREENACCENT':
        return Colors.greenAccent;
        break;
      case 'GREY':
        return Colors.grey;
        break;
      case 'INDIGO':
        return Colors.indigo;
        break;
      case 'INDIGOACCENT':
        return Colors.indigoAccent;
        break;
      case 'LIGHTBLUE':
        return Colors.lightBlue;
        break;
      case 'LIGHTBLUEACCENT':
        return Colors.lightBlueAccent;
        break;
      case 'LIGHTGREEN':
        return Colors.lightGreen;
        break;
      case 'LIGHTGREENACCENT':
        return Colors.lightGreenAccent;
        break;
      case 'LIME':
        return Colors.lime;
        break;
      case 'LIMEACCENT':
        return Colors.limeAccent;
        break;
      case 'ORANGE':
        return Colors.orange;
        break;
      case 'ORANGEACCENT':
        return Colors.orangeAccent;
        break;
      case 'PINK':
        return Colors.pink;
        break;
      case 'PINKACCENT':
        return Colors.pinkAccent;
        break;
      case 'PURPLE':
        return Colors.purple;
        break;
      case 'PURPLEACCENT':
        return Colors.purpleAccent;
        break;
      case 'RED':
        return Colors.red;
        break;
      case 'REDACCENT':
        return Colors.redAccent;
        break;
      case 'TEAL':
        return Colors.teal;
        break;
      case 'TEAL':
        return Colors.tealAccent;
        break;
      case 'TRANSPARENT':
        return Colors.transparent;
        break;
      case 'WHITE':
        return Colors.white;
        break;
      case 'WHITE10':
        return Colors.white10;
        break;
      case 'WHITE12':
        return Colors.white12;
        break;
      case 'WHITE24':
        return Colors.white24;
        break;
      case 'WHITE30':
        return Colors.white30;
        break;
      case 'WHITE38':
        return Colors.white38;
        break;
      case 'WHITE54':
        return Colors.white54;
        break;
      case 'WHITE60':
        return Colors.white60;
        break;
      case 'WHITE70':
        return Colors.white70;
        break;
      case 'YELLOW':
        return Colors.yellow;
        break;
      case 'YELLOWACCENT':
        return Colors.yellowAccent;
        break;
    }
  }

  static const Color white = Color(0xFFFFFFFF);
  static const Color beige = Color(0xFFA8A878);
  static const Color greyblack = Color(0xFF303943);
  static const Color black = Color(0xFF000000);
  static const Color blue = Color(0xFF429BED);
  static const Color brown = Color(0xFFB1736C);
  static const Color darkBrown = Color(0xD0795548);
  static const Color grey = Color(0x64303943);
  static const Color indigo = Color(0xFF6C79DB);
  static const Color lightBlue = Color(0xFF7AC7FF);
  static const Color lightBrown = Color(0xFFCA8179);
  static const Color whiteGrey = Color(0xFFFDFDFD);
  static const Color lightCyan = Color(0xFF98D8D8);
  static const Color lightGreen = Color(0xFF78C850);
  static const Color lighterGrey = Color(0xFFF4F5F4);
  static const Color lightGrey = Color(0xFFF5F5F5);
  static const Color lightPink = Color(0xFFEE99AC);
  static const Color lightPurple = Color(0xFF9F5BBA);
  static const Color lightRed = Color(0xFFFB6C6C);
  static const Color lightTeal = Color(0xFF48D0B0);
  static const Color lightYellow = Color(0xFFFFCE4B);
  static const Color lilac = Color(0xFFA890F0);
  static const Color pink = Color(0xFFF85888);
  static const Color purple = Color(0xFF7C538C);
  static const Color red = Color(0xFFFA6555);
  static const Color teal = Color(0xFF4FC1A6);
  static const Color yellow = Color(0xFFF6C747);
  static const Color semiGrey = Color(0xFFbababa);
  static const Color violet = Color(0xD07038F8);
  static const Color reds = Color(0xFFea3323);
  static const Color txtfieldss = Color(0xFFbdc2c8);
  static const Color greys = Color(0xFF424445);
  static const Color greyss = Color(0xFF2a2a2a);
  static const Color txtfields = Color(0xFFf4f4f4);
  static const Color btn = Color(0xFF2d2d2d);
  static const Color shadow = Color(0xFF838383);
  static const Color duegreen = Color(0xFF27c61c);
  static const Color gold = Color(0xFFFFD700);

  static const Color gold1 = Color(0xFFDAA900);
  static const Color greysss = Color(0xFF1f1f1f);
  static const Color plentyblue = Color(0xFF253a4b);
  static const Color plentyblue1 = Color(0xFF001B71);
  static const Color sadagreen = Color(0xFF116f3d);
//  static const Color sadagreen1 = Color(0xFF013C1Dd);
}
