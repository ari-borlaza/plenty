import 'package:flutter/material.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/constants.dart';

class CartCounter1 extends StatefulWidget {
  final int quantity;

  CartCounter1({this.quantity});

  @override
  _CartCounter1State createState() => _CartCounter1State();
}

class _CartCounter1State extends State<CartCounter1> {
  int numOfItems = 1;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        plusMinusButton1(
            icon: Icons.remove,
            press: () {
              if (numOfItems > 1) {
                setState(() {
                  numOfItems--;
                });
              }
            },
            br: BorderRadius.horizontal(left: Radius.circular(50))),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin / 2),
          child: Text(
            // if our item is less  then 10 then  it shows 01 02 like that
            widget.quantity.toString().padLeft(2, "0"),
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        plusMinusButton1(
            icon: Icons.add,
            press: () {
              setState(() {
                numOfItems++;
              });
            },
            br: BorderRadius.horizontal(right: Radius.circular(15))),
      ],
    );
  }
}
