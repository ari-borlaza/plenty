class Language {
  static bool _arLocal;

  static bool get arLocal => _arLocal;

  static set arLocal(bool value) {
    _arLocal = value;
  }
}
