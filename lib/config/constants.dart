import 'package:flutter/material.dart';

const kTextColor = Color(0xFF535353);
const kTextLightColor = Color(0xFFACACAC);

const kDefaultPaddin = 20.0;

const appColor = Color.fromRGBO(220, 210, 201, 1.0); //login page color
const homePageColor = Color.fromRGBO(250, 250, 250, 1.0); //or 255
const FemalePrimaryColor = Color.fromRGBO(239, 218, 196, 1.0);
const MalePrimaryColor = Color.fromRGBO(147, 164, 146, 1.0);
const textColorFemale = Color.fromRGBO(174, 118, 80, 1.0);
const textColorMale = Color.fromRGBO(147, 163, 145, 1.0);
const unSelectedButtonColor = Color.fromRGBO(198, 198, 198, 1.0);

final kBoxDecoration = BoxDecoration(
    color: homePageColor,
    borderRadius: BorderRadius.circular(5.0),
    boxShadow: [
      BoxShadow(
        color: Colors.grey[200],
        blurRadius: 2,
        spreadRadius: 2,
      )
    ]);
const defaultStyle = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.bold,
);
final tableStyle = TextStyle(
    color: Colors.black.withOpacity(0.7),
    fontSize: 15,
    fontWeight: FontWeight.w600);
