import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plenty/config/textstyles.dart';

import './colors.dart';

const inputFieldTextStyle = TextStyle(
    fontFamily: 'Futura', fontWeight: FontWeight.w500, letterSpacing: 2);

const inputFieldHintTextStyle =
    TextStyle(fontFamily: 'Futura', color: Color(0xff444444), letterSpacing: 3);

const inputFieldPasswordTextStyle = TextStyle(
    fontFamily: 'Futura', fontWeight: FontWeight.w500, letterSpacing: 2);

const inputFieldHintPaswordTextStyle = TextStyle(
  fontFamily: 'Futura',
  color: Color(0xff444444),
  letterSpacing: 2,
);

const inputFieldFocusedBorderStyle = OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(5)),
    borderSide: BorderSide(color: Colors.transparent));

const inputFieldDefaultBorderStyle = OutlineInputBorder(
    borderSide: BorderSide.none,
    gapPadding: 0,
    borderRadius: BorderRadius.all(Radius.circular(5)));

FlatButton plentyFlatBtn(String text, onPressed) {
  return FlatButton(
    onPressed: onPressed,
    child: Text(text, style: TextStyle(fontFamily: 'Futura', fontSize: 15)),
    textColor: AppColors.white,
    color: AppColors.plentyblue,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
    splashColor: AppColors.txtfields,
  );
}

FlatButton plentyFlatBtn3(String text, onPressed) {
  return FlatButton.icon(
    icon: Padding(
      padding: const EdgeInsets.only(right: 20.0),
      child: Image.asset(
        'assets/image/apple_wallet.png',
        width: 34,
        height: 34,
      ),
    ),
    minWidth: double.infinity,
    onPressed: onPressed,
    label: Text(text, style: TextStyle(fontFamily: 'Futura', fontSize: 15)),
    textColor: AppColors.white,
    color: AppColors.black,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
    splashColor: AppColors.txtfields,
  );
}

FlatButton plentyFlatBtn4(String text, onPressed) {
  return FlatButton(
    minWidth: double.infinity,
    onPressed: onPressed,
    child: Text(text, style: TextStyle(fontFamily: 'Futura', fontSize: 15)),
    textColor: AppColors.white,
    color: AppColors.plentyblue1,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
    splashColor: AppColors.txtfields,
  );
}

FlatButton plentyFlatBtn7(String text, onPressed) {
  return FlatButton(
    minWidth: double.infinity,
    onPressed: onPressed,
    child: Text(text, style: TextStyle(fontFamily: 'Futura', fontSize: 15)),
    textColor: AppColors.white,
    color: AppColors.gold1,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    splashColor: AppColors.txtfields,
  );
}

Widget plentyFlatBtn6(String text, onPressed, Color color, color1, Icon icon) {
  return FlatButton.icon(
    // minWidth: double.infinity,
    minWidth: 160,
    icon: icon,
    onPressed: onPressed,
    label: Text(text, style: TextStyle(fontFamily: 'Futura', fontSize: 15)),
    textColor: color,
    color: color1,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(5),
      //side: BorderSide(color: AppColors.black)
    ),

    splashColor: AppColors.txtfields,
  );
}

FlatButton plentyFlatBtn1(String text, onPressed) {
  return FlatButton.icon(
    icon: Icon(CupertinoIcons.chevron_left),
    onPressed: onPressed,
    label: Text(text, style: TextStyle(fontFamily: 'Futura', fontSize: 15)),
    textColor: AppColors.white,
    color: AppColors.greyss,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
    splashColor: AppColors.txtfields,
    minWidth: 300,
  );
}

FlatButton plentyFlatBtn2(String text, onPressed,
    {Color color = AppColors.sadagreen}) {
  return FlatButton(
    height: 25,
    padding: EdgeInsets.all(0),
    onPressed: onPressed,
    child: Text(text, style: TextStyle(fontFamily: 'Futura', fontSize: 10)),
    textColor: AppColors.white,
    color: color,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    splashColor: AppColors.txtfields,
  );
}

FlatButton plentyFlatBtn5(String text, onPressed) {
  return FlatButton(
    minWidth: double.infinity,
    padding: EdgeInsets.fromLTRB(50, 10, 50, 10),
    onPressed: onPressed,
    child: Text(text, style: TextStyle(fontFamily: 'Futura', fontSize: 20)),
    textColor: AppColors.white,
    color: AppColors.plentyblue1,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    splashColor: AppColors.plentyblue1,
  );
}

OutlineButton plentyOutlineBtn(String text, onPressed) {
  return OutlineButton(
    onPressed: onPressed,
    child: Text(text),
    textColor: AppColors.plentyblue,
    highlightedBorderColor: AppColors.txtfields,
    borderSide: BorderSide(color: AppColors.plentyblue),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
    splashColor: AppColors.txtfieldss,
  );
}

Container plentyTextInput(String hintText,
    {onTap, onChanged, onEditingComplete, onSubmitted}) {
  return Container(
    width: 300,
    child: TextField(
      onTap: onTap,
      onChanged: onChanged,
      onEditingComplete: onEditingComplete,
      onSubmitted: onSubmitted,
      // cursorColor: AppColors.plentyblue,
      style: inputFieldTextStyle,
      decoration: InputDecoration(
          filled: true,
          fillColor: AppColors.txtfields,
          prefixIcon: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Image.asset(
              'assets/icons/User02.png',
              width: 10,
              height: 10,
              fit: BoxFit.fill,
              color: AppColors.plentyblue,
            ),
          ),
          hintText: hintText,
          hintStyle: TextStyle(fontFamily: 'Futura', fontSize: 13),
          //focusedBorder: inputFieldFocusedBorderStyle,
          //contentPadding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          border: OutlineInputBorder(
              borderSide: BorderSide.none,
              // gapPadding: 6,
              borderRadius: BorderRadius.all(Radius.circular(10.0)))),
    ),
  );
}

Container plentyPasswordInput(String hintText,
    {onTap, onChanged, onEditingComplete, onSubmitted}) {
  return Container(
    width: 300,
    // height: 200,
    margin: EdgeInsets.only(top: 5),

    child: TextField(
      onTap: onTap,
      onChanged: onChanged,
      onEditingComplete: onEditingComplete,
      onSubmitted: onSubmitted,
      obscureText: true,
      //cursorColor: AppColors.plentyblue,
      style: inputFieldHintPaswordTextStyle,
      decoration: InputDecoration(
          prefixIcon: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Image.asset(
              'assets/icons/Lock.png',
              width: 10,
              height: 10,
              fit: BoxFit.fill,
              color: AppColors.plentyblue,
            ),
          ),
          filled: true,
          fillColor: AppColors.txtfields,
          hintText: hintText,
          hintStyle: TextStyle(fontFamily: 'Futura', fontSize: 13),
          //disabledBorder: inputFieldFocusedBorderStyle,
          //focusedBorder: inputFieldFocusedBorderStyle,
          // contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          //  border: inputFieldFocusedBorderStyle
          border: OutlineInputBorder(
              borderSide: BorderSide.none,
              //       gapPadding: 0,
              borderRadius: BorderRadius.all(Radius.circular(10.0)))),
    ),
  );
}

FlatButton addToBag(onPressed) {
  return FlatButton.icon(
    onPressed: onPressed,
    icon: Icon(Icons.shopping_bag_outlined),
    label: Text("Add to Bag",
        style: TextStyle(fontFamily: 'Futura', fontSize: 15)),
    textColor: AppColors.plentyblue,
    color: AppColors.white,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
    splashColor: AppColors.plentyblue,
  );
}

Widget plusMinusButton({IconData icon, Function press, BorderRadius br}) {
  return InkWell(
    splashColor: AppColors.txtfieldss,
    onTap: press,
    child: Container(
        child: Icon(icon),
        width: 30,
        height: 30,
        decoration: BoxDecoration(color: Colors.grey[200], borderRadius: br)),
  );
}

Widget plusMinusButton1({IconData icon, Function press, BorderRadius br}) {
  return InkWell(
    splashColor: AppColors.txtfieldss,
    onTap: press,
    child: Container(
        alignment: Alignment.center,
        child: Icon(
          icon,
          size: 10,
        ),
        width: 30,
        height: 20,
        decoration: BoxDecoration(color: Colors.grey[200], borderRadius: br)),
  );
}

class TField extends StatefulWidget {
  TField({Key key, this.txt, this.controller}) : super(key: key);
  final String txt;
  final TextEditingController controller;
  @override
  _TFieldState createState() => _TFieldState();
}

class _TFieldState extends State<TField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          homeChoose(widget.txt, 15),
          Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[200].withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 5,
                  offset: Offset(0, 0), // changes position of shadow
                ),
              ],
            ),
            margin: EdgeInsets.symmetric(
              vertical: 10,
            ),
            child: TextFormField(
              controller: widget.controller,
              style: TextStyle(
                  fontFamily: 'Futura', fontSize: 15.0, color: Colors.black),
              decoration: InputDecoration(
                hintStyle: TextStyle(
                    fontFamily: 'Futura',
                    fontSize: 15.0,
                    fontWeight: FontWeight.w100,
                    color: Colors.white),
                //hintText: widget.txt,
                filled: true,
                fillColor: Colors.white,
                contentPadding:
                    const EdgeInsets.only(left: 10.0, bottom: 8.0, top: 8.0),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(10),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
