import 'dart:async';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/provider/app_state.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:easy_localization/easy_localization.dart';

class AccessCard extends StatefulWidget {
/*   final String phoneNumber;
  final String otp;

  AccessCard(this.phoneNumber, this.otp); */

  @override
  _AccessCardState createState() => _AccessCardState();
}

class _AccessCardState extends State<AccessCard> {
  var onTapRecognizer;
  TextEditingController textEditingController = TextEditingController();
  StreamController<ErrorAnimationType> errorController;
  SharedPreferences prefs;
  bool hasError = false;
  String currentText = "";
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();
  TextEditingController field = TextEditingController();
  String invitecode = "";
  String pasteValue = '';
  String subject = 'Plenty of Things Access Code';

  @override
  void initState() {
    onTapRecognizer = TapGestureRecognizer()
      ..onTap = () {
        Navigator.pop(context);
      };
    loadPref();
    errorController = StreamController<ErrorAnimationType>();
    invitecode =
        Provider.of<AppState>(context, listen: false).model.invitationCode;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _getPositions2();
    });
    super.initState();
  }

  loadPref() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void dispose() {
    errorController.close();
    super.dispose();
  }

  GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();
  GlobalKey _keyCard, _keyCardFront = GlobalKey();
  double dx = 0.0, dy = 0.0, dxF = 0.0, dyF = 0.0;
  bool isLoading = true;
  RenderBox renderBoxRed;
  Offset positionRed;
  _getPositions() {
    print("POSITION of Red: $positionRed ");
    setState(() {
      dx = positionRed.dx;
      dy = positionRed.dy;
      isLoading = false;
      print("OK");
    });
  }

  _getPositions2() {
    if (renderBoxRed == null || positionRed == null) {
      renderBoxRed = _keyCardFront.currentContext.findRenderObject();
      positionRed = renderBoxRed.localToGlobal(Offset.zero);
    }
    print("POSITION of Red: $positionRed ");
    setState(() {
      dxF = positionRed.dx;
      dyF = positionRed.dy;
      isLoading = false;
      print("OK");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: AppColors.black,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      backgroundColor: AppColors.white,
      extendBodyBehindAppBar: true,
      // backgroundColor: Colors.blue.shade50,
      key: scaffoldKey,
      body: Stack(
        children: [
          appBarr(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //    SizedBox(height: 100),
              GestureDetector(
                onTap: () {},
                child: Container(
                  padding: EdgeInsets.only(top: 90),
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: ListView(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        child: FlipCard(
                          direction: FlipDirection.HORIZONTAL,

                          onFlipDone: (bool value) {
                            if (value) {
                              //front
                              setState(() {
                                isLoading = true;
                              });

                              _getPositions2();
                            } else {
                              //back
                              setState(() {
                                isLoading = true;
                              });
                              _getPositions();
                            }
                            print(value);
                          }, // default
                          front: Stack(
                            children: [
                              Container(
                                key: _keyCardFront,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage(
                                        'assets/image/AccessCardFront.png'),
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                                height: 175,
                                width: double.infinity,
                                // child: Text('Front'),
                              ),
                              /*    isLoading
                                  ? SizedBox.shrink()
                                  : Positioned(
                                      bottom: dyF * 0.15,
                                      left: dxF * 2.0,
                                      child: Text(
                                        "Abu Bakar",
                                        style: TextStyle(fontFamily: 'Futura',fontFamily: 'Futura',                  color: AppColors.plentyblue1,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ), */
                            ],
                          ),
                          back: Stack(
                            children: [
                              Container(
                                key: _keyCard,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage(
                                        'assets/image/AccessCardBack.png'),
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                                height: 175,
                                width: double.infinity,
                                // child: Text('Back'),
                              ),
                              /*   isLoading
                                  ? SizedBox.shrink()
                                  : Positioned(
                                      top: dy * 0.25,
                                      left: dx * 2,
                                      child: Container(
                                        padding: EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(12),
                                        ),
                                        child: QrImage(
                                          data: Provider.of<AppState>(context,
                                                  listen: false)
                                              .model
                                              .points
                                              .toString(),
                                          version: QrVersions.auto,
                                          size: 80.0,
                                        ),
                                      ),
                                    ),
                              isLoading
                                  ? SizedBox.shrink()
                                  : Positioned(
                                      bottom: dy * 0.2,
                                      right: dx * 3,
                                      child: Text(
                                        "JOIN DATE: ",
                                        style: TextStyle(fontFamily: 'Futura',fontFamily: 'Futura',                    color: AppColors.plentyblue1),
                                      ),
                                    ), */
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 10),
                        child: plentyFlatBtn3("Add to Apple Wallet".tr(),
                            () async {
                          //TODO: code here
                          // PassFile passFile = await Pass()
                          //     .saveFromUrl(url: 'https://link_to_pass/pass.pkpass');
                        }),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Invite Code'.tr(),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            'You will get credit for the invite in their profile'
                                .tr(),
                            style: TextStyle(
                                fontFamily: 'Futura', color: AppColors.grey),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: AppColors.txtfields,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            margin: EdgeInsets.fromLTRB(100, 15, 100, 15),
                            padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
                            alignment: Alignment.center,
                            //width: 200,
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    invitecode,
                                    style: TextStyle(
                                        fontFamily: 'Futura',
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    if (invitecode.trim() == "") {
                                      print('enter text');
                                    } else {
                                      print(invitecode);
                                      FlutterClipboard.copy(invitecode)
                                          .then((value) => print('copied'));
                                    }
                                  },
                                  child: Icon(
                                    Icons.copy,
                                    size: 15,
                                  ),
                                )
                              ],
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: 'Remember you can only invite'.tr(),
                                    style: TextStyle(
                                        fontFamily: 'Futura',
                                        color: AppColors.grey),
                                  ),
                                  TextSpan(
                                      text: ' 3 ' + 'Users'.tr(),
                                      style: TextStyle(
                                          fontFamily: 'Futura',
                                          fontWeight: FontWeight.bold)),
                                ],
                                style: TextStyle(
                                    fontFamily: 'Futura',
                                    color: Colors.black,
                                    fontSize: 15)),
                          ),
                          Container(
                            padding: EdgeInsets.only(bottom: 15, top: 15),
                            child: Row(children: <Widget>[
                              Expanded(
                                child: new Container(
                                    margin: const EdgeInsets.only(
                                        left: 10.0, right: 20.0),
                                    child: Divider(
                                      color: AppColors.grey,
                                      height: 36,
                                    )),
                              ),
                              Text(
                                'or'.tr().toUpperCase(),
                                style: TextStyle(
                                    fontFamily: 'Futura',
                                    color: AppColors.grey,
                                    fontSize: 12),
                              ),
                              Expanded(
                                child: new Container(
                                    margin: const EdgeInsets.only(
                                        left: 20.0, right: 10.0),
                                    child: Divider(
                                      color: AppColors.grey,
                                      height: 36,
                                    )),
                              ),
                            ]),
                          ),
                          InkWell(
                            onTap: () {
                              final RenderBox box = context.findRenderObject();
                              Share.share(invitecode,
                                  subject: subject,
                                  sharePositionOrigin:
                                      box.localToGlobal(Offset.zero) &
                                          box.size);
                            },
                            child: Container(
                              margin: EdgeInsets.fromLTRB(100, 15, 100, 15),
                              padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
                              decoration: BoxDecoration(
                                color: AppColors.plentyblue1,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Expanded(
                                    child: Text(
                                      "Share Invite Code".tr(),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontFamily: 'Futura',
                                          color: AppColors.white,
                                          fontSize: 16),
                                    ),
                                  ),
                                  Icon(
                                    Icons.ios_share,
                                    color: AppColors.white,
                                    size: 20,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget appBarr() {
    return Stack(
      children: [
        Container(
          height: 200,
          decoration: BoxDecoration(
            //  color: AppColors.sadagreen,
            image: DecorationImage(
              image: AssetImage(
                'assets/image/Store Banner.jpg',
              ),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        Positioned(
          top: 100,
          left: 30,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TypewriterAnimatedTextKit(
                text: ['Welcome'.tr()],
                textStyle: TextStyle(
                    fontFamily: 'Futura',
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 25),
                textAlign: TextAlign.start,
                pause: Duration(milliseconds: 15000),
              ),
              SizedBox(
                height: 5,
              ),
              RichText(
                text: TextSpan(
                    text: 'You are now a part of the'.tr(),
                    style: TextStyle(
                        fontFamily: 'Futura',
                        color: Colors.black,
                        fontSize: 15)),
              ),
              RichText(
                text: TextSpan(
                    children: [
                      TextSpan(
                          text: 'Elite'.tr(),
                          style: TextStyle(
                              fontFamily: 'Futura',
                              color: AppColors.gold1,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                        text: ' ' + 'community of Plenty'.tr(),
                      ),
                    ],
                    style: TextStyle(
                        fontFamily: 'Futura',
                        color: Colors.black,
                        fontSize: 15)),
              )
            ],
          ),
        ),
      ],
    );
  }
}
