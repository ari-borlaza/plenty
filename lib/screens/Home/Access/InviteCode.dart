import 'dart:async';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:plenty/Network/network_connection.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/screens/Home/Access/AccessCard.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:easy_localization/easy_localization.dart';

class InviteCode extends StatefulWidget {
/*   final String phoneNumber;
  final String otp;

  InviteCode(this.phoneNumber, this.otp); */

  @override
  _InviteCodeState createState() => _InviteCodeState();
}

class _InviteCodeState extends State<InviteCode> {
  var onTapRecognizer;

  TextEditingController textEditingController = TextEditingController();
  // ..text = "123456";

  StreamController<ErrorAnimationType> errorController;
  SharedPreferences prefs;

  bool hasError = false;
  String currentText = "";
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();
  bool isLoading = true;

  @override
  void initState() {
    onTapRecognizer = TapGestureRecognizer()
      ..onTap = () {
        Navigator.pop(context);
      };
    loadPref();
    errorController = StreamController<ErrorAnimationType>();
    checkForStatus();
    super.initState();
  }

  loadPref() async {
    prefs = await SharedPreferences.getInstance();
  }

  void checkForStatus() async {
    Response res = await AppApi.instance.apiGet("invstatus");
    if (res.statusCode == 200) {
      if (res.data["available"] == true) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.bottomToTop,
                duration: Duration(milliseconds: 500),
                child: AccessCard()));
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  @override
  void dispose() {
    errorController.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: AppColors.black,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      backgroundColor: AppColors.white,
      extendBodyBehindAppBar: true,
      // backgroundColor: Colors.blue.shade50,
      key: scaffoldKey,
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Stack(
              children: [
                appBarr(),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //    SizedBox(height: 100),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        padding: EdgeInsets.only(top: 150),
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        child: ListView(
                          children: <Widget>[
                            //    SizedBox(height: 30),
                            Container(
                              height: 150,
                              decoration: BoxDecoration(
                                //  color: AppColors.sadagreen,
                                image: DecorationImage(
                                  image: AssetImage(
                                    'assets/icons/Plenty Logo.png',
                                  ),
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ),
                            SizedBox(height: 8),
                            SizedBox(
                              height: 5,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 20.0, horizontal: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Enter The Invite Code'.tr(),
                                    style: TextStyle(
                                        fontFamily: 'Futura',
                                        color: AppColors.grey,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 15),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    padding:
                                        EdgeInsets.only(left: 15, right: 15),
                                    decoration: BoxDecoration(
                                      color: AppColors.txtfields,
                                      borderRadius: BorderRadius.circular(18),
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              right: 8.0, bottom: 5),
                                          child: Text(
                                            'P-'.tr(),
                                            style: TextStyle(
                                                fontFamily: 'Futura',
                                                color: AppColors.grey,
                                                fontWeight: FontWeight.w600,
                                                fontSize: 25),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(top: 10),
                                            child: PinCodeTextField(
                                              appContext: context,
                                              pastedTextStyle: TextStyle(
                                                fontFamily:
                                                    'Futura', //  color: Colors.yellow,
                                                fontWeight: FontWeight.bold,
                                              ),
                                              length: 6,
                                              obscureText: false,
                                              obscuringCharacter: '*',
                                              blinkWhenObscuring: true,
                                              animationType: AnimationType.fade,
                                              cursorColor: Colors.black,
                                              animationDuration:
                                                  Duration(milliseconds: 300),
                                              backgroundColor:
                                                  AppColors.txtfields,
                                              enableActiveFill: true,
                                              errorAnimationController:
                                                  errorController,
                                              controller: textEditingController,
                                              keyboardType:
                                                  TextInputType.number,

                                              /*  validator: (v) {
                                          if (v.length < 3) {
                                            return "I'm from validator";
                                          } else {
                                            return null;
                                          }
                                        }, */
                                              /*  boxShadows: [
                                            BoxShadow(
                                              offset: Offset(0, 1),
                                              color: Colors.black12,
                                              blurRadius: 10,
                                            )
                                          ], */

                                              pinTheme: PinTheme(
                                                disabledColor: AppColors.white,
                                                selectedFillColor:
                                                    AppColors.white,
                                                shape: PinCodeFieldShape.box,
                                                borderRadius:
                                                    BorderRadius.circular(6),
                                                selectedColor: AppColors.white,
                                                inactiveColor: AppColors.white,
                                                inactiveFillColor:
                                                    AppColors.white,
                                                activeColor: AppColors.white,
                                                fieldHeight: 40,
                                                fieldWidth: 40,
                                                activeFillColor: hasError
                                                    ? AppColors.white
                                                    : AppColors.white,
                                                //  borderWidth: 2,
                                              ),

                                              onCompleted: (v) {
                                                print("Completed");
                                              },
                                              // onTap: () {
                                              //   print("Pressed");
                                              // },
                                              onChanged: (value) {
                                                print(value);
                                                setState(() {
                                                  currentText = value;
                                                });
                                              },
                                              beforeTextPaste: (text) {
                                                print(
                                                    "Allowing to paste $text");
                                                //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                                                //but you can show anything you want here, like your pop up saying wrong paste format or etc
                                                return true;
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                        20,
                                        20,
                                        20,
                                        50,
                                      ),
                                      child: plentyFlatBtn7("Continue".tr(),
                                          () async {
                                        Response res = await AppApi.instance
                                            .apiPost("invitation", {
                                          //invitation_code(eg. P-123456) and bearer token
                                          "invitation_code": "P-$currentText",
                                        });
                                        if (res.statusCode == 200) {
                                          if (res.data["success"] == true) {
                                            Navigator.pushReplacement(
                                                context,
                                                PageTransition(
                                                    type: PageTransitionType
                                                        .rightToLeftWithFade,
                                                    duration: Duration(
                                                        milliseconds: 500),
                                                    child: AccessCard()));
                                          }
                                        }
                                      }))
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: Text(
                                hasError
                                    ? "Please fill up all the cells properly"
                                        .tr()
                                    : "",
                                style: TextStyle(
                                    fontFamily: 'Futura',
                                    color: Colors.red,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              height: 35,
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              decoration: BoxDecoration(
                                color: AppColors.white,
                                borderRadius: BorderRadius.circular(6),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[200].withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 5,
                                    offset: Offset(
                                        0, 0), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Container(
                                    width: 5,
                                    height: 50,
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(20),
                                            topLeft: Radius.circular(20))),
                                  ),
                                  Expanded(
                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        style: TextStyle(
                                            fontFamily: 'Futura',
                                            color: Colors.black,
                                            fontSize: 11),
                                        children: [
                                          TextSpan(
                                            text:
                                                'You are not allowed to enter Plenty if you dont have the'
                                                        .tr() +
                                                    ' ',
                                          ),
                                          TextSpan(
                                              text: 'Access Card'.tr(),
                                              style: TextStyle(
                                                  fontFamily: 'Futura',
                                                  color: AppColors.gold1,
                                                  fontWeight: FontWeight.bold)),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 14,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
    );
  }

  Widget appBarr() {
    return Stack(
      children: [
        Container(
          height: 200,
          decoration: BoxDecoration(
            //  color: AppColors.sadagreen,
            image: DecorationImage(
              image: AssetImage(
                'assets/image/Store Banner.jpg',
              ),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        Positioned(
          top: 100,
          left: Language.arLocal ? null : 30,
          right: Language.arLocal ? 30 : null,
          // Language.arLocal ? right:30:left: 30,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TypewriterAnimatedTextKit(
                text: ['Welcome'.tr()],
                textStyle: TextStyle(
                    fontFamily: 'Futura',
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 25),
                textAlign: TextAlign.start,
                pause: Duration(milliseconds: 15000),
              ),
              SizedBox(
                height: 5,
              ),
              RichText(
                text: TextSpan(
                    text: 'Please enter the invite code'.tr(),
                    style: TextStyle(
                        fontFamily: 'Futura',
                        color: Colors.black,
                        fontSize: 15)),
              ),
              RichText(
                text: TextSpan(
                    children: [
                      TextSpan(
                        text: 'to get the'.tr() + ' ',
                      ),
                      TextSpan(
                          text: 'Access Card'.tr(),
                          style: TextStyle(
                              fontFamily: 'Futura',
                              color: AppColors.gold1,
                              fontWeight: FontWeight.bold)),
                    ],
                    style: TextStyle(
                        fontFamily: 'Futura',
                        color: Colors.black,
                        fontSize: 15)),
              )
            ],
          ),
        ),
      ],
    );
  }
}
