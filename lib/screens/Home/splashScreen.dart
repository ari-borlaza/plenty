import 'dart:async';

import 'package:flutter/material.dart';
import 'package:plenty/screens/Home/OTPnumber.dart';

class SplshScreen extends StatefulWidget {
  @override
  _SplshScreenState createState() => _SplshScreenState();
}

class _SplshScreenState extends State<SplshScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 5),
        () => Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
              builder: (context) => OTPnumber(),
            ),
            ModalRoute.withName("/")));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('assets/image/Plenty_splash.gif'),
    );
  }
}
