import 'package:dio/dio.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/provider/app_state.dart';
import 'package:plenty/screens/Home/AccountDetails.dart';
import 'package:plenty/screens/Home/Favorite.dart';
import 'package:plenty/screens/Home/OTPnumber.dart';
import 'package:plenty/screens/Home/language_dialog.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wallet/wallet.dart';
import 'package:easy_localization/easy_localization.dart';

import '../contact_us.dart';

class Rewards extends StatefulWidget {
  @override
  _RewardsState createState() => _RewardsState();
}

class _RewardsState extends State<Rewards> {
  SharedPreferences prefs;

  loadPref() async {
    prefs = await SharedPreferences.getInstance();
  }

  List<String> list1 = [
    'Account Details',
    'Contact Us',
    'Change Language',
    'My Favorite',
    'Log Out'
  ];

  List<IconButton> icons1 = [
    IconButton(
      icon: Image.asset('assets/icons/User02.png'),
      onPressed: () {},
    ),
    IconButton(
      icon: Image.asset('assets/icons/Contact.png'),
      onPressed: () {},
    ),
    IconButton(
      icon: Image.asset('assets/icons/Language.png'),
      onPressed: () {},
    ),
    IconButton(
      icon: Image.asset('assets/icons/Fav.png'),
      onPressed: () {},
    ),
    IconButton(
      icon: Image.asset('assets/icons/Sign Out.png'),
      onPressed: () {},
    ),
  ];
  List<dynamic> links1 = [
    AccountDetails(),
    ContactUs(),
    Container(),
    Favorite(),
    OTPnumber(),
  ];

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _getPositions2();
    });
    loadPref();
    super.initState();
  }

  GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();
  GlobalKey _keyCard, _keyCardFront = GlobalKey();
  double dx = 0.0, dy = 0.0, dxF = 0.0, dyF = 0.0;
  bool isLoading = true;
  RenderBox renderBoxRed;
  Offset positionRed;
  _getPositions() {
    print("POSITION of Red: $positionRed ");
    setState(() {
      dx = positionRed.dx;
      dy = positionRed.dy;
      isLoading = false;
      print("OK");
    });
  }

  _getPositions2() {
    if (renderBoxRed == null || positionRed == null) {
      renderBoxRed = _keyCardFront.currentContext.findRenderObject();
      positionRed = renderBoxRed.localToGlobal(Offset.zero);
    }
    print("POSITION of Red: $positionRed ");
    setState(() {
      dxF = positionRed.dx;
      dyF = positionRed.dy;
      isLoading = false;
      print("OK");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverPersistentHeader(
              pinned: true,
              floating: true,
              delegate: ContestTabHeader(
                appBarr(),
              ),
            ),
          ];
        },
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 10),
              titleTxt('My Rewards Card'.tr()),
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: FlipCard(
                  direction: FlipDirection.HORIZONTAL,

                  onFlipDone: (bool value) {
                    if (value) {
                      //front
                      setState(() {
                        isLoading = true;
                      });

                      _getPositions2();
                    } else {
                      //back
                      setState(() {
                        isLoading = true;
                      });
                      _getPositions();
                    }
                    print(value);
                  }, // default
                  front: Stack(
                    children: [
                      Container(
                        key: _keyCardFront,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(
                                'assets/image/ecard_front_update.png'),
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        height: 175,
                        width: double.infinity,
                        // child: Text('Front'),
                      ),
                      isLoading
                          ? SizedBox.shrink()
                          : Positioned(
                              bottom: dyF * 0.15,
                              left: dxF * 2.0,
                              child: Text(
                                Provider.of<AppState>(context, listen: false)
                                            .model
                                            .name ==
                                        "null"
                                    ? ""
                                    : Provider.of<AppState>(context,
                                            listen: false)
                                        .model
                                        .name,
                                style: TextStyle(
                                  fontFamily: 'Futura',
                                  color: AppColors.plentyblue1,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                    ],
                  ),
                  back: Stack(
                    children: [
                      Container(
                        key: _keyCard,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(
                                'assets/image/ecard_back_update.png'),
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        height: 175,
                        width: double.infinity,
                        // child: Text('Back'),
                      ),
                      isLoading
                          ? SizedBox.shrink()
                          : Positioned(
                              top: dy * 0.25,
                              left: dx * 2,
                              child: Container(
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: QrImage(
                                  data: Provider.of<AppState>(context,
                                          listen: false)
                                      .model
                                      .points
                                      .toString(),
                                  version: QrVersions.auto,
                                  size: 80.0,
                                ),
                              ),
                            ),
                      isLoading
                          ? SizedBox.shrink()
                          : Positioned(
                              bottom: dy * 0.2,
                              right: dx * 3,
                              child: Text(
                                "Join Date".tr().toUpperCase() +
                                    ": ${DateFormat.yM().format(DateTime.parse(Provider.of<AppState>(context, listen: false).model.createdAt))}",
                                style: TextStyle(
                                    fontFamily: 'Futura',
                                    color: AppColors.plentyblue1),
                              ),
                            ),
                    ],
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: plentyFlatBtn3("Add to Apple Wallet".tr(), () async {
                  //TODO: code here

                  Response response;
                  Dio dio = new Dio();

                  response = await dio.post(
                      "http://192.168.1.143:8000/storage/passgenerator/pass.pkpass",
                      options: Options(responseType: ResponseType.bytes));

                  if (response.data != null) {
                    try {
                      print(response.data);
                      bool result = await Wallet.presentAddPassViewController(
                          pkpass: response.data);
                      print(result);
                    } catch (e) {
                      print(e);
                      print(e.message);
                    }
                    // try {
                    //   var result2 =
                    //       await FlutterWallet.addPass(pkpass: response.data);
                    //   print(result2);
                    // } catch (e) {
                    //   print(e);
                    //   print(e.message);
                    // }
                  } else {
                    print("Data error");
                  }
                }),
              ),
              SizedBox(height: 10),
              titleTxt('My profile'.tr()),
              //SizedBox(height: 10),
              Container(
                //padding: EdgeInsets.all(5),
                height: 280,
                decoration: BoxDecoration(color: AppColors.white),
                child: ListView.separated(
                    padding: EdgeInsets.zero,
                    separatorBuilder: (BuildContext context, int index) {
                      return Container(
                          margin:
                              const EdgeInsets.only(left: 40.0, right: 40.0),
                          child: Divider(
                            color: Colors.grey,
                            height: 0,
                          ));
                    },
                    //   scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    //  itemExtent: 25.0,
                    itemCount: list1.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        onTap: () async {
                          if (list1[index] == "Log Out") {
                            final SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            await prefs.remove("token");
                            Navigator.pushAndRemoveUntil(
                                context,
                                PageTransition(
                                    type: PageTransitionType.rightToLeft,
                                    duration: Duration(milliseconds: 500),
                                    child: links1[index]),
                                (route) => false);
                          }
                          if (list1[index] == "Change Language") {
                            showDialog(
                              context: context,
                              builder: (context) => Dialog(
                                insetPadding: EdgeInsets.symmetric(
                                    vertical: 24, horizontal: 20),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                                child: LanguageDialog(prefs),
                              ),
                            );
                          } else
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.rightToLeft,
                                    duration: Duration(milliseconds: 500),
                                    child: links1[index]));
                          /* 
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => links1[index])); */
                        },
                        leading: icons1[index],
                        title: Text(
                          list1[index],
                          style: TextStyle(
                              fontFamily: 'Futura',
                              color: AppColors.black,
                              fontSize: 15),
                          textAlign: Language.arLocal
                              ? TextAlign.right
                              : TextAlign.left,
                        ).tr(),
                        trailing: Icon(
                          CupertinoIcons.right_chevron,
                          size: 20,
                          color: AppColors.black,
                        ),
                      );
                    }),
              ),
              Column(
                children: [
                  homeChoose('Find us in Social Media'.tr(), 10),
                  Container(
                      margin: const EdgeInsets.fromLTRB(40, 5, 40, 5),
                      child: Divider(
                        color: Colors.grey,
                        height: 0,
                      )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        icon: Image.asset('assets/icons/FB.png'),
                        onPressed: () {
                          // Navigator.push(
                          //     context,
                          //     PageTransition(
                          //         type: PageTransitionType.rightToLeft,
                          //         duration: Duration(milliseconds: 500),
                          //         child: HomePageNew()));
                        },
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      IconButton(
                        icon: Image.asset('assets/icons/Twtr.png'),
                        onPressed: () {
                          // Navigator.push(
                          //     context,
                          //     PageTransition(
                          //         type: PageTransitionType.rightToLeft,
                          //         duration: Duration(milliseconds: 500),
                          //         child: HomePageNew()));
                        },
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      IconButton(
                        icon: Image.asset('assets/icons/Insta.png'),
                        onPressed: () {
                          // Navigator.push(
                          //     context,
                          //     PageTransition(
                          //         type: PageTransitionType.rightToLeft,
                          //         duration: Duration(milliseconds: 500),
                          //         child: HomePageNew()));
                        },
                      ),
                    ],
                  ),
                  Text(
                    'www.plentyofthings.com',
                    style: TextStyle(
                        fontFamily: 'Futura', fontSize: 10, letterSpacing: 5),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget titleTxt(String txt) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.txtfields,
        borderRadius: BorderRadius.all(Radius.circular(30)),
      ),
      padding: const EdgeInsets.all(10.0),
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          homeChoose(txt, 15),
        ],
      ),
    );
  }

  Widget appBarr() {
    return Container(
      //height: 10,
      decoration: BoxDecoration(
        //  color: AppColors.sadagreen,
        image: DecorationImage(
          image: AssetImage('assets/image/Store Banner.jpg'),
          fit: BoxFit.fitWidth,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Language.arLocal
                  ? Icon(
                      CupertinoIcons.chevron_right,
                      color: Colors.black,
                    )
                  : Icon(
                      CupertinoIcons.chevron_left,
                      color: Colors.black,
                    ),
            ),
            SizedBox(height: 10),
            homeChoose('Welcome to your account'.tr(), 10),
            homeChoose(
                Provider.of<AppState>(context, listen: false).model.name ==
                        "null"
                    ? ""
                    : Provider.of<AppState>(context, listen: false).model.name,
                15),
          ],
        ),
      ),
    );
  }
}

class ContestTabHeader extends SliverPersistentHeaderDelegate {
  ContestTabHeader(
    this.appBarr,
  );
  final Widget appBarr;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return appBarr;
  }

  @override
  double get maxExtent => 125.0;

  @override
  double get minExtent => 125.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
