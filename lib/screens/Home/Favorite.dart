import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/config/utils.dart';
import 'package:plenty/data/fashion_data_list.dart';
import 'package:plenty/data/fashion_product.dart';
import 'package:plenty/database/fave.dart';
import 'package:plenty/screens/Fashion/details/details_screen.dart';
import 'package:plenty/screens/Fashion/fashion_item_card.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:easy_localization/easy_localization.dart';

class Favorite extends StatefulWidget {
  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> with TickerProviderStateMixin {
  Box<Fave> faveBox = Hive.box<Fave>('fave');
  //Box<Fave> faveBox;
  bool selected = true;
  AnimationController animationController;
  List<Map<String, dynamic>> items = fashionStore[0]["item"];
  String description =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id neque libero. Donec finibus sem viverra.';
  String description1 = 'Lorem ipsum dolor sit amet';
  // int itemsCount = products.length;
  List<Product> favProduct = [];
  List<Product> favFeatProduct = [];
  @override
  void initState() {
    // faveBox.clear();
    fetchFavProducts();
    super.initState();
  }

  fetchFavProducts() {
    favProduct =
        products.where((e) => faveBox.values.contains(Fave(e.id))).toList();
/*     favFeatProduct = featuredProducts
        .where((e) => faveBox.values.contains(Fave(e.id)))
        .toList(); */
  }

  @override
  Widget build(BuildContext context) {
    print(faveBox.containsKey('ok'));
    return Scaffold(
        backgroundColor: AppColors.white,
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverPersistentHeader(
                pinned: true,
                floating: true,
                delegate: ContestTabHeader(
                  appBarr(),
                ),
              ),
            ];
          },
          body: ValueListenableBuilder(
            valueListenable: faveBox.listenable(),
            builder: (context, Box<Fave> box, _) {
              if (box.values.isEmpty)
                return Center(
                  child: Text("No favorites".tr()),
                );
              return Expanded(
                child: LiveGrid(
                  // shrinkWrap: true,
                  // physics: NeverScrollableScrollPhysics(),
                  reAnimateOnVisibility: true,
                  itemCount: favProduct.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 20,
                    childAspectRatio: 0.8,
                  ),
                  itemBuilder: animationItemBuilder((index) {
                    Product c = favProduct[index];

                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12.0),
                      child:
                          /* Column(
                        children: [Text("${c.faveId}")],
                      ), */

                          ItemCard(
                        product: c,
                        //  products.where((el) => el.id == c.faveId).last,
                        press: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetailsScreen(
                                product: products[index],
                              ),
                            )),
                        delProduct: (index) async {
                          if (faveBox.containsKey("prod_$index")) {
                            await faveBox.delete("prod_$index");
                          }
                          favFeatProduct.removeWhere((e) => e.id == index);
                          favProduct.removeWhere((e) => e.id == index);
                          // selected = !selected;
                          setState(() {});
                        },
                      ),
                    );
                  }),
                ),
              );
            },
          ),
        ));
  }

  Widget gridProducts(BuildContext context) {
    return Container(
      child: LiveGrid(
        // shrinkWrap: true,
        // physics: NeverScrollableScrollPhysics(),
        reAnimateOnVisibility: true,
        itemCount: products.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 20,
          childAspectRatio: 0.8,
        ),
        itemBuilder: animationItemBuilder((index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: ItemCard(
              product: products[index],
              press: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailsScreen(
                      product: products[index],
                    ),
                  )),
            ),
          );
        }),
      ),
    );
  }

  Widget appBarr() {
    return Container(
      //height: 10,
      decoration: BoxDecoration(
        //  color: AppColors.sadagreen,
        image: DecorationImage(
          image: AssetImage('assets/image/Store Banner.jpg'),
          fit: BoxFit.fitWidth,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 10, 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(
                      context,
                    );
                  },
                  child: Language.arLocal
                      ? Icon(
                          CupertinoIcons.chevron_right,
                          color: Colors.black,
                        )
                      : Icon(
                          CupertinoIcons.chevron_left,
                          color: Colors.black,
                        ),
                ),
                SizedBox(height: 10),
                homeChoose('My Favorite'.tr(), 20),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ContestTabHeader extends SliverPersistentHeaderDelegate {
  ContestTabHeader(
    this.appBarr,
  );
  final Widget appBarr;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return appBarr;
  }

  @override
  double get maxExtent => 125.0;

  @override
  double get minExtent => 125.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
