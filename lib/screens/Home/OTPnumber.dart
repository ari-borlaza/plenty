import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:page_transition/page_transition.dart';
import 'package:plenty/Network/network_connection.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/model/user_model.dart';
import 'package:plenty/provider/app_state.dart';
import 'package:plenty/screens/Home/OTPverify.dart';
import 'package:plenty/screens/Home/home_page_new.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OTPnumber extends StatefulWidget {
  @override
  _OTPnumberState createState() => _OTPnumberState();
}

class _OTPnumberState extends State<OTPnumber> {
  GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  String phoneNumber = "";
  bool isLoading = true;
  String cc = "";
  String isocode = '';
  @override
  void initState() {
    autoLogin();

    super.initState();
  }

  @override
  void didChangeDependencies() {
    Language.arLocal = context.locale == Locale('ar', 'AE');
    super.didChangeDependencies();
  }

  autoLogin() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("token") != null) {
      Response res = await AppApi.instance.apiPost("autologin", {});
      if (res.statusCode == 200) {
        print("my_res: ${res.data}");
        UserModel m = UserModel.fromJson(res.data["user"]);
        Provider.of<AppState>(context, listen: false).model = m;
        // setState(() {
        //   isLoading = false;
        // });
        Navigator.pushAndRemoveUntil(
            context,
            PageTransition(
                type: PageTransitionType.bottomToTop,
                duration: Duration(milliseconds: 500),
                child: HomePageNew()),
            (route) => false);
      }
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      backgroundColor: AppColors.white,
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Stack(
              children: [
                appBarr(),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 150,
                      decoration: BoxDecoration(
                        //  color: AppColors.sadagreen,
                        image: DecorationImage(
                          image: AssetImage(
                            'assets/icons/P.png',
                          ),
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.all(20),
                        alignment: Alignment.centerLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            homeWelcome2('Welcome to Plenty!'.tr(), 30),
                            Text("Please enter your mobile number to login")
                                .tr(),
                          ],
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: IntlPhoneField(
                        showDropdownIcon: false,
                        textAlign: TextAlign.center,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          hintStyle: TextStyle(
                              fontFamily: 'Futura',
                              fontSize: 20.0,
                              fontWeight: FontWeight.w100,
                              color: Colors.white),
                          //hintText: widget.txt,
                          filled: true,
                          fillColor: AppColors.txtfields,
                          contentPadding: const EdgeInsets.only(
                              left: 10.0, bottom: 8.0, top: 8.0),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          /*
                    enabledBorder: const OutlineInputBorder(
                      // width: 0.0 produces a thin "hairline" border
                      borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                    ),
                    //border: InputBorder.none,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: BorderSide(color: AppColors.txtfields),
                    ),
                    contentPadding: EdgeInsets.symmetric(horizontal: 20.0),
                    filled: true,
                    fillColor: AppColors.txtfields,
                    //       labelText: 'Phone Number', */
                        ),
                        initialCountryCode: 'SA',
                        onChanged: (phone) {
                          phoneNumber = phone.completeNumber;
                          cc = phone.countryCode;
                          isocode = phone.countryISOCode;
                          print(phone.completeNumber);
                        },
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: plentyFlatBtn5('Send OTP'.tr(), () async {
                        if (phoneNumber == "") {
                          scaffoldState.currentState.showSnackBar(SnackBar(
                            content:
                                Text("Please enter your phone number").tr(),
                            backgroundColor: Colors.black,
                            action: SnackBarAction(
                              label: "OK",
                              textColor: Colors.white,
                              onPressed: () {},
                            ),
                          ));
                        } else {
                          Response res = await AppApi.instance.apiPost('otp', {
                            "contact": phoneNumber,
                          });
                          if (res.statusCode == 200) {
                            print(res.data);
                            if (res.data['message'] ==
                                "Please enter a valid KSA phone number.") {
                              scaffoldState.currentState.showSnackBar(SnackBar(
                                content: Text(
                                    "Please enter a valid KSA phone number"
                                        .tr()),
                                backgroundColor: Colors.black,
                                action: SnackBarAction(
                                  label: "OK".tr(),
                                  textColor: Colors.white,
                                  onPressed: () {},
                                ),
                              ));
                            } else {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.bottomToTop,
                                      duration: Duration(milliseconds: 500),
                                      child: PinCodeVerificationScreen(
                                          phoneNumber,
                                          res.data["otp"].toString(),
                                          cc,
                                          isocode)));
                            }
                          }
                        }
                      }),
                    ),
                  ],
                ),
              ],
            ),
    );
  }

  Widget appBarr() {
    return Container(
      height: 200,
      decoration: BoxDecoration(
        //  color: AppColors.sadagreen,
        image: DecorationImage(
          image: AssetImage(
            'assets/image/Store Banner.jpg',
          ),
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }
}
