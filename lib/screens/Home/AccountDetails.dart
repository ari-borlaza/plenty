import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:page_transition/page_transition.dart';
import 'package:plenty/Network/network_connection.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/model/user_model.dart';
import 'package:plenty/provider/app_state.dart';
import 'package:plenty/screens/Checkout/NewAdd.dart';
import 'package:plenty/screens/Home/home_page_new.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';

class AccountDetails extends StatefulWidget {
  final String txt;
  final bool isFromOTP;
  final String contact;
  final String code;
  final String isocode;
  const AccountDetails(
      {Key key,
      this.txt,
      this.contact,
      this.isFromOTP = false,
      this.code,
      this.isocode})
      : super(key: key);
  @override
  _AccountDetailsState createState() => _AccountDetailsState();
}

class _AccountDetailsState extends State<AccountDetails> {
  //List<String> txt = ['Full Name', 'Email Address', 'Mobile Number'];
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  String contact = "";
  TextEditingController bday = TextEditingController();
  String gender = "";
/*   List gender = [
    "Male",
    "Female",
  ];
 */
  String select;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    // print("from otp: ${widget.isFromOTP}");
    if (!widget.isFromOTP) {
      name.text =
          Provider.of<AppState>(context, listen: false).model.name == "null"
              ? ""
              : Provider.of<AppState>(context, listen: false).model.name;
      email.text =
          Provider.of<AppState>(context, listen: false).model.email == "null"
              ? ""
              : Provider.of<AppState>(context, listen: false).model.email;
      bday.text =
          Provider.of<AppState>(context, listen: false).model.bday == "null"
              ? ""
              : Provider.of<AppState>(context, listen: false).model.bday;
      gender =
          Provider.of<AppState>(context, listen: false).model.gender == "null"
              ? ""
              : Provider.of<AppState>(context, listen: false).model.gender;
      contact =
          Provider.of<AppState>(context, listen: false).model.contact == "null"
              ? ""
              : Provider.of<AppState>(context, listen: false)
                  .model
                  .contact
                  .substring(4);
    } else {
      contact = widget.contact.substring(widget.code.length);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: AppColors.white,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverPersistentHeader(
              pinned: true,
              floating: true,
              delegate: ContestTabHeader(
                appBarr(),
              ),
            ),
          ];
        },
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              TField(
                txt: 'Full Name'.tr(),
                controller: name,
              ),
              TField(
                txt: 'E-mail Address'.tr(),
                controller: email,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    homeChoose('Birth of Date'.tr(), 15),
                    SizedBox(height: 10),
                    Container(
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[200].withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 5,
                            offset: Offset(0, 0), // changes position of shadow
                          ),
                        ],
                      ),
                      child: TextFormField(
                        controller: bday,
                        onTap: () {
                          DatePicker.showPicker(
                            context,
                            showTitleActions: true,
                            onChanged: (date) {
                              print('change $date in time zone ' +
                                  date.timeZoneOffset.inHours.toString());
                            },
                            onConfirm: (date) {
                              print('confirm $date');
                              setState(() {
                                bday.text = date.toString().split(" ")[0];
                              });
                            },
                          );
                        },
                        style: TextStyle(
                            fontFamily: 'Futura',
                            fontSize: 15.0,
                            color: Colors.black),
                        decoration: InputDecoration(
                          hintStyle: TextStyle(
                              fontFamily: 'Futura',
                              fontSize: 15.0,
                              fontWeight: FontWeight.w100,
                              color: Colors.white),
                          //hintText: widget.txt,
                          filled: true,
                          fillColor: Colors.white,
                          contentPadding: const EdgeInsets.only(
                              left: 10.0, bottom: 8.0, top: 8.0),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    homeChoose('Gender'.tr(), 15),
                    Row(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            setState(() {
                              gender = "Male";
                            });
                          },
                          child: Container(
                              padding: EdgeInsets.all(10),
                              alignment: Alignment.center,
                              margin: EdgeInsets.symmetric(vertical: 5),
                              child: Row(
                                children: [
                                  Icon(
                                    gender == "Male"
                                        ? Icons.check_circle
                                        : Icons.circle,
                                    color: gender == "Male"
                                        ? AppColors.plentyblue1
                                        : Colors.grey[200],
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Text('Male'.tr(),
                                      style: TextStyle(
                                        fontFamily: 'Futura',
                                        color: Colors.black,
                                        fontSize: 13,
                                      )),
                                ],
                              )),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              gender = "Female";
                            });
                          },
                          child: Container(
                              padding: EdgeInsets.all(10),
                              alignment: Alignment.center,
                              margin: EdgeInsets.symmetric(vertical: 5),
                              child: Row(
                                children: [
                                  Icon(
                                    gender == "Female"
                                        ? Icons.check_circle
                                        : Icons.circle,
                                    color: gender == "Female"
                                        ? AppColors.plentyblue1
                                        : Colors.grey[200],
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Text('Female'.tr(),
                                      style: TextStyle(
                                        fontFamily: 'Futura',
                                        color: Colors.black,
                                        fontSize: 13,
                                      )),
                                ],
                              )),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      homeChoose('Mobile Number'.tr(), 15),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[200].withOpacity(0.5),
                              spreadRadius: 2,
                              blurRadius: 5,
                              offset:
                                  Offset(0, 0), // changes position of shadow
                            ),
                          ],
                        ),
                        child: IntlPhoneField(
                          showDropdownIcon: false,
                          initialValue: contact,
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.phone,
                          enabled: false,
                          decoration: InputDecoration(
                            hintStyle: TextStyle(
                                fontFamily: 'Futura',
                                fontSize: 15.0,
                                fontWeight: FontWeight.w100,
                                color: Colors.white),
                            //hintText: widget.txt,
                            filled: true,
                            fillColor: AppColors.white,
                            contentPadding:
                                const EdgeInsets.only(bottom: 5.0, top: 5.0),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          initialCountryCode: "SA",
                          onChanged: (phone) {
                            contact = phone.completeNumber;
                          },
                          /*
                        enabledBorder: const OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                        ),
                        //border: InputBorder.none,
                        border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide: BorderSide(color: AppColors.txtfields),
                        ),
                        contentPadding: EdgeInsets.symmetric(horizontal: 20.0),
                        filled: true,
                        fillColor: AppColors.txtfields,
                        //       labelText: 'Phone Number', */
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  20,
                  20,
                  20,
                  50,
                ),
                child:
                    plentyFlatBtn4("Save Changes".tr().toUpperCase(), () async {
                  // Navigator.push(
                  //     context,
                  //     PageTransition(
                  //         type: PageTransitionType.topToBottom,
                  //         duration: Duration(milliseconds: 500),
                  //         child: HomePageNew()));
                  if (name.text.isNotEmpty &&
                      email.text.isNotEmpty &&
                      bday.text.isNotEmpty &&
                      gender.isNotEmpty &&
                      contact.isNotEmpty) {
                    Response res = await AppApi.instance.apiPost("profile", {
                      "action": "update", //update
                      "name": name.text,
                      "email": email.text,
                      "bday": bday.text,
                      "gender": gender,
                      "contact": contact
                    });
                    if (res.statusCode == 200) {
                      print("my_res: ${res.data}");
                      UserModel m = UserModel.fromJson(res.data["user"]);
                      Provider.of<AppState>(context, listen: false).model = m;
                      if (widget.isFromOTP) {
                        Navigator.pushAndRemoveUntil(
                            context,
                            PageTransition(
                                type: PageTransitionType.bottomToTop,
                                duration: Duration(milliseconds: 500),
                                child: HomePageNew()),
                            (route) => false);
                      } else
                        Navigator.pop(
                          context,
                          /*     PageTransition(
                              type: PageTransitionType.bottomToTop,
                              duration: Duration(milliseconds: 500),
                              child: HomePage())) */
                        );
                    } else {
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                        content: Text("Internal Server Error".tr()),
                        backgroundColor: Colors.black,
                        action: SnackBarAction(
                          label: "OK".tr(),
                          textColor: Colors.white,
                          onPressed: () {},
                        ),
                      ));
                    }
                  } else {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text("Some Fields are missing".tr()),
                      backgroundColor: Colors.black,
                      action: SnackBarAction(
                        label: "OK".tr(),
                        textColor: Colors.white,
                        onPressed: () {},
                      ),
                    ));
                  }
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Padding titleTxt(String txt) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          homeChoose(txt, 15),
        ],
      ),
    );
  }

  Widget appBarr() {
    return Container(
      //height: 10,
      decoration: BoxDecoration(
        //  color: AppColors.sadagreen,
        image: DecorationImage(
          image: AssetImage('assets/image/Store Banner.jpg'),
          fit: BoxFit.fitWidth,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 10, 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () async {
                    Navigator.pop(context);
                  },
                  child: widget.isFromOTP
                      ? SizedBox.shrink()
                      : Language.arLocal
                          ? Icon(
                              CupertinoIcons.chevron_right,
                              color: Colors.black,
                            )
                          : Icon(
                              CupertinoIcons.chevron_left,
                              color: Colors.black,
                            ),
                ),
                SizedBox(height: 10),
                homeChoose('Account Details'.tr(), 20),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
