import 'dart:async';

import 'package:cool_alert/cool_alert.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:plenty/Network/network_connection.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/model/user_model.dart';
import 'package:plenty/provider/app_state.dart';
import 'package:plenty/screens/Home/AccountDetails.dart';
import 'package:plenty/screens/Home/home_page_new.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:easy_localization/easy_localization.dart';

/* 
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: PinCodeVerificationScreen(
          "+8801376221100"), // a random number, please don't call xD
    );
  }
} */

class PinCodeVerificationScreen extends StatefulWidget {
  final String phoneNumber;
  final String otp;
  final String code;
  final String isocode;
  PinCodeVerificationScreen(
      this.phoneNumber, this.otp, this.code, this.isocode);

  @override
  _PinCodeVerificationScreenState createState() =>
      _PinCodeVerificationScreenState();
}

class _PinCodeVerificationScreenState extends State<PinCodeVerificationScreen> {
  var onTapRecognizer;

  TextEditingController textEditingController = TextEditingController();
  // ..text = "123456";

  StreamController<ErrorAnimationType> errorController;
  SharedPreferences prefs;

  bool hasError = false;
  String currentText = "";
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    onTapRecognizer = TapGestureRecognizer()
      ..onTap = () {
        Navigator.pop(context);
      };
    loadPref();
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  loadPref() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void dispose() {
    errorController.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: AppColors.black,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      backgroundColor: AppColors.white,
      extendBodyBehindAppBar: true,
      // backgroundColor: Colors.blue.shade50,
      key: scaffoldKey,
      body: Stack(
        children: [
          appBarr(),
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                //    SizedBox(height: 100),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.only(top: 50),
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: ListView(
                      children: <Widget>[
                        //    SizedBox(height: 30),
                        Container(
                          height: 150,
                          decoration: BoxDecoration(
                            //  color: AppColors.sadagreen,
                            image: DecorationImage(
                              image: AssetImage(
                                'assets/icons/P.png',
                              ),
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        ),
                        SizedBox(height: 8),
                        /*    Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Text(
                            'Phone Number Verification',
                            style: TextStyle(fontFamily: 'Futura',        fontWeight: FontWeight.bold, fontSize: 22),
                            textAlign: TextAlign.center,
                          ),
                        ), */
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30.0, vertical: 8),
                          child: RichText(
                            text: TextSpan(
                                text: "Enter the code sent to".tr() + " ",
                                children: [
                                  TextSpan(
                                      text: widget.phoneNumber,
                                      style: TextStyle(
                                          fontFamily: 'Futura',
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15)),
                                ],
                                style: TextStyle(
                                    fontFamily: 'Futura',
                                    color: Colors.black,
                                    fontSize: 15)),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Form(
                          key: formKey,
                          child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 20.0, horizontal: 50),
                              child: Container(
                                child: PinCodeTextField(
                                  appContext: context,
                                  pastedTextStyle: TextStyle(
                                    fontFamily:
                                        'Futura', //  color: Colors.yellow,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  length: 4,
                                  obscureText: false,
                                  obscuringCharacter: '*',
                                  blinkWhenObscuring: true,
                                  animationType: AnimationType.fade,
                                  /*     validator: (v) {
                                    if (v.length < 3) {
                                      return "I'm from validator";
                                    } else {
                                      return null;
                                    }
                                  }, */
                                  pinTheme: PinTheme(
                                    disabledColor: AppColors.txtfields,
                                    selectedFillColor: AppColors.txtfields,
                                    shape: PinCodeFieldShape.box,
                                    borderRadius: BorderRadius.circular(15),
                                    selectedColor: AppColors.txtfields,
                                    inactiveColor: AppColors.txtfields,
                                    inactiveFillColor: AppColors.txtfields,
                                    activeColor: AppColors.txtfields,
                                    fieldHeight: 50,
                                    fieldWidth: 50,
                                    borderWidth: 1,
                                    activeFillColor: hasError
                                        ? AppColors.txtfields
                                        : AppColors.txtfields,
                                  ),
                                  cursorColor: Colors.black,
                                  animationDuration:
                                      Duration(milliseconds: 300),
                                  //backgroundColor: Colors.blue.shade50,
                                  enableActiveFill: true,

                                  errorAnimationController: errorController,
                                  controller: textEditingController,
                                  keyboardType: TextInputType.number,

                                  /*  boxShadows: [
                                    BoxShadow(
                                      offset: Offset(0, 1),
                                      color: Colors.black12,
                                      blurRadius: 10,
                                    )
                                  ], */
                                  onCompleted: (v) {
                                    print("Completed");
                                  },
                                  // onTap: () {
                                  //   print("Pressed");
                                  // },
                                  onChanged: (value) {
                                    print(value);
                                    setState(() {
                                      currentText = value;
                                    });
                                  },
                                  beforeTextPaste: (text) {
                                    print("Allowing to paste".tr() + " $text");
                                    //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                                    //but you can show anything you want here, like your pop up saying wrong paste format or etc
                                    return true;
                                  },
                                ),
                              )),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30.0),
                          child: Text(
                            hasError
                                ? "Please fill up all the cells properly".tr()
                                : "",
                            style: TextStyle(
                                fontFamily: 'Futura',
                                color: Colors.red,
                                fontSize: 12,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                text: "Didn't receive the code?".tr() ,
                                style: TextStyle(
                                    fontFamily: 'Futura',
                                    color: Colors.black,
                                    fontSize: 15),
                                /*  children: [
                                    TextSpan(
                                        text: " RESEND",
                                        recognizer: onTapRecognizer,
                                        style: TextStyle(fontFamily: 'Futura',                    color: Color(0xFF91D3B3),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16))
                                  ] */
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                // OTP code resend
                              },
                              child: Container(
                                padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 1,
                                      blurRadius: 3,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: Text(
                                  "Resend OTP".tr().toUpperCase(),
                                  style: TextStyle(
                                      fontFamily: 'Futura',
                                      color: AppColors.plentyblue,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 14,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: plentyFlatBtn5(
                            'Login'.tr().toUpperCase(),
                            () async {
                              print(widget.phoneNumber);
                              print(currentText);
                              Response res =
                                  await AppApi.instance.apiPost('verify', {
                                "contact": widget.phoneNumber,
                                "otp": currentText,
                              });
                              if (res.statusCode == 200) {
                                print(res.data);
                                if (res.data['success']) {
                                  Response res1 = await AppApi.instance
                                      .apiPost('register', {
                                    "contact": widget.phoneNumber,
                                  });
                                  if (res1.statusCode == 200) {
                                    print(res1.data);
                                    await prefs.setString(
                                        "token", res1.data["token"]);
                                    if (res1.data["message"] ==
                                        "User already exists.") {
                                      UserModel m =
                                          UserModel.fromJson(res1.data["user"]);
                                      Provider.of<AppState>(context,
                                              listen: false)
                                          .model = m;
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType
                                                  .bottomToTop,
                                              duration:
                                                  Duration(milliseconds: 500),
                                              child: HomePageNew()));
                                    } else {
                                      CoolAlert.show(
                                          context: context,
                                          type: CoolAlertType.success,
                                          text: "Congratulations" +
                                              "!!! " +
                                              "You have got 100 points" +
                                              ".".tr(),
                                          barrierDismissible: false,
                                          confirmBtnText: "OK".tr(),
                                          confirmBtnColor: Colors.green,
                                          onConfirmBtnTap: () {
                                            Navigator.push(
                                                context,
                                                PageTransition(
                                                    type: PageTransitionType
                                                        .bottomToTop,
                                                    duration: Duration(
                                                        milliseconds: 500),
                                                    child: AccountDetails(
                                                        isFromOTP: true,
                                                        contact:
                                                            widget.phoneNumber,
                                                        code: widget.code,
                                                        isocode:
                                                            widget.isocode)));
                                            // AccountDetails()
                                          });
                                    }
                                  }
                                } else {
                                  scaffoldKey.currentState
                                      .showSnackBar(SnackBar(
                                    content:
                                        Text("Please enter your OTP number")
                                            .tr(),
                                    backgroundColor: Colors.black,
                                    action: SnackBarAction(
                                      label: "OK".tr(),
                                      textColor: Colors.white,
                                      onPressed: () {},
                                    ),
                                  ));
                                }
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget appBarr() {
    return Stack(
      children: [
        Language.arLocal
                  ? Icon(
                      CupertinoIcons.chevron_right,
                      color: Colors.black,
                    )
                  : Icon(
                      CupertinoIcons.chevron_left,
                      color: Colors.black,
                    ),
        Container(
          height: 200,
          decoration: BoxDecoration(
            //  color: AppColors.sadagreen,
            image: DecorationImage(
              image: AssetImage(
                'assets/image/Store Banner.jpg',
              ),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
      ],
    );
  }
}
