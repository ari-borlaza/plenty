import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/data/plenty_data.dart';
import 'package:plenty/model/food_category.dart';
import 'package:plenty/provider/app_state.dart';
import 'package:plenty/screens/Checkout/MyBag.dart';
import 'package:plenty/screens/Food/FoodCategoryPage.dart';
import 'package:plenty/screens/Home/Access/InviteCode.dart';
import 'package:plenty/screens/Home/Favorite.dart';
import 'package:plenty/size.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:video_player/video_player.dart';
import 'package:easy_localization/easy_localization.dart';

import 'RewardCard.dart';

class HomePageNew extends StatefulWidget {
  @override
  _HomePageNewState createState() => _HomePageNewState();
}

class _HomePageNewState extends State<HomePageNew> {
  PlentyData _plentyData = PlentyData();
  PageController _pageController, _pageController2;
  int selectedIndex = 0;

  @override
  void initState() {
    _pageController = PageController(
      viewportFraction: 1.0,
      initialPage: 0,
    );
    _pageController2 = PageController(
      viewportFraction: 0.6,
      initialPage: 0,
    );
    super.initState();
  }

  @override
  void didChangeDependencies() {
    Language.arLocal = context.locale == Locale('ar', 'AE');
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    MySize().init(context);
    print(MySize().width);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight + 40),
        child: SafeArea(child: _header()),
      ),
      extendBodyBehindAppBar: true,
      body: Provider.of<AppState>(context, listen: true).isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Stack(
              fit: StackFit.expand,
              children: [
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: PageView.builder(
                    controller: _pageController,
                    // pageSnapping: false,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) =>
                        _ReusableStack(
                            plentyData: _plentyData.plentyList[index]),
                    itemCount: _plentyData.plentyList.length,
                    // children: <Widget>[
                    //
                    //   ReusableStack(plentyData: _plentyData, selectedIndex: 1),
                    //   ReusableStack(plentyData: _plentyData, selectedIndex: 2),
                    // ],
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Container(
                    decoration: BoxDecoration(
                      // color: Colors.white.withOpacity(.4),
                      gradient: LinearGradient(
                          colors: [
                            AppColors.plentyblue.withOpacity(1),
                            Colors.white.withOpacity(.6),
                            Colors.white.withOpacity(.7),
                            Colors.white.withOpacity(.8),
                            Colors.white.withOpacity(1)
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [0.01, 0.2, 0.5, 0.6, 0.7]),
                    ),
                  ),
                ),
                Positioned(
                  top: 100,
                  left: -90,
                  right: 0,
                  bottom: 0,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 205,
                          padding: EdgeInsets.only(left: 90),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                        left: 18.0,
                                        bottom: 10,
                                        right: 18.0,
                                      ),
                                      child: TypewriterAnimatedTextKit(
                                        text: [
                                          "Choose your".tr() +
                                              "\n" +
                                              "Experience".tr()
                                        ],
                                        textStyle: TextStyle(
                                          fontFamily: 'Futura',
                                          fontSize: 32,
                                          fontWeight: FontWeight.w400,
                                          color: AppColors.plentyblue1,
                                        ),
                                        textAlign: TextAlign.start,
                                        pause: Duration(milliseconds: 15000),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                      child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  type: PageTransitionType
                                                      .rightToLeft,
                                                  duration: Duration(
                                                      milliseconds: 1000),
                                                  child: MyBag()));
                                        },
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Container(
                                              padding: EdgeInsets.all(2),
                                              child: Image.asset(
                                                'assets/icons/Bag.png',
                                                color: AppColors.plentyblue1,
                                              ),
                                              decoration: Language.arLocal
                                                  ? BoxDecoration(
                                                      border: Border.all(
                                                        color: AppColors
                                                            .plentyblue1,
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        topRight:
                                                            Radius.circular(
                                                                100),
                                                        bottomRight:
                                                            Radius.circular(
                                                                100),
                                                      ))
                                                  : BoxDecoration(
                                                      border: Border.all(
                                                          color: AppColors
                                                              .plentyblue1),
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        topLeft:
                                                            Radius.circular(
                                                                100),
                                                        bottomLeft:
                                                            Radius.circular(
                                                                100),
                                                      )),
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 5),
                                              width: 50,
                                              height: 35,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8.0, right: 2.0),
                                              child: itemTxt1(
                                                  "My Bag".tr(),
                                                  12,
                                                  AppColors.plentyblue1,
                                                  FontWeight.normal),
                                            ),
                                          ],
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  type: PageTransitionType
                                                      .rightToLeft,
                                                  duration: Duration(
                                                      milliseconds: 1000),
                                                  child: Favorite()));
                                        },
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Container(
                                              padding: EdgeInsets.all(2),
                                              child: Image.asset(
                                                'assets/icons/Fav.png',
                                                color: AppColors.plentyblue1,
                                              ),
                                              decoration: Language.arLocal
                                                  ? BoxDecoration(
                                                      border: Border.all(
                                                        color: AppColors
                                                            .plentyblue1,
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        topRight:
                                                            Radius.circular(
                                                                100),
                                                        bottomRight:
                                                            Radius.circular(
                                                                100),
                                                      ))
                                                  : BoxDecoration(
                                                      border: Border.all(
                                                        color: AppColors
                                                            .plentyblue1,
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        topLeft:
                                                            Radius.circular(
                                                                100),
                                                        bottomLeft:
                                                            Radius.circular(
                                                                100),
                                                      )),
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 5),
                                              width: 50,
                                              height: 35,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8.0, right: 2.0),
                                              child: itemTxt1(
                                                  "Favorite".tr(),
                                                  12,
                                                  AppColors.plentyblue1,
                                                  FontWeight.normal),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 5.0),
                                        child: InkWell(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                PageTransition(
                                                    type: PageTransitionType
                                                        .rightToLeft,
                                                    duration: Duration(
                                                        milliseconds: 1000),
                                                    child: InviteCode()));
                                          },
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: [
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Shimmer.fromColors(
                                                    // direction: ShimmerDirection.btt,
                                                    baseColor: AppColors.gold1,
                                                    highlightColor:
                                                        Colors.grey[100],
                                                    enabled: true,
                                                    child: Container(
                                                      width: 100,
                                                      height: 40,
                                                      decoration: Language
                                                              .arLocal
                                                          ? BoxDecoration(
                                                              color: AppColors
                                                                  .gold,
                                                              /*  border: Border.all(
                                                          color: AppColors.plentyblue1), */
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .only(
                                                                topRight: Radius
                                                                    .circular(
                                                                        20),
                                                                bottomRight:
                                                                    Radius
                                                                        .circular(
                                                                            20),
                                                              ))
                                                          : BoxDecoration(
                                                              color: AppColors
                                                                  .gold,
                                                              /* border: Border.all(
                                                      color: AppColors.plentyblue1), */
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .only(
                                                                topLeft: Radius
                                                                    .circular(
                                                                        20),
                                                                bottomLeft: Radius
                                                                    .circular(
                                                                        20),
                                                              )),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 8.0,
                                                            right: 8.0,
                                                            top: 5),
                                                    child: itemTxt1(
                                                        "My Access".tr(),
                                                        12,
                                                        AppColors.plentyblue1,
                                                        FontWeight.normal),
                                                  ),
                                                ],
                                              ),
                                              Positioned(
                                                top: -2,
                                                right: 0,
                                                child: Container(
                                                  /*
                                                  child: Image.asset(
                                                    'assets/icons/P.png',
                                                    color: AppColors.white,
                                                  ), */
                                                  padding: EdgeInsets.all(5),
                                                  decoration: Language.arLocal
                                                      ? BoxDecoration(
                                                          color:
                                                              AppColors.gold1,
                                                          /*  border: Border.all(
                                                          color: AppColors.plentyblue1), */
                                                          borderRadius:
                                                              BorderRadius.only(
                                                            topRight:
                                                                Radius.circular(
                                                                    20),
                                                            bottomRight:
                                                                Radius.circular(
                                                                    20),
                                                          ))
                                                      : BoxDecoration(
                                                          color:
                                                              AppColors.gold1,
                                                          /*  border: Border.all(
                                                          color: AppColors.plentyblue1), */
                                                          borderRadius:
                                                              BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            bottomLeft:
                                                                Radius.circular(
                                                                    20),
                                                          )),
                                                  margin: EdgeInsets.symmetric(
                                                      vertical: 5),
                                                  width: 100,
                                                  height: 34,
                                                ),
                                              ),
                                              Positioned(
                                                  top: 10,
                                                  child: Icon(
                                                      Icons.card_giftcard,
                                                      color: AppColors.white)),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 18.0),
                                child: Divider(
                                  indent: 5,
                                  endIndent:
                                      MediaQuery.of(context).size.width / 2,
                                  thickness: 1.5,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 480,
                          margin: EdgeInsets.only(top: 50),
                          child: PageView.builder(
                            controller: _pageController2,
                            // pageSnapping: false,
                            onPageChanged: (index) {
                              setState(() {
                                selectedIndex = index;
                                _pageController.jumpToPage(selectedIndex);
                              });
                            },
                            itemBuilder: (BuildContext context, int index) =>
                                _ReusableStack2(
                                    plentyData: Provider.of<AppState>(context,
                                            listen: false)
                                        .mainCategories[index],
                                    index: index,
                                    sIndex: selectedIndex),
                            itemCount:
                                Provider.of<AppState>(context, listen: false)
                                    .mainCategories
                                    .length,
                            // children: <Widget>[
                            //
                            //   ReusableStack(plentyData: _plentyData, selectedIndex: 1),
                            //   ReusableStack(plentyData: _plentyData, selectedIndex: 2),
                            // ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  _header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
      child: Row(
        children: [
          Image.asset(
            'assets/icons/User01.png',
            fit: BoxFit.cover,
            width: 34,
            height: 34,
          ),
          TypewriterAnimatedTextKit(
            text: [
              'Welcome'.tr() +
                  ' ${Provider.of<AppState>(context, listen: false).model.name == "null" ? "" : Provider.of<AppState>(context, listen: false).model.name}'
            ],
            textStyle: TextStyle(
                fontFamily: 'Futura',
                color: Colors.white,
                fontWeight: FontWeight.w300),
            textAlign: TextAlign.start,
            pause: Duration(milliseconds: 15000),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.rightToLeft,
                      duration: Duration(milliseconds: 500),
                      child: Rewards()));
            },
            child: IconButton(
              icon: Image.asset(
                'assets/icons/Menu.png',
                color: AppColors.white,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    PageTransition(
                        type: PageTransitionType.rightToLeft,
                        duration: Duration(milliseconds: 500),
                        child: Rewards()));
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _ReusableStack extends StatefulWidget {
  const _ReusableStack({
    Key key,
    @required PlentyModel plentyData,
  })  : _plentyData = plentyData,
        super(key: key);

  final PlentyModel _plentyData;

  @override
  __ReusableStackState createState() => __ReusableStackState();
}

class __ReusableStackState extends State<_ReusableStack> {
  VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.asset(widget._plentyData.video);

    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(true);
    _controller.initialize().then((_) => setState(() {}));
    _controller.play();
    print("play play play");
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _controller.value.initialized
        ? AspectRatio(
            aspectRatio: _controller.value.aspectRatio,
            child: VideoPlayer(_controller),
          )
        : Container();
  }
}

class _ReusableStack2 extends StatelessWidget {
  const _ReusableStack2({
    Key key,
    @required MainCategories plentyData,
    @required int index,
    @required int sIndex,
  })  : _plentyData = plentyData,
        index = index,
        sIndex = sIndex,
        super(key: key);

  final MainCategories _plentyData;
  final int index;
  final int sIndex;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AnimatedOpacity(
          duration: Duration(seconds: 1),
          opacity: sIndex == index ? 1 : 0.5,
          child: Text(
            _plentyData.nameEn,
            style: TextStyle(
                fontFamily: 'Futura',
                fontWeight: FontWeight.w500,
                fontSize: 26,
                color: AppColors.plentyblue1),
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.rightToLeft,
                  duration: Duration(milliseconds: 500),
                  child: movetoPage(index, _plentyData),
                ));
          },
          child: CachedNetworkImage(
            imageUrl: _plentyData.imgurl,
            placeholder: (context, url) => Image.asset(
              'assets/icons/Plenty Logo.png',
              fit: BoxFit.cover,
            ),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),

          // Image.network(
          //   _plentyData.imgurl,
          // ),
        ),
      ],
    );
  }

  movetoPage(int index, MainCategories model) {
    switch (index) {
      case 0:
        return FoodCategoryPage(
          model: model,
        );
        break;
      case 1:
        return FoodCategoryPage(
          model: model,
        );
        break;
      case 2:
        return FoodCategoryPage(
          model: model,
        );
        break;
    }
  }
}
