import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plenty/Network/network_connection.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/size.dart';
import 'package:plenty/widget/Reusable_material_button.dart';
import 'package:plenty/widget/empty_text_field.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';
import 'Checkout/MyBag.dart';

List<String> type = [
  "Bug Report",
  "Feature Request",
  "Feedback",
  "Complaint",
  "Suggestions",
  "Account Issues",
  "Others",
];
List<String> priority = [
  "Low",
  "Medium",
  "High",
];

class ContactUs extends StatefulWidget {
  @override
  _GetHelpState createState() => _GetHelpState();
}

class _GetHelpState extends State<ContactUs> {
  String _selectedType;
  String _selectedPriority;
  TextEditingController dialogMessageController = TextEditingController();
  TextEditingController titleController = TextEditingController();
  TextEditingController descController = TextEditingController();
  GlobalKey<FormState> _formState = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  @override
  void initState() {
    super.initState();
    _selectedType = type[0];
    _selectedPriority = priority[0];
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverPersistentHeader(
              pinned: true,
              floating: true,
              delegate: ContestTabHeader(
                appBarr(),
              ),
            ),
          ];
        },
        body: ListView(
          children: [
            Form(
              key: _formState,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "We'd love to hear from you".tr(),
                      style:
                          TextStyle(fontWeight: FontWeight.w400, fontSize: 20),
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: Text(
                        "We are here to help and answer any question you might have."
                            .tr(),
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                      width: double.infinity,
                      height: MySize().height * 0.25,
                      child: Image.asset(
                        'assets/image/contactus.png',
                        fit: BoxFit.cover,
                      )),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Title".tr(),
                          style: TextStyle(color: Colors.black, fontSize: 12),
                        ),
                        ReusableEmptyTextField(
                          controller: titleController,
                          onSubmit: (value) =>
                              FocusScope.of(context).nextFocus(),
                        ),
                        Text(
                          "Type".tr(),
                          style: TextStyle(color: Colors.black, fontSize: 12),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(
                            vertical: 10,
                          ),
                          padding: EdgeInsets.only(left: 5, right: 5),
                          height: 40,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[200],
                                blurRadius: 2,
                                spreadRadius: 2,
                              ),
                            ],
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              icon: Icon(
                                Icons.keyboard_arrow_down,
                                color: Colors.black,
                              ),
                              value: _selectedType,
                              items: type
                                  .map(
                                    (value) => DropdownMenuItem(
                                      value: value,
                                      child: Text(
                                        value,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 14),
                                      ).tr(),
                                    ),
                                  )
                                  .toList(),
                              onChanged: (value) {
                                setState(() {
                                  _selectedType = value;
                                });
                              },
                            ),
                          ),
                        ),
                        Text(
                          "Priority".tr(),
                          style: TextStyle(color: Colors.black, fontSize: 12),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(
                            vertical: 10,
                          ),
                          padding: EdgeInsets.only(left: 5, right: 5),
                          height: 40,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[200],
                                blurRadius: 2,
                                spreadRadius: 2,
                              ),
                            ],
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              icon: Icon(
                                Icons.keyboard_arrow_down,
                                color: Colors.black,
                              ),
                              value: _selectedPriority,
                              items: priority
                                  .map(
                                    (value) => DropdownMenuItem(
                                      value: value,
                                      child: Text(
                                        value,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 14),
                                      ).tr(),
                                    ),
                                  )
                                  .toList(),
                              onChanged: (value) {
                                setState(() {
                                  _selectedPriority = value;
                                });
                              },
                            ),
                          ),
                        ),
                        Text(
                          "Description".tr(),
                          style: TextStyle(color: Colors.black, fontSize: 12),
                        ),
                        ReusableEmptyTextField(
                          maxLines: 4,
                          controller: descController,
                          onSubmit: (value) => FocusScope.of(context).unfocus(),
                          textInputAction: TextInputAction.done,
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        isLoading == false
                            ? SizedBox(
                                width: MySize().width,
                                child: ReusableMaterialButton(
                                  title: "Send".tr(),
                                  pressMe: () async {
                                    if (_formState.currentState.validate()) {
                                      setState(() {
                                        isLoading = true;
                                      });
                                      Map<String, dynamic> data = {
                                        "title": titleController.text,
                                        "dept": _selectedType,
                                        "priority_id": convertPriorityToInt(
                                            _selectedPriority),
                                        "body": descController.text,
                                        "name": ' UserDetails.user.name',
                                        "email": ' UserDetails.user.email',
                                      };
                                      Response response = await AppApi()
                                          .apiPost("contactus", data);
                                      if (response.statusCode == 200) {
                                        setState(() {
                                          isLoading = false;
                                        });
                                        print(response.data);
                                        _scaffold.currentState.showSnackBar(
                                          SnackBar(
                                            content: Text(
                                              "Message sent successfully".tr(),
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            action: SnackBarAction(
                                              onPressed: () {},
                                              label: "OK".tr(),
                                              textColor: Colors.white,
                                            ),
                                          ),
                                        );
                                      }
                                    }
                                  },
                                ),
                              )
                            : Center(
                                child: SizedBox(
                                    width: 30,
                                    height: 30,
                                    child: CircularProgressIndicator())),
                        SizedBox(
                          height: MySize().height * 0.01,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Divider(
                                height: 2,
                                color: Colors.grey,
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Text(
                                "or".tr().toUpperCase(),
                                style: TextStyle(fontWeight: FontWeight.w600),
                              ),
                            ),
                            Expanded(
                                child: Divider(
                              height: 2,
                              color: Colors.grey,
                            )),
                          ],
                        ),
                        SizedBox(
                          height: MySize().height * 0.03,
                        ),
                        GestureDetector(
                          onTap: () async {
                            String number = "+971505628883";
                            try {
                              if (await canLaunch(
                                  "whatsapp://send?phone=$number&text")) {
                                await launch(
                                    "whatsapp://send?phone=$number&text");
                              } else if (await canLaunch(
                                  "https://api.whatsapp.com/send?phone=$number&text=&source=&data=&app_absent=")) {
                                await launch(
                                    "https://api.whatsapp.com/send?phone=$number&text=&source=&data=&app_absent=");
                              } else {
                                throw 'Could not launch ';
                              }
                            } catch (e) {}
                          },
                          child: Stack(
                            overflow: Overflow.visible,
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(vertical: 10),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(5.0),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey[200],
                                        blurRadius: 2,
                                        spreadRadius: 2,
                                      )
                                    ]),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 4,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          left: 65.0, //Language.arLocal ? 0 :
                                          // right: Language.arLocal ? 65 : 0,
                                        ),
                                        child: Text(
                                          "Contact us through Whatsapp".tr(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: -5,
                                left: 10, //Language.arLocal ? null :
                                // right: Language.arLocal ? 10 : null,
                                child: Transform(
                                  transform: Matrix4.identity()
                                    ..setEntry(3, 2, 0.01)
                                    ..rotateY(44.7),
                                  child: Image.asset(
                                    'assets/brandsheader/whatsapp-icon.png',
                                    fit: BoxFit.cover,
                                    width: 45,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget appBarr() {
    return Container(
      //height: 10,
      decoration: BoxDecoration(
        //  color: AppColors.sadagreen,
        image: DecorationImage(
          image: AssetImage('assets/image/Store Banner.jpg'),
          fit: BoxFit.fitWidth,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 10, 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () async {
                    Navigator.pop(context);
                  },
                  child: Language.arLocal
                      ? Icon(
                          CupertinoIcons.chevron_right,
                          color: Colors.black,
                        )
                      : Icon(
                          CupertinoIcons.chevron_left,
                          color: Colors.black,
                        ),
                ),
                SizedBox(height: 10),
                homeChoose('Contact Us'.tr(), 20),
              ],
            ),
            /* Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  icon: Image.asset('assets/icons/Bag.png'),
                  onPressed: () {},
                ),
              ],
            ), */
          ],
        ),
      ),
    );
  }

  int convertPriorityToInt(String selectedPriority) {
    switch (selectedPriority) {
      case "Low":
        return 3;
        break;
      case "Medium":
        return 2;
        break;
      case "High":
        return 1;
        break;
    }
    return null;
  }
}
