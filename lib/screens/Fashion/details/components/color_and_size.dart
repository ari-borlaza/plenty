import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/constants.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/data/fashion_product.dart';
import 'package:plenty/database/cart.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:easy_localization/easy_localization.dart';

class ColorAndSize extends StatefulWidget {
  const ColorAndSize({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  _ColorAndSizeState createState() => _ColorAndSizeState();
}

class _ColorAndSizeState extends State<ColorAndSize> {
  String dropDownValue;
  List<String> sizeList = [
    'Small',
    'Medium ',
    'Large',
  ];
  var cartBox = Hive.box<Cart>('Cart');
  bool cart = false;
  int numOfItems = 1;

  @override
  void initState() {
    super.initState();
  }

  setFilters() {
    setState(() {
      dropDownValue = sizeList[0];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            itemTxt4("Size".tr() + ":", 15, AppColors.black, FontWeight.bold),
            SizedBox(
              width: 150,
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 0.5,
                      blurRadius: 3,
                      offset: Offset(0, 0), // changes position of shadow
                    ),
                  ],
                ),
                child: DropdownButtonFormField(
                  //elevation: 20,
                  //     itemHeight: 60,
                  iconEnabledColor: Colors.white,
                  icon: Icon(
                    Icons.expand_more,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(10),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200]),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      filled: true,
                      hintStyle: TextStyle(
                          fontFamily: 'Futura',
                          fontSize: 15.0,
                          fontWeight: FontWeight.w100,
                          color: Colors.black),
                      hintText: "Select Size".tr(),
                      fillColor: AppColors.white),
                  value: dropDownValue,
                  onChanged: (String value) {
                    setState(() {
                      dropDownValue = value;
                    });
                  },
                  items: sizeList
                      .map((sizeTitle) => DropdownMenuItem(
                          value: sizeTitle,
                          child: Text(
                            "$sizeTitle".tr(),
                            style: TextStyle(
                                fontFamily: 'Futura',
                                fontSize: 15.0,
                                fontWeight: FontWeight.w100,
                                color: Colors.black),
                          )))
                      .toList(),
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 15,
        ),
        itemTxt4(
            "Select Color".tr() + ":", 15, AppColors.black, FontWeight.bold),
        Row(
          children: <Widget>[
            ColorDot(
              color: Color(0xFF356C95),
              isSelected: true,
            ),
            ColorDot(color: Color(0xFFF8C078)),
            ColorDot(color: Color(0xFFA29B9B)),
          ],
        ),
        SizedBox(
          height: 15,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            itemTxt4(
                "Quantity".tr() + ":", 15, AppColors.black, FontWeight.bold),
            Row(
              children: <Widget>[
                plusMinusButton1(
                    icon: Icons.remove,
                    press: () {
                      if (numOfItems > 1) {
                        setState(() {
                          numOfItems--;
                        });
                      }
                    },
                    br: BorderRadius.horizontal(
                        right: Language.arLocal
                            ? Radius.circular(15)
                            : Radius.circular(0),
                        left: Language.arLocal
                            ? Radius.circular(0)
                            : Radius.circular(15))),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: kDefaultPaddin / 2),
                  child: Text(
                    // if our item is less  then 10 then  it shows 01 02 like that
                    numOfItems.toString().padLeft(2, "0"),
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                plusMinusButton1(
                    icon: Icons.add,
                    press: () {
                      setState(() {
                        numOfItems++;
                      });
                    },
                    br: BorderRadius.horizontal(
                        right: Language.arLocal
                            ? Radius.circular(0)
                            : Radius.circular(15),
                        left: Language.arLocal
                            ? Radius.circular(15)
                            : Radius.circular(0))),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 15,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              decoration: BoxDecoration(
                //  color: Colors.white,
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[200].withOpacity(0.5),
                    spreadRadius: 0.5,
                    blurRadius: 5,
                    offset: Offset(0, 0), // changes position of shadow
                  ),
                ],
              ),
              child: plentyFlatBtn6("Add to Bag".tr(), () async {
                //TODO: show dialog
                // if (cartBox.values.isNotEmpty &&
                //     (cartBox.values.toList()[0].category == "finedining" ||
                //         cartBox.values.toList()[0].category == "beauty")) {
                //   Alert(
                //     context: context,
                //     type: AlertType.warning,
                //     title: "Cart",
                //     desc:
                //         "your cart contains different category products. do you want to  clear your cart before proceed?",
                //     style: AlertStyle(
                //       titleStyle: TextStyle(fontSize: 12),
                //       descStyle: TextStyle(fontSize: 11),
                //     ),
                //     buttons: [
                //       DialogButton(
                //         child: Text(
                //           "CANCEL",
                //           style: TextStyle(color: Colors.white, fontSize: 14),
                //         ),
                //         onPressed: () => Navigator.pop(context),
                //         width: 80,
                //       ),
                //       DialogButton(
                //         child: Text(
                //           "OK",
                //           style: TextStyle(color: Colors.white, fontSize: 14),
                //         ),
                //         onPressed: () async {
                //           Navigator.pop(context);
                //           await cartBox.clear();
                //           await cartBox.put(
                //             widget.product.id,
                //             Cart(
                //               id: widget.product.id,
                //               image: widget.product.image,
                //               name: widget.product.title,
                //               size: widget.product.size.toString(),
                //               color: widget.product.color.value,
                //               price: widget.product.price.toString(),
                //               quantity: numOfItems,
                //               category: "fashion",
                //             ),
                //           );
                //         },
                //         width: 80,
                //       )
                //     ],
                //   ).show();
                // } else {
                //   if (cartBox.values.length > 0) {
                //     for (int i = 0; i < cartBox.values.length; i++) {
                //       if (cartBox.getAt(i).name == widget.product.title) {
                //         await cartBox.put(
                //           widget.product.id,
                //           Cart(
                //             id: widget.product.id,
                //             image: widget.product.image,
                //             name: widget.product.title,
                //             size: widget.product.size.toString(),
                //             color: widget.product.color.value,
                //             price: widget.product.price.toString(),
                //             quantity: cartBox.getAt(i).quantity + numOfItems,
                //             category: "fashion",
                //           ),
                //         );
                //         cart = true;
                //       }
                //     }
                //   }
                //   if (!cart) {
                //     await cartBox.put(
                //       widget.product.id,
                //       Cart(
                //         id: widget.product.id,
                //         image: widget.product.image,
                //         name: widget.product.title,
                //         size: widget.product.size.toString(),
                //         color: widget.product.color.value,
                //         price: widget.product.price.toString(),
                //         quantity: numOfItems,
                //         category: "fashion",
                //       ),
                //     );
                //   } else {
                //     cart = false;
                //   }
                // }
                if (cartBox.values.isNotEmpty &&
                    (cartBox.values.toList()[0].category == "finedining" ||
                        cartBox.values.toList()[0].category == "beauty")) {
                  Alert(
                    context: context,
                    type: AlertType.warning,
                    title: "Cart",
                    desc:
                        "Your bag contains different category products. Do you want to clear your bag to proceed?"
                            .tr(),
                    style: AlertStyle(
                      titleStyle: TextStyle(fontSize: 12),
                      descStyle: TextStyle(fontSize: 11),
                    ),
                    buttons: [
                      DialogButton(
                        child: Text(
                          "Cancel".toUpperCase(),
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                        onPressed: () => Navigator.pop(context),
                        width: 80,
                      ),
                      DialogButton(
                        child: Text(
                          "OK".tr(),
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                        onPressed: () async {
                          Navigator.pop(context);
                          await cartBox.clear();
                          await cartBox.put(
                            widget.product.id,
                            Cart(
                              pid: widget.product.id,
                              image: widget.product.image,
                              name: widget.product.title,
                              size: widget.product.size.toString(),
                              // color: widget.product.color.value,
                              price: widget.product.price.toString(),
                              quantity: numOfItems,
                              category: "fashion",
                            ),
                          );
                        },
                        width: 80,
                      )
                    ],
                  ).show();
                } else {
                  if (cartBox.values.length > 0) {
                    for (int i = 0; i < cartBox.values.length; i++) {
                      if (cartBox.getAt(i).name == widget.product.title) {
                        await cartBox.put(
                          widget.product.id,
                          Cart(
                            pid: widget.product.id,
                            image: widget.product.image,
                            name: widget.product.title,
                            size: widget.product.size.toString(),
                            // color: widget.product.color.value,
                            price: widget.product.price.toString(),
                            quantity: cartBox.getAt(i).quantity + numOfItems,
                            category: "fashion",
                          ),
                        );
                        cart = true;
                      }
                    }
                  }
                  if (!cart) {
                    await cartBox.put(
                      widget.product.id,
                      Cart(
                        pid: widget.product.id,
                        image: widget.product.image,
                        name: widget.product.title,
                        size: widget.product.size.toString(),
                        // color: widget.product.color.value,
                        price: widget.product.price.toString(),
                        quantity: numOfItems,
                        category: "fashion",
                      ),
                    );
                  } else {
                    cart = false;
                  }
                }
              }, AppColors.black, AppColors.white, Icon(Icons.shopping_bag)),
            ),
            plentyFlatBtn6(
              "Checkout".tr(),
              () async {
                // if (cartBox.values.isNotEmpty ||
                //     cartBox.values.toList()[0].category == "finedining" ||
                //     cartBox.values.toList()[0].category == "beauty") {
                //   Alert(
                //     context: context,
                //     type: AlertType.warning,
                //     title: "Cart",
                //     desc:
                //         "your cart contains different category products. do you want to  clear your cart before proceed?",
                //     style: AlertStyle(
                //       titleStyle: TextStyle(fontSize: 12),
                //       descStyle: TextStyle(fontSize: 11),
                //     ),
                //     buttons: [
                //       DialogButton(
                //         child: Text(
                //           "CANCEL",
                //           style: TextStyle(color: Colors.white, fontSize: 14),
                //         ),
                //         onPressed: () => Navigator.pop(context),
                //         width: 80,
                //       ),
                //       DialogButton(
                //         child: Text(
                //           "OK",
                //           style: TextStyle(color: Colors.white, fontSize: 14),
                //         ),
                //         onPressed: () async {
                //           Navigator.pop(context);
                //           await cartBox.clear();
                //           await cartBox.put(
                //             widget.product.id,
                //             Cart(
                //               id: widget.product.id,
                //               image: widget.product.image,
                //               name: widget.product.title,
                //               size: widget.product.size.toString(),
                //               color: widget.product.color.value,
                //               price: widget.product.price.toString(),
                //               quantity: numOfItems,
                //               category: "fashion",
                //             ),
                //           );
                //         },
                //         width: 80,
                //       )
                //     ],
                //   ).show();
                // } else {
                //   if (cartBox.values.length > 0) {
                //     for (int i = 0; i < cartBox.values.length; i++) {
                //       if (cartBox.getAt(i).name == widget.product.title) {
                //         await cartBox.put(
                //           widget.product.id,
                //           Cart(
                //             id: widget.product.id,
                //             image: widget.product.image,
                //             name: widget.product.title,
                //             size: widget.product.size.toString(),
                //             color: widget.product.color.value,
                //             price: widget.product.price.toString(),
                //             quantity: cartBox.getAt(i).quantity + numOfItems,
                //             category: "fashion",
                //           ),
                //         );
                //         cart = true;
                //       }
                //     }
                //   }
                //   if (!cart) {
                //     await cartBox.put(
                //       widget.product.id,
                //       Cart(
                //         id: widget.product.id,
                //         image: widget.product.image,
                //         name: widget.product.title,
                //         size: widget.product.size.toString(),
                //         color: widget.product.color.value,
                //         price: widget.product.price.toString(),
                //         quantity: numOfItems,
                //         category: "fashion",
                //       ),
                //     );
                //   } else {
                //     cart = false;
                //   }
                // }
                if (cartBox.values.isNotEmpty ||
                    cartBox.values.toList()[0].category == "finedining" ||
                    cartBox.values.toList()[0].category == "beauty") {
                  Alert(
                    context: context,
                    type: AlertType.warning,
                    title: "Cart".tr(),
                    desc:
                        "Your bag contains different category products. Do you want to clear your bag to proceed?"
                            .tr(),
                    style: AlertStyle(
                      titleStyle: TextStyle(fontSize: 12),
                      descStyle: TextStyle(fontSize: 11),
                    ),
                    buttons: [
                      DialogButton(
                        child: Text(
                          "Cancel".tr().toUpperCase(),
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                        onPressed: () => Navigator.pop(context),
                        width: 80,
                      ),
                      DialogButton(
                        child: Text(
                          "OK",
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                        onPressed: () async {
                          Navigator.pop(context);
                          await cartBox.clear();
                          await cartBox.put(
                            widget.product.id,
                            Cart(
                              pid: widget.product.id,
                              image: widget.product.image,
                              name: widget.product.title,
                              size: widget.product.size.toString(),
                              // color: widget.product.color.value,
                              price: widget.product.price.toString(),
                              quantity: numOfItems,
                              category: "fashion",
                            ),
                          );
                        },
                        width: 80,
                      )
                    ],
                  ).show();
                } else {
                  if (cartBox.values.length > 0) {
                    for (int i = 0; i < cartBox.values.length; i++) {
                      if (cartBox.getAt(i).name == widget.product.title) {
                        await cartBox.put(
                          widget.product.id,
                          Cart(
                            pid: widget.product.id,
                            image: widget.product.image,
                            name: widget.product.title,
                            size: widget.product.size.toString(),
                            // color: widget.product.color.value,
                            price: widget.product.price.toString(),
                            quantity: cartBox.getAt(i).quantity + numOfItems,
                            category: "fashion",
                          ),
                        );
                        cart = true;
                      }
                    }
                  }
                  if (!cart) {
                    await cartBox.put(
                      widget.product.id,
                      Cart(
                        pid: widget.product.id,
                        image: widget.product.image,
                        name: widget.product.title,
                        size: widget.product.size.toString(),
                        // color: widget.product.color.value,
                        price: widget.product.price.toString(),
                        quantity: numOfItems,
                        category: "fashion",
                      ),
                    );
                  } else {
                    cart = false;
                  }
                }
              },
              AppColors.white,
              AppColors.black,
              Icon(Icons.add_shopping_cart),
            ),
          ],
        )
      ],
    );
  }
}

class ColorDot extends StatelessWidget {
  final Color color;
  final bool isSelected;
  const ColorDot({
    Key key,
    this.color,
    // by default isSelected is false
    this.isSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          /*  margin: EdgeInsets.only(
            top: kDefaultPaddin / 4,
            right: kDefaultPaddin / 2,
          ), */
          padding: EdgeInsets.all(5),
          //padding: EdgeInsets.all(2.5),
          height: 40,
          width: 40,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            /*  border: Border.all(
              color: isSelected ? color : Colors.transparent,
            ), */
          ),
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: color,
              shape: BoxShape.circle,
            ),
          ),
        ),
        Positioned(
          top: 8,
          left: 4,
          child: isSelected
              ? Container(
                  alignment: Alignment.center,
                  width: 16,
                  height: 16,
                  child: Icon(
                    Icons.check,
                    color: Colors.white,
                    size: 12,
                  ),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.green
                      /*  border: Border.all(
              color: isSelected ? color : Colors.transparent,
            ), */
                      ),
                )
              : SizedBox(),
        )
      ],
    );
  }
}
