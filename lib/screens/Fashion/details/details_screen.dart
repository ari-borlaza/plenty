import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/constants.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/data/fashion_product.dart';

import 'components/body.dart';

class DetailsScreen extends StatefulWidget {
  final Product product;

  const DetailsScreen({
    Key key,
    this.product,
  }) : super(key: key);

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  bool selected = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // each product have a color
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: buildAppBar(context),
      body: Body(product: widget.product),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      leading: IconButton(
        icon: Language.arLocal
            ? Icon(
                CupertinoIcons.chevron_right,
                color: Colors.black,
              )
            : Icon(
                CupertinoIcons.chevron_left,
                color: Colors.black,
              ),
        onPressed: () => Navigator.pop(context),
      ),
      actions: <Widget>[
        SizedBox(
          height: 40,
          child: IconButton(
            splashColor: Colors.transparent,
            onPressed: () {
              setState(() {
                selected = !selected;
              });
            },
            icon: selected
                ? Image.asset(
                    'assets/icons/Fav.png',
                  )
                : Image.asset(
                    'assets/icons/Fav2.png',
                    color: AppColors.red,
                  ),
          ),
        ),
        SizedBox(width: kDefaultPaddin / 2)
      ],
    );
  }
}
