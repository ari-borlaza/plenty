import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/data/fashion_product.dart';
import 'package:plenty/database/fave.dart';
import 'package:easy_localization/easy_localization.dart';

class ItemCard extends StatefulWidget {
  final Product product;

  final Function press;
  final Function(int) delProduct;
  const ItemCard({
    Key key,
    this.product,
    this.press,
    this.delProduct,
  }) : super(key: key);

  @override
  _ItemCardState createState() => _ItemCardState();
}

class _ItemCardState extends State<ItemCard> {
  Box<Fave> faveBox;
  bool selected = true;
  List<Product> favProduct = [];
  @override
  void initState() {
    super.initState();
    faveBox = Hive.box<Fave>('fave');
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.press,
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Hero(
                      tag: "${widget.product.id}",
                      child: Image.asset(
                        widget.product.image,
                      ),
                    ),
                  ),
                ),
                itemTxt1(
                    widget.product.title, 16, AppColors.black, FontWeight.w300),
                Padding(
                  padding: const EdgeInsets.fromLTRB(5, 5, 5, 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    children: [
                      Text(
                        'SAR'.tr(),
                        style: TextStyle(
                            fontFamily: 'Futura',
                            fontSize: 12,
                            color: AppColors.black,
                            fontWeight: FontWeight.w600),
                      ),
                      itemTxt2(" ${widget.product.price}", 15),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: 40,
                child: IconButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onPressed: () {},
                  icon: Image.asset('assets/icons/Bag.png'),
                ),
              ),
              SizedBox(
                height: 40,
                child: IconButton(
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  onPressed: () async {
                    print(widget.product.id);

                    if (faveBox.containsKey("prod_${widget.product.id}")) {
                      await faveBox.delete("prod_${widget.product.id}");
                      //   favProduct.removeWhere((e) => e.id == widget.product.id);
                      await widget.delProduct(widget.product.id);
                      Image.asset(
                        'assets/icons/Fav.png',
                      );
                      setState(() {});
                    } else {
                      faveBox.put(
                          "prod_${widget.product.id}", Fave(widget.product.id));
                      Image.asset(
                        'assets/icons/Fav2.png',
                        color: AppColors.red,
                      );
                    }
                    /* favProduct.removeWhere((e) => e.id == widget.product.id);
                    widget.delProduct(widget.product.id);
                     */ //widget.delProduct(widget.product.id);
                    // selected = !selected;
                    setState(() {});
                  },
                  icon: faveBox.containsKey("prod_${widget.product.id}")
                      ? Image.asset(
                          'assets/icons/Fav2.png',
                          color: AppColors.red,
                        )
                      : Image.asset(
                          'assets/icons/Fav.png',
                        ),
                ),
              ),
            ],
          )),
        ],
      ),
    );
  }
}
