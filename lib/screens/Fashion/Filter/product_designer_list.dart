import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/utils.dart';
import 'package:plenty/data/fashion_data_list.dart';
import 'package:plenty/data/fashion_product.dart';
import 'package:plenty/screens/Fashion/details/details_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import '../fashion_item_card.dart';

class ProductDesignerList extends StatefulWidget {
  @override
  _ProductDesignerListState createState() => _ProductDesignerListState();
}

class _ProductDesignerListState extends State<ProductDesignerList>
    with TickerProviderStateMixin {
  AnimationController animationController;
  List<Map<String, dynamic>> items = fashionStore[0]["item"];
  List<Tab> tabList = List();
  TabController _tabController;
  String description =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id neque libero. Donec finibus sem viverra.';
  String description1 = 'Lorem ipsum dolor sit amet';
  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 500) * 5, () {
      if (!mounted) {
        return;
      }
      setState(() {
        itemsCount += 10;
      });
    });
    tabList.add(new Tab(
      text: 'Dress',
    ));
    tabList.add(new Tab(
      text: 'Tops',
    ));
    tabList.add(new Tab(
      text: 'Bottoms',
    ));
    tabList.add(new Tab(
      text: 'Skirt',
    ));
    tabList.add(new Tab(
      text: 'Pants',
    ));
    _tabController = new TabController(vsync: this, length: tabList.length);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  int itemsCount = products.length;

  Widget fashionTab() {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      decoration: new BoxDecoration(color: AppColors.white),
      child: new TabBar(
          labelStyle: TextStyle(fontFamily: 'Futura',
            fontSize: 18,
          ),
          labelColor: AppColors.plentyblue,
          unselectedLabelColor: AppColors.semiGrey,
          isScrollable: true,
          controller: _tabController,
          indicatorColor: AppColors.plentyblue,
          indicatorSize: TabBarIndicatorSize.tab,
          tabs: tabList),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: Column(
        children: [
          appBarr(),
          Expanded(
            child: new TabBarView(
              controller: _tabController,
              children: <Widget>[
                gridProducts(context),
                gridProducts(context),
                gridProducts(context),
                gridProducts(context),
                gridProducts(context),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget gridProducts(BuildContext context) {
    return Container(
      child: LiveGrid(
        // shrinkWrap: true,
        // physics: NeverScrollableScrollPhysics(),
        reAnimateOnVisibility: true,
        itemCount: products.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 20,
          crossAxisSpacing: 5,
          childAspectRatio: 0.8,
        ),
        itemBuilder: animationItemBuilder(
          (index) => Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: ItemCard(
              product: products[index],
              press: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailsScreen(
                      product: products[index],
                    ),
                  )),
            ),
          ),
        ),
      ),
    );
  }

  Widget appBarr() {
    return Container(
      decoration: BoxDecoration(
        // color: AppColors.sadagreen,
        image: DecorationImage(
          image: AssetImage('assets/image/Store Banner.jpg'),
          fit: BoxFit.fitWidth,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 50, 10, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Language.arLocal
                  ? Icon(
                      CupertinoIcons.chevron_right,
                      color: Colors.black,
                    )
                  : Icon(
                      CupertinoIcons.chevron_left,
                      color: Colors.black,
                    ),
                ),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 40),
                  child: Text(
                    "Designer".tr()+": Bashayer",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),

                // Padding(
                //   padding: const EdgeInsets.only(bottom: 8.0),
                //   child: Column(
                //     children: [
                //       Container(
                //         height: 50,
                //         child: Image.asset('assets/images/logo.png'),
                //       ),
                //       appBartxt1('Store 1 Tagline', 8),
                //     ],
                //   ),
                // ),
                // IconButton(icon: Icon(Icons.attribution_outlined,
                //   color: Colors.black,),
                //   onPressed: (){
                //     Navigator.push(
                //         context,
                //         PageTransition(
                //             type: PageTransitionType.leftToRight,
                //             duration: Duration(milliseconds: 500),
                //             child: FilterScreen()));
                //   },
                //
                // ),
              ],
            ),
          ),
          fashionTab(),
        ],
      ),
    );
  }
}
