import 'dart:async';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:location/location.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/database/address.dart';
import 'package:plenty/size.dart';
import 'package:easy_localization/easy_localization.dart';

class NewAdd extends StatefulWidget {
  final String txt;

  const NewAdd({Key key, this.txt}) : super(key: key);
  @override
  _NewAddState createState() => _NewAddState();
}

class _NewAddState extends State<NewAdd> {
  TextEditingController bday = TextEditingController();
  String dropDownValue;
  GlobalKey<ScaffoldState> scaffold = GlobalKey<ScaffoldState>();
  var cartBox = Hive.box<Address>('Address');
  List<String> cityList = [
/*     'Abhā'.tr(),
    'Abqaiq'.tr(),
    'Al-Baḥah'.tr(),
    'Al-Dammām'.tr(),
    'Al-Hufūf'.tr(),
    'Al-Jawf'.tr(),
    'Al-Kharj'.tr(),
    'Al-Khubar'.tr(),
    'Al-Qaṭīf'.tr(),
    'Al-Ṭaʾif'.tr(),
    'ʿArʿar'.tr(),
    'Buraydah'.tr(),
    'Dhahran'.tr(),
    'Ḥāʾil'.tr(), */
    'Jiddah'.tr(),
    'Jīzān'.tr(),
    'Khamīs Mushayt'.tr(),
    'King Khalīd Military City'.tr(),
    'Mecca'.tr(),
    'Medina'.tr(),
    'Najrān'.tr(),
    'Ras Tanura'.tr(),
    'Riyadh'.tr(), /*
    'Sakākā'.tr(),
    'Tabūk'.tr(),
    'Yanbuʿ'.tr(), */
  ];

  Location _location = Location();
  bool _serviceEnabled;
  Completer<GoogleMapController> _controller = Completer();
  final Set<Marker> _markers = {};
  CameraPosition _kLake;
  LatLng currentLocation = LatLng(0, 0);
  LatLng cameraPosition = LatLng(0, 0);
  // List<dynamic> address = [];
  bool isLoading = true;
  TextEditingController address = TextEditingController();
  TextEditingController contact = TextEditingController();
  String label = "";

  String select;
  @override
  void initState() {
    // dropDownValue = cityList[0];
    _kLake = CameraPosition(
        bearing: 192.8334901395799,
        target: LatLng(24.455665, 54.668878),
        tilt: 59.440717697143555,
        zoom: 14.151926040649414);
    currentLocation = LatLng(24.455665, 54.668878);
    _markers.clear();
    addMarkers("1", LatLng(24.455665, 54.668878)).then((marker) {
      setState(() {
        _markers.add(marker);
      });
    });
    Future.delayed(
        Duration(seconds: 2),
        () => checkForService().then((value) {
              if (value) {
                _getCurrentLocation().then((value) {
                  currentLocation = LatLng(value.latitude, value.longitude);
                  moveto(value.latitude, value.longitude);
                  _markers.clear();
                  addMarkers("1", LatLng(value.latitude, value.longitude))
                      .then((marker) {
                    setState(() {
                      _markers.add(marker);
                      isLoading = false;
                    });
                    getLocationAddress(value.latitude, value.longitude);
                  });
                });
              } else {
                print("turn on location");
              }
            }).catchError((error) {
              print(error);
            }));
    super.initState();
  }

  setFilters() {
    setState(() {
      dropDownValue = cityList[0];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffold,
      backgroundColor: AppColors.white,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverPersistentHeader(
              pinned: true,
              floating: true,
              delegate: ContestTabHeader(
                appBarr(),
              ),
            ),
          ];
        },
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            TField(
              txt: 'Address line'.tr(),
              controller: address,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: homeChoose('Select City'.tr(), 15),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[200].withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 0), // changes position of shadow
                  ),
                ],
              ),
              child: DropdownButtonFormField(
                //elevation: 20,
                //     itemHeight: 60,
                iconEnabledColor: Colors.white,
                icon: Icon(
                  Icons.expand_more,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(10),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    filled: true,
                    hintStyle: TextStyle(
                        fontFamily: 'Futura',
                        fontSize: 15.0,
                        fontWeight: FontWeight.w100,
                        color: Colors.grey),
                    hintText: "Select City".tr(),
                    fillColor: AppColors.white),
                value: dropDownValue,
                onChanged: (String value) {
                  setState(() {
                    dropDownValue = value;
                  });
                },
                items: cityList
                    .map((sizeTitle) => DropdownMenuItem(
                        value: sizeTitle,
                        child: Text(
                          "$sizeTitle",
                          style: TextStyle(
                              fontFamily: 'Futura',
                              fontSize: 15.0,
                              fontWeight: FontWeight.w100,
                              color: Colors.black),
                        )))
                    .toList(),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            TField(
              txt: 'Contact Number'.tr(),
              controller: contact,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      homeChoose('Address Label'.tr(), 15),
                      SizedBox(),
                      InkWell(
                        onTap: () {
                          setState(() {
                            label = "Home";
                          });
                        },
                        child: Container(
                          height: 30,
                          width: 80,
                          padding: EdgeInsets.zero,
                          decoration: BoxDecoration(
                            color:
                                label == "Home" ? Colors.black : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            //border: Border.all(color: Colors.grey[200]),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[200].withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset:
                                    Offset(0, 0), // changes position of shadow
                              ),
                            ],
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Icon(
                                Icons.home,
                                color: label == "Home"
                                    ? Colors.white
                                    : Colors.black,
                              ),
                              homeChoose(
                                'Home'.tr(),
                                15,
                                label == "Home" ? Colors.white : Colors.black,
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            label = "Work";
                          });
                        },
                        child: Container(
                          height: 30,
                          width: 80,
                          padding: EdgeInsets.zero,
                          decoration: BoxDecoration(
                            color:
                                label == "Work" ? Colors.black : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            //border: Border.all(color: Colors.grey[200]),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[200].withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset:
                                    Offset(0, 0), // changes position of shadow
                              ),
                            ],
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Icon(
                                Icons.business_center,
                                color: label == "Work"
                                    ? Colors.white
                                    : Colors.black,
                              ),
                              homeChoose(
                                'Work'.tr(),
                                15,
                                label == "Work" ? Colors.white : Colors.black,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  homeChoose('Pin Location on Maps'.tr(), 15),
                ],
              ),
            ),
            Expanded(
              child: Card(
                elevation: 2.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Stack(
                    children: [
                      GoogleMap(
                        mapType: MapType.normal,
                        markers: _markers,
                        zoomControlsEnabled: false,
                        trafficEnabled: false,
                        compassEnabled: false,
                        myLocationButtonEnabled: false,
                        buildingsEnabled: true,
                        initialCameraPosition: _kLake,
                        gestureRecognizers: {
                          Factory<OneSequenceGestureRecognizer>(
                            () => EagerGestureRecognizer(),
                          ),
                        },
                        onTap: (location) => addMarkers("1", location).then(
                          (marker) => setState(() {
                            _markers.add(marker);
                          }),
                        ),
                        onCameraMove: (position) {
                          setState(() {
                            cameraPosition = position.target;
                          });
                        },
                        onCameraIdle: () {
                          if (cameraPosition.latitude != 0.0) {
                            addMarkers("1", cameraPosition).then(
                              (marker) => setState(() {
                                _markers.add(marker);
                              }),
                            );
                            getLocationAddress(cameraPosition.latitude,
                                cameraPosition.longitude);
                            cameraPosition = LatLng(0, 0);
                          }
                        },
                        onMapCreated: (GoogleMapController controller) {
                          _controller.complete(controller);
                        },
                      ),
                      Positioned(
                        bottom: 10,
                        right: 10,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            InkWell(
                              onTap: () async {
                                try {
                                  if (currentLocation == null) {
                                    _markers.clear();
                                    _getCurrentLocation()
                                        .then((LocationData locationData) {
                                      addMarkers("1", locationData as LatLng)
                                          .then(
                                        (marker) => setState(() {
                                          _markers.add(marker);
                                        }),
                                      );
                                      moveto(locationData.latitude,
                                          locationData.longitude);
                                      getLocationAddress(locationData.latitude,
                                          locationData.longitude);
                                    });
                                  } else {
                                    _markers.clear();
                                    addMarkers(
                                            "1",
                                            LatLng(currentLocation.latitude,
                                                currentLocation.longitude))
                                        .then(
                                      (marker) => setState(() {
                                        _markers.add(marker);
                                      }),
                                    );
                                    moveto(currentLocation.latitude,
                                        currentLocation.longitude);
                                    getLocationAddress(currentLocation.latitude,
                                        currentLocation.longitude);
                                  }
                                } catch (e) {
                                  print(e);
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.all(7),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black26,
                                          blurRadius: 10,
                                          spreadRadius: 1)
                                    ]),
                                child: Icon(
                                  Icons.my_location,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: isLoading,
                        child: BackdropFilter(
                          filter: ImageFilter.blur(
                            sigmaX: 5.0,
                            sigmaY: 5.0,
                          ),
                          child: Container(
                            color: Colors.black.withOpacity(0.1),
                          ),
                        ),
                      ),
                      Visibility(
                          visible: isLoading,
                          child: Positioned(
                              top: 90,
                              left: (MySize().width / 2) - 50,
                              child: Container(
                                  padding: EdgeInsets.all(30),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: CupertinoActivityIndicator()))),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(
                20,
                0,
                20,
                50,
              ),
              child: plentyFlatBtn4("Save Address".tr().toUpperCase(), () async {
                if (address.text.isEmpty ||
                    dropDownValue == null ||
                    contact.text.isEmpty ||
                    label.isEmpty) {
                  scaffold.currentState.showSnackBar(SnackBar(
                    content: Text("Some Fields are missing".tr()),
                    backgroundColor: Colors.black,
                    action: SnackBarAction(
                      label: "OK".tr(),
                      textColor: Colors.white,
                      onPressed: () {},
                    ),
                  ));
                } else {
                  await cartBox.add(Address(
                    address: address.text,
                    city: dropDownValue,
                    contactNumber: contact.text,
                    country: label,
                  ));
                  Navigator.pop(context);
                }
              }),
            ),
          ],
        ),
      ),
    );
  }

  Widget appBarr() {
    return Container(
      //height: 10,
      decoration: BoxDecoration(
        //  color: AppColors.sadagreen,
        image: DecorationImage(
          image: AssetImage('assets/image/Store Banner.jpg'),
          fit: BoxFit.fitWidth,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 10, 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(
                      context,
                      /*     PageTransition(
                            type: PageTransitionType.bottomToTop,
                            duration: Duration(milliseconds: 500),
                            child: HomePage())) */
                    );
                  },
                  child: Language.arLocal
                  ? Icon(
                      CupertinoIcons.chevron_right,
                      color: Colors.black,
                    )
                  : Icon(
                      CupertinoIcons.chevron_left,
                      color: Colors.black,
                    ),
                ),
                SizedBox(height: 10),
                homeChoose('Add Delivery Address'.tr(), 20),
              ],
            ),
          ],
        ),
      ),
    );
  }

  moveto(double lat, double lng) async {
    final GoogleMapController controller = await _controller.future;

    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        bearing: 192.8334901395799,
        target: LatLng(lat, lng),
        tilt: 0.440717697143555,
        zoom: 15.9)));
  }

  Future<Marker> addMarkers(String id, LatLng location) async {
    BitmapDescriptor descriptor = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(20, 20)), 'assets/icons/rsz_place.png');

    return Marker(markerId: MarkerId(id), position: location, icon: descriptor);
  }

  Future<bool> checkForService() async {
    PermissionStatus _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted == PermissionStatus.granted) {
        _serviceEnabled = await _location.serviceEnabled();
        if (!_serviceEnabled) {
          _serviceEnabled = await _location.requestService();
          if (!_serviceEnabled) {
            return false;
          }
        }
        if (_serviceEnabled) {
          return true;
        }
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  Future<LocationData> _getCurrentLocation() async {
    try {
      LocationData position =
          await _location.getLocation().catchError((onError) {
        print(onError);
      });
      print(position);
      return position;
    } catch (e) {
      print(e);
    }
  }

  getLocationAddress(double lat, double lng) async {
    try {
      Response response = await Dio().get(
          "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&key=AIzaSyBoF_1QHYpyKtjHsaNZXOetnnrQXgo2lOo");
      var data = response.data["results"][0]["formatted_address"];
      setState(() {
        address.text = data.toString();
      });
    } catch (e) {
      print(e);
    }
//    var googlePlace =
//        place.GooglePlace("AIzaSyBv45LZlF4Xupe_DRZdVDqCvc0li_Db8Jg");
//    place.NearBySearchResponse result = await googlePlace.search
//        .getNearBySearch(place.Location(lat: lat, lng: lng), 100);
//    for (place.SearchResult myResult in result.results) {
//      print(myResult.formattedAddress);
//    }
  }
}

class ContestTabHeader extends SliverPersistentHeaderDelegate {
  ContestTabHeader(
    this.appBarr,
  );
  final Widget appBarr;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return appBarr;
  }

  @override
  double get maxExtent => 125.0;

  @override
  double get minExtent => 125.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
