import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hive/hive.dart';
import 'package:page_transition/page_transition.dart';
import 'package:plenty/Network/network_connection.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/database/address.dart';
import 'package:plenty/database/cart.dart';
import 'package:plenty/provider/app_state.dart';
import 'package:provider/provider.dart';

import 'ThankYou.dart';

class OrderSummary extends StatefulWidget {
  final String txt;

  const OrderSummary({Key key, this.txt}) : super(key: key);
  @override
  _OrderSummaryState createState() => _OrderSummaryState();
}

class _OrderSummaryState extends State<OrderSummary> {
  String warningtext =
      "10% " + "Discount for MVP Application and Game Design".tr();

  GlobalKey<ScaffoldState> scaffkey = GlobalKey<ScaffoldState>();
  var cartBox = Hive.box<Cart>('Cart');
  var addressBox = Hive.box<Address>('Address');
  // List<CartListModel> cartdetail = [];
  // List<String> txt = ['Address lin]e 1', 'Select City', 'Contact Number'];
  String couponCode, couponErrorText, couponSuccessText;
  double couponValue = 0, tax;
  double walletValue = 0;
  double delivery = 0;
  double subtotal = 0;
  bool isLoading = false;
  int selectedaddindex;
  TextEditingController couponCodeCheckerController = TextEditingController();
  int total = 0;
  TextEditingController couponController = TextEditingController();
  String payType = "";

  int calculatetotal([int price, int quantity, bool firstTime = false]) {
    if (firstTime) {
      total += (price * quantity);
      subtotal += (price * quantity);
    } else {
      total = cartBox.values
          .toList()
          .map((e) => (int.parse(e.price) * e.quantity))
          .fold(0, (previousValue, element) => previousValue + element);
    }
    total =
        total + delivery.toInt() - couponValue.toInt() - walletValue.toInt();
    return total;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      key: scaffkey,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverPersistentHeader(
              pinned: true,
              floating: true,
              delegate: ContestTabHeader(
                appBarr(),
              ),
            ),
          ];
        },
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Table(
                  columnWidths: {0: FractionColumnWidth(.4)},
                  children: [
                    TableRow(
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 0.05)),
                        ),
                        children: [
                          Container(
                            padding: EdgeInsets.only(bottom: 5),
                            child: Text("Item(s)".tr(),
                                textAlign: TextAlign.center,
                                textScaleFactor: 0.9,
                                style: TextStyle(
                                    fontFamily: 'Futura',
                                    fontWeight: FontWeight.normal)),
                            alignment: Alignment.centerLeft,
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: Text("QTY".tr(),
                                textScaleFactor: 0.9,
                                style: TextStyle(
                                    fontFamily: 'Futura',
                                    fontWeight: FontWeight.normal)),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Text("Price".tr(),
                                textAlign: TextAlign.center,
                                textScaleFactor: 0.9,
                                style: TextStyle(
                                    fontFamily: 'Futura',
                                    fontWeight: FontWeight.normal)),
                          ),
                        ]),
                    ...cartBox.values
                        .toList()
                        .map(
                          (e) => TableRow(children: [
                            Container(
                                padding: EdgeInsets.symmetric(vertical: 8),
                                child: Text(
                                  "${e.name}",
                                  textAlign: TextAlign.start,
                                  textScaleFactor: 0.9,
                                  style: TextStyle(
                                    fontFamily: 'Futura',
                                    fontWeight: FontWeight.w600,
                                  ),
                                )),
                            Container(
                              padding: EdgeInsets.symmetric(vertical: 8),
                              child: Text(
                                "${e.quantity}",
                                textScaleFactor: 0.9,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: 'Futura',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            Container(
                                padding: EdgeInsets.symmetric(vertical: 8),
                                child: Text(
                                  "SAR".tr() +
                                      " ${int.parse(e.price) * e.quantity}",
                                  textScaleFactor: 0.9,
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    fontFamily: 'Futura',
                                    fontWeight: FontWeight.w600,
                                  ),
                                )),
                          ]),
                        )
                        .toList(),
                    TableRow(
                        decoration: BoxDecoration(
                          border: Border(top: BorderSide(width: 0.01)),
                        ),
                        children: [
                          Container(
                              padding: EdgeInsets.symmetric(vertical: 5),
                              child: Text("", textScaleFactor: 0.9)),
                          Container(
                              padding: EdgeInsets.symmetric(vertical: 5),
                              child: Text(
                                "Subtotal".tr(),
                                textAlign: TextAlign.start,
                                textScaleFactor: 0.9,
                                style: TextStyle(
                                  fontFamily: 'Futura',
                                ),
                              )),
                          Container(
                              padding: EdgeInsets.symmetric(vertical: 5),
                              child: Text(
                                "SAR".tr() + " ${calculatetotal()}",
                                textScaleFactor: 0.9,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontFamily: 'Futura',
                                ),
                              )),
                        ]),
                    TableRow(children: [
                      Container(
                          padding: EdgeInsets.symmetric(vertical: 5),
                          child: Text("", textScaleFactor: 0.9)),
                      Container(
                          padding: EdgeInsets.symmetric(vertical: 5),
                          child: Text(
                            "Delivery".tr(),
                            textScaleFactor: 0.9,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontFamily: 'Futura',
                            ),
                          )),
                      Container(
                          padding: EdgeInsets.symmetric(vertical: 5),
                          child: Text(
                            "SAR".tr() + " ${delivery.toStringAsFixed(0)}",
                            textScaleFactor: 0.9,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'Futura',
                            ),
                          )),
                    ]),
                    // TableRow(children: [
                    //   Container(
                    //       padding: EdgeInsets.symmetric(vertical: 5),
                    //       child: Text("", textScaleFactor: 0.9)),
                    //   Container(
                    //       padding: EdgeInsets.symmetric(vertical: 5),
                    //       child: Text(
                    //         "Coupon Code".tr(),
                    //         textAlign: TextAlign.start,
                    //         textScaleFactor: 0.9,
                    //         style: TextStyle(
                    //           fontFamily: 'Futura',
                    //         ),
                    //       )),
                    //   Container(
                    //       padding: EdgeInsets.symmetric(vertical: 5),
                    //       child: Text(
                    //           "SAR".tr() +
                    //               " - ${couponValue != null ? couponValue.toInt() : 0}",
                    //           textScaleFactor: 0.9,
                    //           textAlign: TextAlign.end,
                    //           style: TextStyle(
                    //             fontFamily: 'Futura',
                    //             color: Colors.red,
                    //           ))),
                    // ]),
                    // TableRow(children: [
                    //   Container(
                    //       padding: EdgeInsets.symmetric(vertical: 5),
                    //       child: Text("", textScaleFactor: 0.9)),
                    //   Container(
                    //       padding: EdgeInsets.symmetric(vertical: 5),
                    //       child: Text(
                    //         "Wallet Balance".tr(),
                    //         textAlign: TextAlign.start,
                    //         textScaleFactor: 0.9,
                    //         style: TextStyle(
                    //           fontFamily: 'Futura',
                    //         ),
                    //       )),
                    //   Container(
                    //       padding: EdgeInsets.symmetric(vertical: 5),
                    //       child: Text(
                    //           "SAR".tr() +
                    //               " - ${walletValue != null ? walletValue.toInt() : 0}",
                    //           textScaleFactor: 0.9,
                    //           textAlign: TextAlign.end,
                    //           style: TextStyle(
                    //             fontFamily: 'Futura',
                    //             color: Colors.red,
                    //           ))),
                    // ])
                    /*
                    couponValue > 0
                        ? TableRow(children: [
                            Container(
                                padding: EdgeInsets.symmetric(vertical: 5),
                                child: Text("", textScaleFactor: 0.9)),
                            Container(
                                padding: EdgeInsets.symmetric(vertical: 5),
                                child: Text(
                                  "Coupon Code",
                                  textAlign: TextAlign.start,
                                  textScaleFactor: 0.9,
                                  style: TextStyle(),
                                )),
                            Container(
                                padding: EdgeInsets.symmetric(vertical: 5),
                                child: Text(
                                    "SAR ${couponValue != null ? couponValue.toInt() : 0}",
                                    textScaleFactor: 0.9,
                                    textAlign: TextAlign.end,
                                    style: TextStyle(fontFamily: 'Futura',            color: Colors.red,
                                    ))),
                          ])
                        : TableRow(
                            children: [Container(), Container(), Container()]), */
                    // ,
                    TableRow(
                        decoration: BoxDecoration(
                          border: Border(top: BorderSide(width: 0.05)),
                        ),
                        children: [
                          Container(
                              padding: EdgeInsets.symmetric(vertical: 5),
                              child: Text("", textScaleFactor: 0.9)),
                          Container(
                              padding: EdgeInsets.only(top: 8),
                              child: Text(
                                "Total".tr(),
                                textAlign: TextAlign.start,
                                textScaleFactor: 0.9,
                                style: TextStyle(
                                  fontFamily: 'Futura',
                                ),
                              )),
                          Container(
                              padding: EdgeInsets.only(top: 8),
                              child: Text(
                                "SAR".tr() + " $total",
                                textScaleFactor: 0.9,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontFamily: 'Futura',
                                ),
                              )),
                        ]),
                  ],
                ),
              ),
              Container(
                margin:
                    EdgeInsets.only(left: 20, top: 10, right: 20, bottom: 10),
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[200].withOpacity(0.5),
                        spreadRadius: 3,
                        blurRadius: 2,
                        offset: Offset(0, 0),
                      ),
                    ],
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(9.0))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Row(
                    //   children: [
                    //     Expanded(
                    //       child: Padding(
                    //         padding: const EdgeInsets.fromLTRB(13.0, 8, 13, 3),
                    //         child: TextField(
                    //           controller: couponController,
                    //           decoration: InputDecoration(
                    //             hintText: 'Enter the coupon code...'.tr(),
                    //             border: InputBorder.none,
                    //             fillColor: AppColors.txtfields,
                    //             filled: true,
                    //             isDense: true,
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //     SizedBox(
                    //       height: 30,
                    //       child: Padding(
                    //         padding:
                    //             const EdgeInsets.only(right: 20.0, left: 20),
                    //         child: couponValue > 0
                    //             ? RaisedButton(
                    //                 onPressed: () async {
                    //                   couponValue = 0;
                    //                   couponController.text = "";
                    //                   setState(() {});
                    //                 },
                    //                 color: Colors.black,
                    //                 shape: RoundedRectangleBorder(
                    //                   borderRadius: BorderRadius.circular(3.0),
                    //                   side: BorderSide(),
                    //                 ),
                    //                 child: Text('Remove'.tr().toUpperCase(),
                    //                     style: TextStyle(
                    //                         fontFamily: 'Futura',
                    //                         fontSize: 14,
                    //                         color: Colors.white)),
                    //               )
                    //             : RaisedButton(
                    //                 onPressed: () async {
                    //                   scaffkey.currentState
                    //                       .hideCurrentSnackBar();
                    //                   if (couponCodeCheckerController.text ==
                    //                       null) {
                    //                     scaffkey.currentState.showSnackBar(
                    //                         SnackBar(
                    //                             content: Text(
                    //                                 'Please enter a coupon code'
                    //                                     .tr())));
                    //                   } else {
                    //                     scaffkey.currentState
                    //                         .showSnackBar(SnackBar(
                    //                       content: Text(
                    //                           'Applying the coupon code...'
                    //                               .tr()),
                    //                       duration: Duration(seconds: 1),
                    //                     ));
                    //                   }
                    //                 },
                    //                 color: AppColors.plentyblue1,
                    //                 shape: RoundedRectangleBorder(
                    //                   borderRadius: BorderRadius.circular(10.0),
                    //                   // side: BorderSide(),
                    //                 ),
                    //                 child: Text('Apply'.tr().toUpperCase(),
                    //                     style: TextStyle(
                    //                         fontFamily: 'Futura',
                    //                         fontSize: 12,
                    //                         color: Colors.white)),
                    //               ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    // Padding(
                    //   padding: const EdgeInsets.only(left: 22, bottom: 4),
                    //   child: Text(warningtext,
                    //       style: TextStyle(
                    //           fontFamily: 'Futura',
                    //           color: Colors.black,
                    //           fontSize: 8,
                    //           fontWeight: FontWeight.w600)),
                    // ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(20),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Select Payment Method'.tr(),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          payType = "Cash";
                        });
                      },
                      child: Container(
                          // height: 30,
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          margin: EdgeInsets.symmetric(vertical: 5),
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 3,
                                blurRadius: 3,
                                offset: Offset(0, 0),
                              ),
                            ],
                            color: payType == "Cash"
                                ? AppColors.plentyblue1
                                : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Row(
                            children: [
                              Icon(
                                payType == "Cash"
                                    ? Icons.check_circle
                                    : Icons.circle,
                                color: payType == "Cash"
                                    ? Colors.white
                                    : Colors.grey[200],
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Text('Cash On Delivery'.tr(),
                                  style: TextStyle(
                                    fontFamily: 'Futura',
                                    color: payType == "Cash"
                                        ? Colors.white
                                        : Colors.black,
                                    fontSize: 13,
                                  )),
                            ],
                          )),
                    ),
                    // InkWell(
                    //   onTap: () {
                    //     setState(() {
                    //       payType = "Card";
                    //     });
                    //   },
                    //   child: Container(
                    //       // height: 30,
                    //       padding: EdgeInsets.all(10),
                    //       alignment: Alignment.center,
                    //       margin: EdgeInsets.symmetric(vertical: 5),
                    //       decoration: BoxDecoration(
                    //         boxShadow: [
                    //           BoxShadow(
                    //             color: Colors.grey.withOpacity(0.2),
                    //             spreadRadius: 3,
                    //             blurRadius: 3,
                    //             offset: Offset(0, 0),
                    //           ),
                    //         ],
                    //         color: payType == "Card"
                    //             ? AppColors.plentyblue1
                    //             : Colors.white,
                    //         borderRadius: BorderRadius.circular(10),
                    //       ),
                    //       child: Row(
                    //         children: [
                    //           Icon(
                    //             payType == "Card"
                    //                 ? Icons.check_circle
                    //                 : Icons.circle,
                    //             color: payType == "Card"
                    //                 ? Colors.white
                    //                 : Colors.grey[200],
                    //           ),
                    //           SizedBox(
                    //             width: 15,
                    //           ),
                    //           Text('Credit/ Debit Card'.tr(),
                    //               style: TextStyle(
                    //                 fontFamily: 'Futura',
                    //                 color: payType == "Card"
                    //                     ? Colors.white
                    //                     : Colors.black,
                    //                 fontSize: 13,
                    //               )),
                    //         ],
                    //       )),
                    // ),
                    // InkWell(
                    //   onTap: () {
                    //     setState(() {
                    //       payType = "Wallet";
                    //     });
                    //   },
                    //   child: Container(
                    //       // height: 30,
                    //       padding: EdgeInsets.all(10),
                    //       alignment: Alignment.center,
                    //       margin: EdgeInsets.symmetric(vertical: 5),
                    //       decoration: BoxDecoration(
                    //         boxShadow: [
                    //           BoxShadow(
                    //             color: Colors.grey.withOpacity(0.2),
                    //             spreadRadius: 3,
                    //             blurRadius: 3,
                    //             offset: Offset(0, 0),
                    //           ),
                    //         ],
                    //         color: payType == "Wallet"
                    //             ? AppColors.plentyblue1
                    //             : Colors.white,
                    //         borderRadius: BorderRadius.circular(10),
                    //       ),
                    //       child: Row(
                    //         children: [
                    //           Icon(
                    //             payType == "Wallet"
                    //                 ? Icons.check_circle
                    //                 : Icons.circle,
                    //             color: payType == "Wallet"
                    //                 ? Colors.white
                    //                 : Colors.grey[200],
                    //           ),
                    //           SizedBox(
                    //             width: 15,
                    //           ),
                    //           Text('Use My Wallet Balance'.tr(),
                    //               style: TextStyle(
                    //                 fontFamily: 'Futura',
                    //                 color: payType == "Wallet"
                    //                     ? Colors.white
                    //                     : Colors.black,
                    //                 fontSize: 13,
                    //               )),
                    //         ],
                    //       )),
                    // ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  20,
                  0,
                  20,
                  50,
                ),
                child:
                    plentyFlatBtn4("Place Order".tr().toUpperCase(), () async {
                  if (payType.isEmpty) {
                    scaffkey.currentState.showSnackBar(SnackBar(
                      content: Text("Please Select Payment Method".tr()),
                      backgroundColor: Colors.black,
                      action: SnackBarAction(
                        label: "OK".tr(),
                        textColor: Colors.white,
                        onPressed: () {},
                      ),
                    ));
                  } else {
                    List<Map<String, dynamic>> temp = [];
                    cartBox.values.forEach((e) {
                      temp.add(e.toJson());
                    });
                    print(addressBox
                        .getAt(Provider.of<AppState>(context, listen: false)
                            .selectedAddress)
                        .address);
                    FormData fd = new FormData.fromMap({
                      "total_amount": total,
                      "amount_due": total,
                      "delivery_location": addressBox
                          .getAt(Provider.of<AppState>(context, listen: false)
                              .selectedAddress)
                          .address,
                      "orderdetails": temp
                    });
                    final response = await AppApi().apiPost("orders", fd);
                    if (response.statusCode == 200) {
                      if (response.data["success"] == true) {
                        cartBox.clear();
                        Provider.of<AppState>(context, listen: false)
                            .clearIndex();
                        Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.topToBottom,
                                duration: Duration(milliseconds: 1000),
                                child: ThankYou()));
                      } else {
                        scaffkey.currentState.showSnackBar(SnackBar(
                          content: Text(
                              "An error has occurred. Please try again later"
                                  .tr()),
                          backgroundColor: Colors.black,
                          action: SnackBarAction(
                            label: "OK".tr(),
                            textColor: Colors.white,
                            onPressed: () {},
                          ),
                        ));
                      }
                    }
                  }
                  // add api here
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget appBarr() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/image/Store Banner.jpg'),
          fit: BoxFit.fitWidth,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 10, 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(
                      context,
                    );
                  },
                  child: Language.arLocal
                      ? Icon(
                          CupertinoIcons.chevron_right,
                          color: Colors.black,
                        )
                      : Icon(
                          CupertinoIcons.chevron_left,
                          color: Colors.black,
                        ),
                ),
                SizedBox(height: 10),
                homeChoose('Order Summary'.tr(), 20),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class TField extends StatefulWidget {
  TField({Key key, this.txt}) : super(key: key);
  final String txt;
  @override
  _TFieldState createState() => _TFieldState();
}

class _TFieldState extends State<TField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          homeChoose(widget.txt, 15),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
            child: TextFormField(
              style: TextStyle(
                  fontFamily: 'Futura',
                  fontSize: 20.0,
                  fontWeight: FontWeight.w100,
                  color: Colors.black),
              decoration: InputDecoration(
                hintStyle: TextStyle(
                    fontFamily: 'Futura',
                    fontSize: 20.0,
                    fontWeight: FontWeight.w100,
                    color: Colors.white),
                filled: true,
                fillColor: Colors.grey[100],
                contentPadding:
                    const EdgeInsets.only(left: 10.0, bottom: 8.0, top: 8.0),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(10),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ContestTabHeader extends SliverPersistentHeaderDelegate {
  ContestTabHeader(
    this.appBarr,
  );
  final Widget appBarr;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return appBarr;
  }

  @override
  double get maxExtent => 125.0;

  @override
  double get minExtent => 125.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
