import 'package:auto_animated/auto_animated.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:page_transition/page_transition.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/config/utils.dart';
import 'package:plenty/data/fashion_product.dart';
import 'package:plenty/database/address.dart';
import 'package:plenty/provider/app_state.dart';
import 'package:plenty/screens/Checkout/NewAdd.dart';
import 'package:plenty/screens/Checkout/OrderSummary.dart';
import 'package:provider/provider.dart';

class SelectAdd extends StatefulWidget {
  final Product product;

  const SelectAdd({Key key, this.product}) : super(key: key);
  @override
  _SelectAddState createState() => _SelectAddState();
}

class _SelectAddState extends State<SelectAdd> {
  // List<Map<String, dynamic>> address = deliveryAddress[0]["address"];

  int select = -1;

  var cartBox = Hive.box<Address>('Address');
  GlobalKey<ScaffoldState> scaffold = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffold,
      backgroundColor: AppColors.white,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverPersistentHeader(
              pinned: true,
              floating: true,
              delegate: ContestTabHeader(
                appBarr(),
              ),
            ),
          ];
        },
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            //SizedBox(height: 10),
            cartBox.values.length > 0
                ? Expanded(
                    child: LiveList(
                      showItemInterval: Duration(milliseconds: 150),
                      showItemDuration: Duration(milliseconds: 350),
                      padding: EdgeInsets.all(16),
                      reAnimateOnVisibility: true,
                      scrollDirection: Axis.vertical,
                      itemCount: cartBox.values.length,
                      itemBuilder: animationItemBuilder(
                        (index) => addList(cartBox.getAt(index), index),
                        padding: EdgeInsets.symmetric(vertical: 8),
                      ),
                    ),
                  )
                : Expanded(
                    child: Center(
                    child: Text("Please Add Address".tr()),
                  )),
            InkWell(
              onTap: () {
                /*  setState(() {
                  select = '';
                }); */
                Navigator.push(
                        context,
                        PageTransition(
                            type: PageTransitionType.topToBottom,
                            duration: Duration(milliseconds: 500),
                            child: NewAdd()))
                    .then((value) {
                  setState(() {});
                });
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.grey[200]),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 3,
                        blurRadius: 3,
                        offset: Offset(0, 0),
                      ),
                    ],
                  ),
                  height: 40,
                  //    decoration: kBoxDecoration,
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      itemTxt2("Add New Address".tr().toUpperCase(), 12),
                      Icon(
                        Icons.add,
                        color: Colors.black,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // subTotal("Subtotal"),
            Padding(
              padding: const EdgeInsets.fromLTRB(
                20,
                0,
                20,
                50,
              ),
              child: plentyFlatBtn4("Continue".tr().toUpperCase(), () {
                if (select != -1 && cartBox.values.length > 0) {
                  Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.topToBottom,
                          duration: Duration(milliseconds: 500),
                          child: OrderSummary()));
                } else {
                  scaffold.currentState.showSnackBar(
                    SnackBar(
                      content: Text("Please Select An Address".tr()),
                      backgroundColor: Colors.black,
                      action: SnackBarAction(
                        label: "OK".tr(),
                        textColor: Colors.white,
                        onPressed: () {},
                      ),
                    ),
                  );
                }
              }),
            ),
          ],
        ),
      ),
    );
  }

  Widget appBarr() {
    return Container(
      //height: 10,
      decoration: BoxDecoration(
        //  color: AppColors.sadagreen,
        image: DecorationImage(
          image: AssetImage('assets/image/Store Banner.jpg'),
          fit: BoxFit.fitWidth,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 10, 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(
                      context,
                      /*     PageTransition(
                            type: PageTransitionType.bottomToTop,
                            duration: Duration(milliseconds: 500),
                            child: HomePage())) */
                    );
                  },
                  child: Language.arLocal
                      ? Icon(
                          CupertinoIcons.chevron_right,
                          color: Colors.black,
                        )
                      : Icon(
                          CupertinoIcons.chevron_left,
                          color: Colors.black,
                        ),
                ),
                SizedBox(height: 10),
                homeChoose('Select Delivery Address'.tr(), 20),
              ],
            ),
            /* Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  icon: Image.asset('assets/icons/Bag.png'),
                  onPressed: () {},
                ),
              ],
            ), */
          ],
        ),
      ),
    );
  }

  Widget addList(Address address, int index) {
    return InkWell(
      onTap: () {
        /*   Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.topToBottom,
                duration: Duration(milliseconds: 500),
                child: FoodItemPage())); */
      },
      child: Stack(
        children: [
          InkWell(
            onTap: () {
              setState(() {
                select = index;
                Provider.of<AppState>(context, listen: false).setIndex(index);
              });
            },
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.grey[200]),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 3,
                    offset: Offset(0, 0),
                  ),
                ],
              ),
              height: 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          select == index
                              ? Icons.radio_button_on
                              : Icons.radio_button_off,
                          color: Colors.black,
                        ),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //  itemTxt2(address[index]['title'], 15),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: itemTxt2('Country'.tr(), 12),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: itemTxt2('City'.tr(), 12),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: itemTxt2('Address'.tr(), 12),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: itemTxt2("Contact Number".tr(), 12),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //  itemTxt2(address[index]['title'], 15),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: itemTxt2("KSA".tr(), 12),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: itemTxt2(address.city, 12),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: itemTxt2(address.address, 12),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: itemTxt2(address.contactNumber, 12),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 40,
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 3,
            right: 3,
            child: InkResponse(
              onTap: () async {
                await cartBox.deleteAt(index);
                setState(() {});
              },
              child: Row(
                children: [
                  Text(
                    'Delete'.tr(),
                    style: TextStyle(
                      fontFamily: 'Futura',
                      fontSize: 12,
                    ),
                  ),
                  Icon(Icons.delete_outline),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  // Row addRadioButton(
  //   int btnValue,
  // ) {
  //   return Row(
  //     mainAxisAlignment: MainAxisAlignment.start,
  //     children: <Widget>[
  //       Radio(
  //         activeColor: Colors.black,
  //         value: adrs[btnValue],
  //         groupValue: select,
  //         onChanged: (value) {
  //           setState(() {
  //             print(value);
  //             select = value;
  //           });
  //         },
  //       ),
  //     ],
  //   );
  // }
}

class ContestTabHeader extends SliverPersistentHeaderDelegate {
  ContestTabHeader(
    this.appBarr,
  );
  final Widget appBarr;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return appBarr;
  }

  @override
  double get maxExtent => 125.0;

  @override
  double get minExtent => 125.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
