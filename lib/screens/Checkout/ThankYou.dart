import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:page_transition/page_transition.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/screens/Home/home_page_new.dart';

class ThankYou extends StatefulWidget {
  @override
  _ThankYouState createState() => _ThankYouState();
}

class _ThankYouState extends State<ThankYou> {
  // var cartBox = Hive.box<Cart>('Cart');
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              /*   decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.black,
              ), */
              child: GestureDetector(
                onTap: () {
                  Navigator.pushReplacement(
                      context,
                      PageTransition(
                          type: PageTransitionType.topToBottom,
                          duration: Duration(milliseconds: 500),
                          child: HomePageNew()));
                },
                child: Language.arLocal
                    ? Icon(
                        CupertinoIcons.chevron_right,
                        color: Colors.black,
                      )
                    : Icon(
                        CupertinoIcons.chevron_left,
                        color: Colors.black,
                      ),
              ),
            ),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child: Lottie.asset(
                  'assets/images/your-order.json',
                ))
              ],
            ),
            Text(
              'Thank You For your order',
              style: TextStyle(
                fontFamily: 'Futura',
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(
              'Your order has been placed successfully!',
              style: TextStyle(
                fontFamily: 'Futura',
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.w400,
                height: 1,
              ),
            ),
            SizedBox(
              height: 30,
            ),
            // MaterialButton(
            //   color: Colors.black,
            //   elevation: 0,
            //   shape: RoundedRectangleBorder(
            //     borderRadius: BorderRadius.circular(8),
            //   ),
            //   minWidth: 260,
            //   onPressed: () async {
            //     // await cartBox.clear();
            //     // Navigator.of(context).pushReplacement(MaterialPageRoute(
            //     //     builder: (context) => MyOrderScreen(
            //     //           fromThankYou: true,
            //     //         )));
            //   },
            //   child: Text(
            //     'TRACK ORDER',
            //     style: TextStyle(fontFamily: 'Futura', color: Colors.white),
            //   ),
            // )
          ],
        ));
  }
}
