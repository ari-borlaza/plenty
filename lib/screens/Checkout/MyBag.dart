import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:page_transition/page_transition.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/constants.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/config/utils.dart';
import 'package:plenty/data/fashion_data_list.dart';
import 'package:plenty/data/fashion_product.dart';
import 'package:plenty/database/cart.dart';
import 'package:plenty/screens/Checkout/SelectAdd.dart';
import 'package:plenty/screens/Fashion/details/components/color_and_size.dart';
import 'package:easy_localization/easy_localization.dart';

class MyBag extends StatefulWidget {
  final Product product;

  const MyBag({Key key, this.product}) : super(key: key);
  @override
  _MyBagState createState() => _MyBagState();
}

class _MyBagState extends State<MyBag> {
  List<Map<String, dynamic>> items = fashionStore[0]["item"];
  var cartBox = Hive.box<Cart>('Cart');
  List<Cart> cart = [];
  double total = 0;
  @override
  void initState() {
    cart = [...cartBox.values.toList()];
    for (int i = 0; i < cartBox.values.length; i++)
      calculatetotal(
          int.parse(cartBox.getAt(i).price), cartBox.getAt(i).quantity, true);

    super.initState();
  }

  void calculatetotal([int price, int quantity, bool firstTime = false]) {
    if (firstTime) {
      total += (price * quantity);
    } else {
      total = cart
          .map((e) => (int.parse(e.price) * e.quantity))
          .fold(0, (previousValue, element) => previousValue + element);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverPersistentHeader(
              pinned: true,
              floating: true,
              delegate: ContestTabHeader(
                appBarr(),
              ),
            ),
          ];
        },
        body: cart.length > 0
            ? Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //SizedBox(height: 10),
                  Expanded(
                    child: LiveList(
                      showItemInterval: Duration(milliseconds: 150),
                      showItemDuration: Duration(milliseconds: 350),
                      padding: EdgeInsets.all(16),
                      reAnimateOnVisibility: true,
                      scrollDirection: Axis.vertical,
                      itemCount: cart.length,
                      itemBuilder: animationItemBuilder(
                        (index) => bagList(index),
                        padding: EdgeInsets.symmetric(vertical: 8),
                      ),
                    ),
                  ),
                  subTotal("Subtotal".tr()),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(
                      20,
                      0,
                      20,
                      50,
                    ),
                    child: plentyFlatBtn4("Checkout".tr().toUpperCase(), () {
                      Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.topToBottom,
                              duration: Duration(milliseconds: 500),
                              child: SelectAdd()));
                    }),
                  ),
                ],
              )
            : Center(
                child: Text("Your Bag is empty".tr()),
              ),
      ),
    );
  }

  Widget subTotal(String txt) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: AppColors.txtfields,
        //borderRadius: BorderRadius.all(Radius.circular(30)),
      ),
      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          homeChoose(txt, 15),
          homeChoose("SAR".tr() + ": ${total.toString()}", 15),
        ],
      ),
    );
  }

  Widget appBarr() {
    return Container(
      //height: 10,
      decoration: BoxDecoration(
        //  color: AppColors.sadagreen,
        image: DecorationImage(
          image: AssetImage('assets/image/Store Banner.jpg'),
          fit: BoxFit.fitWidth,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 10, 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(
                      context,
                    );
                  },
                  child: Language.arLocal
                      ? Icon(
                          CupertinoIcons.chevron_right,
                          color: Colors.black,
                        )
                      : Icon(
                          CupertinoIcons.chevron_left,
                          color: Colors.black,
                        ),
                ),
                SizedBox(height: 10),
                homeChoose('My Bag'.tr(), 20),
              ],
            ),
            /* Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  icon: Image.asset('assets/icons/Bag.png'),
                  onPressed: () {},
                ),
              ],
            ), */
          ],
        ),
      ),
    );
  }

  Widget bagList(int index) {
    return InkWell(
      onTap: () {
        /*   Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.topToBottom,
                duration: Duration(milliseconds: 500),
                child: FoodItemPage())); */
      },
      child: Stack(
        overflow: Overflow.visible,
        children: [
          Positioned(
            top: -15,
            right: 0,
            child: InkWell(
              onTap: () async {
                cart.removeAt(index);
                await cartBox.deleteAt(index);
                calculatetotal();
                setState(() {});
              },
              child: Icon(
                CupertinoIcons.clear_thick_circled,
                color: Colors.red,
              ),
            ),
          ),
          Container(
            height: 120,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 28.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          //  itemTxt2(items[index]['title'], 15),

                          itemTxt2(cartBox.values.toList()[index].name, 15),
                          Text(
                            'Size'.tr() +
                                ': ${cartBox.values.toList()[index].size}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontFamily: 'Futura',
                                fontSize: 12,
                                color: AppColors.grey,
                                fontWeight: FontWeight.normal),
                          ),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              itemTxt4('Color'.tr() + ':', 12, AppColors.grey,
                                  FontWeight.normal),
                              SizedBox(
                                height: 25,
                                width: 25,
                                child: ColorDot(
                                  color: Color(
                                      0xFFCC00), //cartBox.values.toList()[index].color
                                ),
                              ),
                            ],
                          ),
                          RichText(
                            text: TextSpan(
                                text: 'SAR'.tr(),
                                style: TextStyle(
                                    color: Colors.black, fontSize: 12),
                                children: <TextSpan>[
                                  TextSpan(
                                    text: cartBox.values.toList()[index].price,
                                    style: TextStyle(
                                        fontFamily: 'Futura',
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold),
                                  )
                                ]),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            children: <Widget>[
                              plusMinusButton1(
                                  icon: Icons.remove,
                                  press: () async {
                                    if (cart[index].quantity > 1) {
                                      setState(() {
                                        cart[index].quantity--;
                                      });

                                      await cartBox.put(
                                          cart[index].key,
                                          Cart(
                                            pid: cart[index].pid,
                                            image: cart[index].image,
                                            name: cart[index].name,
                                            price: cart[index].price.toString(),
                                            quantity: cart[index].quantity,
                                            size: cart[index].size,
                                            addsOn: cart[index].addsOn,
                                            sizeID: cart[index].sizeID,
                                            key: cart[index].key,
                                            category: cart[index].category,
                                            shopID: cart[index].shopID,
                                          ));
                                      calculatetotal();
                                    }
                                  },
                                  br: BorderRadius.horizontal(
                                      right: Language.arLocal
                                          ? Radius.circular(15)
                                          : Radius.circular(0),
                                      left: Language.arLocal
                                          ? Radius.circular(0)
                                          : Radius.circular(15))),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: kDefaultPaddin / 2),
                                child: Text(
                                  // if our item is less  then 10 then  it shows 01 02 like that
                                  cartBox.values
                                      .toList()[index]
                                      .quantity
                                      .toString()
                                      .padLeft(2, "0"),
                                  style: Theme.of(context).textTheme.headline6,
                                ),
                              ),
                              plusMinusButton1(
                                  icon: Icons.add,
                                  press: () async {
                                    setState(() {
                                      cart[index].quantity++;
                                    });
                                    await cartBox.put(
                                        cart[index].key,
                                        Cart(
                                          pid: cart[index].pid,
                                          image: cart[index].image,
                                          name: cart[index].name,
                                          price: cart[index].price.toString(),
                                          quantity: cart[index].quantity,
                                          size: cart[index].size,
                                          addsOn: cart[index].addsOn,
                                          sizeID: cart[index].sizeID,
                                          key: cart[index].key,
                                          category: cart[index].category,
                                          shopID: cart[index].shopID,
                                        ));
                                    calculatetotal();

                                    // else {
                                    //   Utils.showSnackBar(
                                    //       key,
                                    //       "You Have already select the max quantity",
                                    //           () {});
                                    // }
                                  },
                                  br: BorderRadius.horizontal(
                                      right: Language.arLocal
                                          ? Radius.circular(0)
                                          : Radius.circular(15),
                                      left: Language.arLocal
                                          ? Radius.circular(15)
                                          : Radius.circular(0))),
                            ],
                          ),
                        ],
                      ),
                      Container(
                        width: 100,
                        height: 100,
                        //margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.transparent,
                            image: DecorationImage(
                              image: cartBox.values
                                      .toList()[index]
                                      .image
                                      .contains("assets")
                                  ? AssetImage(
                                      cartBox.values.toList()[index].image,
                                    )
                                  : NetworkImage(
                                      cartBox.values.toList()[index].image,
                                    ),
                              fit: BoxFit
                                  .cover, /*
                              colorFilter: ColorFilter.mode(
                                  Colors.white.withOpacity(0.9), BlendMode.lighten), */
                            )),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Divider(
                    color: Colors.black,
                    height: 1,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ContestTabHeader extends SliverPersistentHeaderDelegate {
  ContestTabHeader(
    this.appBarr,
  );
  final Widget appBarr;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return appBarr;
  }

  @override
  double get maxExtent => 125.0;

  @override
  double get minExtent => 125.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
