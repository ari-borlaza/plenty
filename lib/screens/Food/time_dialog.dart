// import 'package:dio/dio.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:plenty/Network/network_connection.dart';
// import 'package:plenty/config/constants.dart';
//
// String tapText = "";
//
// class TimeDialogScreen extends StatefulWidget {
//   @override
//   _TimeDialogScreenState createState() => _TimeDialogScreenState();
// }
//
// class _TimeDialogScreenState extends State<TimeDialogScreen> {
//   // List<TimeSlotModel> timeSlotModel = [];
//   bool isLoading = true;
//   @override
//   void initState() {
//     tapText = "";
//     fetchTimeSlots();
//     super.initState();
//   }
//
//   fetchTimeSlots() async {
// //    print(BookingModel.selectedService);
//     Response response = await AppApi.instance.apiGet('timeslots',
//         queryParameters: {"service_id": ServiceModel.selectedService});
//     if (response.statusCode.toString().startsWith("2")) {
//       if (response.data != null && response.data["error"] == null) {
//         print(response.data);
//         for (Map<String, dynamic> data in response.data['timeslots']) {
//           timeSlotModel.add(TimeSlotModel.fromJson(data));
//         }
//
//         setState(() {
//           isLoading = false;
//         });
//       } else {
//         setState(() {
//           isLoading = false;
//         });
//       }
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 450,
//       padding: EdgeInsets.only(bottom: 20, top: 20),
//       decoration: BoxDecoration(
//         color: Colors.white,
//         borderRadius: BorderRadius.circular(20),
//       ),
//       child: isLoading
//           ? Center(
//               child: SpinKitPulse(
//                 color: Theme.of(context).primaryColor,
//                 size: 80.0,
//               ),
//             )
//           : timeSlotModel.isEmpty
//               ? Center(
//                   child: Text(
//                     "No Slots Available",
//                     textScaleFactor: 1.5,
//                   ),
//                 )
//               : Column(
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   mainAxisSize: MainAxisSize.min,
//                   children: [
//                     Padding(
//                       padding: const EdgeInsets.only(bottom: 20.0),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.start,
//                         children: [
//                           Expanded(
//                             child: Align(
//                               alignment: Alignment.centerLeft,
//                               child: FlatButton.icon(
//                                 label: Text(
//                                   '',
//                                   style: TextStyle(
//                                       color: Colors.black,
//                                       fontWeight: FontWeight.w600,
//                                       fontSize: 15),
//                                 ),
//                                 onPressed: () {
//                                   Navigator.pop(context, "false");
//                                 },
//                                 icon: Icon(
//                                   Icons.arrow_back_ios,
//                                   size: 20,
//                                 ),
//                               ),
//                             ),
//                           ),
//                           Expanded(
//                             flex: 2,
//                             child: Text(
//                               'Select Time',
//                               textAlign: TextAlign.start,
//                               style: TextStyle(
//                                   fontWeight: FontWeight.w600, fontSize: 23),
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                     Expanded(
//                         child: ListView(
//                       children: [
//                         if (timeSlotModel
//                             .any((element) => element.sched == "M")) ...{
//                           ReusablePadding(times: "Morning"),
//                           ReusableGrid(
//                             times: timeSlotModel
//                                 .where((element) => element.sched == "M")
//                                 .toList(),
//                             pressMe: (TimeSlotModel value) {
//                               if (value.available == 1) {
//                                 setState(() {
//                                   tapText = value.time;
//                                 });
// //                    Navigator.pop(context, tapText);
//                                 ServiceModel.time = tapText;
//                                 int id = timeSlotModel
//                                     .firstWhere(
//                                         (element) => element.time == tapText)
//                                     .id;
//
//                                 if (ServiceModel.specialist == null) {
//                                   showDialog(
//                                       context: context,
//                                       builder: (context) => Dialog(
//                                             child: SpecialistDialog(
//                                               id: id,
//                                             ),
//                                             insetPadding: EdgeInsets.symmetric(
//                                                 vertical: 24, horizontal: 20),
//                                             shape: RoundedRectangleBorder(
//                                                 borderRadius:
//                                                     BorderRadius.circular(20)),
//                                           )).then((value) {
//                                     if (value != null && value == "yes")
//                                       Navigator.pop(context, "yes");
//                                   });
//                                 } else {
//                                   Navigator.pop(context, "yes");
//                                 }
//                               }
//                             },
//                           ),
//                           SizedBox(
//                             height: 8,
//                           ),
//                         },
//                         if (timeSlotModel
//                             .any((element) => element.sched == "A")) ...{
//                           ReusablePadding(times: "Afternoon"),
//                           ReusableGrid(
//                             times: timeSlotModel
//                                 .where((element) => element.sched == "A")
//                                 .toList(),
//                             pressMe: (TimeSlotModel value) {
//                               if (value.available == 1) {
//                                 setState(() {
//                                   tapText = value.time;
//                                 });
// //                    Navigator.pop(context, tapText);
//                                 ServiceModel.time = tapText;
//                                 int id = timeSlotModel
//                                     .firstWhere(
//                                         (element) => element.time == tapText)
//                                     .id;
//                                 // if (ServiceModel.specialist == null) {
//                                 //   showDialog(
//                                 //       context: context,
//                                 //       builder: (context) => Dialog(
//                                 //             child: SpecialistDialog(
//                                 //               id: id,
//                                 //             ),
//                                 //             insetPadding: EdgeInsets.symmetric(
//                                 //                 vertical: 24, horizontal: 20),
//                                 //             shape: RoundedRectangleBorder(
//                                 //                 borderRadius:
//                                 //                     BorderRadius.circular(20)),
//                                 //           )).then((value) {
//                                 //     if (value != null && value == "yes")
//                                 //       Navigator.pop(context, "yes");
//                                 //   });
//                                 // } else {
//                                 Navigator.pop(context, "yes");
//                                 // }
//                               }
//                             },
//                           ),
//                           SizedBox(
//                             height: 8,
//                           ),
//                         },
//                         if (timeSlotModel
//                             .any((element) => element.sched == "E")) ...{
//                           ReusablePadding(times: "Evening"),
//                           ReusableGrid(
//                             times: timeSlotModel
//                                 .where((element) => element.sched == "E")
//                                 .toList(),
//                             pressMe: (TimeSlotModel value) {
//                               if (value.available == 1) {
//                                 setState(() {
//                                   tapText = value.time;
//                                 });
// //                    Navigator.pop(context, tapText);
//                                 ServiceModel.time = tapText;
//                                 int id = timeSlotModel
//                                     .firstWhere(
//                                         (element) => element.time == tapText)
//                                     .id;
//                                 Navigator.pop(context, "yes");
//                               }
//                             },
//                           ),
//                         },
//                       ],
//                     )),
//                   ],
//                 ),
//     );
//   }
// }
//
// class ReusableGrid extends StatelessWidget {
//   const ReusableGrid({
//     Key key,
//     @required this.times,
//     @required this.pressMe,
//   });
//
//   final List<TimeSlotModel> times;
//   final Function(TimeSlotModel) pressMe;
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(left: 11.0),
//       child: Wrap(
//           children: times
//               .map((e) => InkResponse(
//                     onTap: () {
//                       print(e);
//                       pressMe(e);
//                     },
//                     child: Container(
//                       width: 80,
//                       height: 30,
//                       alignment: Alignment.center,
//                       margin: EdgeInsets.only(right: 10, bottom: 10),
//                       decoration: BoxDecoration(
//                           color: tapText == e.time
//                               ? Theme.of(context).primaryColor
//                               : unSelectedButtonColor.withOpacity(0.7),
//                           borderRadius: BorderRadius.circular(20)),
//                       child: e.available == 1
//                           ? Text(
//                               e.time,
//                               style: TextStyle(color: Colors.black),
//                             )
//                           : CustomPaint(
//                               painter: DrawLine(),
//                               child: Text(
//                                 e.time,
//                                 style: TextStyle(color: Colors.black),
//                               ),
//                             ),
//                     ),
//                   ))
//               .toList()),
//     );
//   }
// }
//
// class ReusablePadding extends StatelessWidget {
//   const ReusablePadding({
//     Key key,
//     @required this.times,
//   }) : super(key: key);
//
//   final String times;
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(bottom: 8.0, left: 16, right: 16),
//       child: Text(
//         times,
//         style: TextStyle(
//             fontWeight: FontWeight.w600,
//             color: Colors.black87.withOpacity(0.7)),
//       ),
//     );
//   }
// }
//
// class DrawLine extends CustomPainter {
//   @override
//   void paint(Canvas canvas, Size size) {
//     Paint line = new Paint()
//       ..color = Colors.red
//       ..strokeCap = StrokeCap.round
//       ..style = PaintingStyle.fill
//       ..strokeWidth = 2;
//     canvas.drawLine(Offset(size.width, 0), Offset(0, size.height), line);
//   }
//
//   @override
//   bool shouldRepaint(CustomPainter oldDelegate) {
//     // TODO: implement shouldRepaint
//     return true;
//   }
// }
