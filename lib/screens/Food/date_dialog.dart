import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:intl/intl.dart';
import 'package:plenty/config/constants.dart';
import 'package:plenty/size.dart';
import 'package:easy_localization/easy_localization.dart';

class ShowDateDialog extends StatefulWidget {
  @override
  _ShowDateDialogState createState() => _ShowDateDialogState();
}

class _ShowDateDialogState extends State<ShowDateDialog> {
  DateTime _currentDate = DateTime.now();
  DateTime _currentDate2;
  String _currentMonth = DateFormat.yMMM().format(DateTime.now());
  DateTime _targetDateTime = DateTime.now();
  final int maxSelectedDays = 30; //TODO: MAKE IT TO 30
  CalendarCarousel _calendarCarousel, _calendarCarouselNoHeader;
  @override
  Widget build(BuildContext context) {
    _calendarCarouselNoHeader = CalendarCarousel(
      onDayPressed: (DateTime date, _) {
        print(date);
        this.setState(() => _currentDate2 = date);
        // ServiceModel.date = date;
        // showDialog(
        //     context: context,
        //     builder: (context) => Dialog(
        //           insetPadding:
        //               EdgeInsets.symmetric(vertical: 24, horizontal: 20),
        //           shape: RoundedRectangleBorder(
        //               borderRadius: BorderRadius.circular(20)),
        //           child: TimeDialogScreen(),
        //         )).then(
        //   (value) {
        //     if (value != null && value == "yes") {
        //       Navigator.pop(context, "yes");
        //     }
        //   },
        // );
      },
      daysHaveCircularBorder: true,
      showOnlyCurrentMonthDate: false,
      childAspectRatio: MySize().shortestSide > 600 ? 2.0 : 1.0,
      weekdayTextStyle: TextStyle(
          color: Colors.black, fontSize: 15, fontWeight: FontWeight.w500),
      weekFormat: false,
      todayBorderColor: unSelectedButtonColor,

//      firstDayOfWeek: 4,

      height: 420.0,
      selectedDateTime: _currentDate2,
      targetDateTime: _targetDateTime,
      customGridViewPhysics: NeverScrollableScrollPhysics(),
//      markedDateCustomShapeBorder:
//          CircleBorder(side: BorderSide(color: Colors.yellow)),
//      markedDateCustomTextStyle: TextStyle(
//        fontSize: 18,
//        color: Colors.pink,
//      ),
      showHeader: false,
      todayTextStyle: TextStyle(
        color: Colors.black,
      ),
      // markedDateShowIcon: true,
      // markedDateIconMaxShown: 2,
      // markedDateIconBuilder: (event) {
      //   return event.icon;
      // },
      // markedDateMoreShowTotal:
      //     true,
      weekendTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 16,
      ),
      dayPadding: 6.0,
      todayButtonColor: unSelectedButtonColor,
      selectedDayButtonColor: Theme.of(context).primaryColor,
      selectedDayBorderColor: Colors.transparent,
      selectedDayTextStyle: TextStyle(
        color: Colors.white,
      ),
      minSelectedDate: _currentDate.subtract(Duration(days: 1)),
      maxSelectedDate: _currentDate.add(Duration(days: maxSelectedDays)),
//      prevDaysTextStyle: TextStyle(
//        fontSize: 16,
//        color: Colors.orange,
//      ),
      inactiveDaysTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 16,
      ),
      inactiveWeekendTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 16,
      ),
      onCalendarChanged: (DateTime date) {
        this.setState(() {
          _targetDateTime = date;
          _currentMonth = DateFormat.yMMM().format(_targetDateTime);
        });
      },
      onDayLongPressed: (DateTime date) {
        print('long pressed date $date');
      },
    );

    return Container(
      height: 450,
      padding: EdgeInsets.only(bottom: 20, top: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 50.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                _currentDate.add(Duration(days: maxSelectedDays)).month ==
                        _currentDate.month
                    ? SizedBox.shrink()
                    : FlatButton.icon(
                        label: Text(
                          '',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontSize: 15),
                        ),
                        onPressed: () {
                          setState(() {
                            _targetDateTime = DateTime(_targetDateTime.year,
                                _targetDateTime.month - 1);
                            _currentMonth =
                                DateFormat.yMMM().format(_targetDateTime);
                          });
                        },
                        icon: Icon(
                          Icons.arrow_back,
                          color: Colors.black,
                          size: 20,
                        ),
                      ),
                Expanded(
                  flex: 2,
                  child: Text(
                    'Select Date'.tr(),
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 23),
                  ),
                ),
                _currentDate.add(Duration(days: maxSelectedDays)).month ==
                        _currentDate.month
                    ? SizedBox.shrink()
                    : FlatButton.icon(
                        label: Text(''),
                        onPressed: () {
                          setState(() {
                            _targetDateTime = DateTime(_targetDateTime.year,
                                _targetDateTime.month + 1);
                            _currentMonth =
                                DateFormat.yMMM().format(_targetDateTime);
                          });
                        },
                        icon: Icon(
                          Icons.arrow_forward,
                          size: 20,
                          color: Colors.black,
                        ),
                      ),
              ],
            ),
          ),
          Expanded(
            child: ListView(
              children: [
//                  Container(
//                    margin: EdgeInsets.symmetric(horizontal: 16.0),
//                    child: _calendarCarousel,
//                  ),
// This trailing comma makes auto-formatting nicer for build methods.

                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: _calendarCarouselNoHeader,
                ), //
              ],
            ),
          ),
          Text(
            "Grey circles are the available dates".tr(),
            style: TextStyle(
              color: Colors.grey,
              fontSize: 15,
            ),
          ),
        ],
      ),
    );
  }
}
