import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/data/food_data_list.dart';
import 'package:plenty/database/cart.dart';
import 'package:plenty/model/food_category.dart';
import 'package:plenty/provider/app_state.dart';
import 'package:plenty/screens/Fashion/FashionStorePage.dart';
import 'package:plenty/screens/Food/FoodStorePage.dart';
import 'package:plenty/widget/reusable_stack.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

import '../../size.dart';

class FoodCategoryPage extends StatefulWidget {
  final MainCategories model;

  FoodCategoryPage({this.model});

  @override
  _FoodCategoryPageState createState() => _FoodCategoryPageState();
}

class _FoodCategoryPageState extends State<FoodCategoryPage>
    with SingleTickerProviderStateMixin {
  int _plentyIndex = 0;

  PageController _pageController, _pageController2;
  List<Shops> shops = [];

  Animation _animation;
  AnimationController _animationController;
  var cartBox = Hive.box<Cart>('Cart');
  List<Map<String, List<MostPopularItems>>> popularItems = [];

  @override
  void initState() {
    _pageController = PageController(
      viewportFraction: 1.0,
      initialPage: 0,
    );
    _pageController2 = PageController(
      viewportFraction: 0.6,
      initialPage: 0,
    );
    saveData();
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animation = Tween<double>(begin: 0.0, end: MySize().width / 2).animate(
        CurvedAnimation(curve: Curves.easeInOut, parent: _animationController));
    // _animationController.forward();
    _animationController.addListener(() {
      setState(() {});
    });
    shops = [...widget.model.shops];
    super.initState();
  }

  saveData() {
    switch (widget.model.nameEn) {
      case "Fine Dining":
        popularItems = itemFineDining;
        break;
      case "Fashion":
        popularItems = itemFashion;
        break;
      case "Beauty":
        popularItems = itemBeauty;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.transparent,
        title: Text(widget.model.nameEn),
        actions: [
          ValueListenableBuilder(
            valueListenable: Hive.box<Cart>('Cart').listenable(),
            builder: (context, box, widget) {
              return ReusableStack(
                total: box.values.length,
              );
            },
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: PageView.builder(
              controller: _pageController,
              // pageSnapping: false,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) =>
                  _ReusableStack(plentyData: shops[index].style.bgvid),
              itemCount: shops.length,
              // children: <Widget>[
              //
              //   ReusableStack(plentyData: _plentyData, selectedIndex: 1),
              //   ReusableStack(plentyData: _plentyData, selectedIndex: 2),
              // ],
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              decoration: BoxDecoration(
                // color: Colors.white.withOpacity(.4),
                gradient: LinearGradient(
                    colors: [
                      AppColors.plentyblue.withOpacity(1),
                      Colors.white.withOpacity(.6),
                      Colors.white.withOpacity(.7),
                      Colors.white.withOpacity(.8),
                      Colors.white.withOpacity(1)
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.01, 0.2, 0.5, 0.6, 0.7]),
              ),
            ),
          ),
          Positioned(
            left: -90,
            right: 0,
            top: 100,
            bottom: 0,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 120.0),
                    child: TypewriterAnimatedTextKit(
                      onTap: () {
                        print("Tap Event");
                      },
                      text: [
                        'Hello'.tr() +
                            ' ${Provider.of<AppState>(context, listen: false).model.name == "null" ? "" : Provider.of<AppState>(context, listen: false).model.name} ' +
                            'What are you in the mood for'.tr() +
                            "?"
                      ],
                      textStyle: TextStyle(
                        fontFamily: 'Futura',
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                      ),
                      textAlign: TextAlign.start,
                      pause: Duration(milliseconds: 30000),
                    ),
                  ),
                  Container(
                    height: 390,
                    child: PageView.builder(
                      controller: _pageController2,
                      // pageSnapping: false,
                      onPageChanged: (index) {
                        _animationController.reverse(from: MySize().width / 2);
                        setState(() {
                          _plentyIndex = index;
                          _pageController.jumpToPage(_plentyIndex);
                        });
                      },
                      itemBuilder: (BuildContext context, int index) =>
                          _ReusableStack2(
                              img: shops[index].style.poster,
                              index: index,
                              sIndex: _plentyIndex,
                              model: widget.model,
                              shop: shops[index]),
                      itemCount: shops.length,
                      // children: <Widget>[
                      //
                      //   ReusableStack(plentyData: _plentyData, selectedIndex: 1),
                      //   ReusableStack(plentyData: _plentyData, selectedIndex: 2),
                      // ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 90.0),
                    child: Divider(
                      indent: 40,
                      endIndent: 40,
                      thickness: 2,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 120.0),
                    child: TypewriterAnimatedTextKit(
                      onTap: () {
                        print("Tap Event");
                      },
                      text: ['Most Popular Items'.tr()],
                      textStyle: TextStyle(
                          fontFamily: 'Futura',
                          color: Colors.black,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.start,
                      pause: Duration(milliseconds: 30000),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 90.0),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: SafeArea(
                        bottom: true,
                        top: false,
                        left: false,
                        right: false,
                        child: Padding(
                          padding: EdgeInsets.only(left: _animation.value),
                          child: Row(
                            children: popularItems[_plentyIndex]
                                    ["$_plentyIndex"]
                                .map(
                                  (e) => Container(
                                    width: 165,
                                    height: 190,
                                    margin: EdgeInsets.fromLTRB(20, 25, 0, 20),
                                    decoration: BoxDecoration(
                                      color: Colors.black.withOpacity(0.8),
                                      borderRadius: BorderRadius.circular(20),
                                    ),
                                    child: Stack(
                                      fit: StackFit.expand,
                                      alignment: Alignment.topCenter,
                                      overflow: Overflow.visible,
                                      children: [
                                        Positioned(
                                          top: -40,
                                          left: 15,
                                          right: 15,
                                          child: Image.asset(
                                            e.image,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        Positioned(
                                          top: 80,
                                          left: 0,
                                          right: 0,
                                          child: Text(
                                            e.title,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontFamily: 'Futura',
                                                color: Colors.white,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Positioned(
                                          top: 100,
                                          left: 0,
                                          right: 0,
                                          child: Text(
                                            e.desc,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontFamily: 'Futura',
                                              color: Colors.white,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 10,
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 30,
                                          right: 10,
                                          child: Text.rich(
                                            TextSpan(
                                                text: "SAR".tr(),
                                                style: TextStyle(
                                                  fontFamily: 'Futura',
                                                  fontSize: 11,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.white,
                                                ),
                                                children: [
                                                  TextSpan(
                                                    text: " " + e.price,
                                                    style: TextStyle(
                                                        fontFamily: 'Futura',
                                                        fontSize: 20,
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.w700),
                                                  ),
                                                ]),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                        Positioned(
                                          bottom: -22,
                                          right: 10,
                                          left: 10,
                                          child: FlatButton.icon(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            color: Colors.white,
                                            onPressed: () {
                                              if (widget.model.nameEn ==
                                                  "Fine Dining") {
                                                //fine dining

                                              } else if (widget.model.nameEn ==
                                                  "Fashion") {
                                                //fashion

                                              } else if (widget.model.nameEn ==
                                                  "Beauty") {
                                                //beauty

                                              }
                                            },
                                            icon: Image.asset(
                                              'assets/icons/Bag.png',
                                              fit: BoxFit.cover,
                                              width: 30,
                                              color: Colors.black,
                                            ),
                                            label: Text(
                                              "Add to bag".tr(),
                                              style: TextStyle(
                                                fontFamily: 'Futura',
                                                color: Colors.black,
                                                fontWeight: FontWeight.w400,
                                                fontSize: 12,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                                .toList(),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _ReusableStack extends StatefulWidget {
  const _ReusableStack({
    Key key,
    @required String plentyData,
  })  : _plentyData = plentyData,
        super(key: key);

  final String _plentyData;

  @override
  __ReusableStackState createState() => __ReusableStackState();
}

class __ReusableStackState extends State<_ReusableStack> {
  VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    print(widget._plentyData);
    _controller = VideoPlayerController.network(widget._plentyData)
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
          setState(() {});
        });
      });

    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(true);
    // _controller.initialize().then((_) => setState(() {}));
    _controller.play();
    print("play play play");
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _controller.value.initialized
        ? AspectRatio(
            aspectRatio: _controller.value.aspectRatio,
            child: VideoPlayer(_controller),
          )
        : Container();
  }
}

class _ReusableStack2 extends StatelessWidget {
  const _ReusableStack2({
    Key key,
    @required String img,
    @required int index,
    @required int sIndex,
    @required MainCategories model,
    @required Shops shop,
  })  : img = img,
        index = index,
        sIndex = sIndex,
        model = model,
        shop = shop,
        super(key: key);

  final String img;
  final int index;
  final int sIndex;
  final MainCategories model;
  final Shops shop;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        switch (model.nameEn) {
          case "Fine Dining":
            Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.rightToLeft,
                    duration: Duration(milliseconds: 500),
                    child: FoodStorePage(
                      shop: shop,
                    )));
            break;
          case "Fashion":
            Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.rightToLeft,
                    duration: Duration(milliseconds: 500),
                    child: FashionStorePage()));
            break;

          case "Beauty":
            Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.rightToLeft,
                    duration: Duration(milliseconds: 500),
                    child: FoodStorePage(
                      isFromBeauty: true,
                      shop: shop,
                    )));
            break;
        }
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 30.0),
        child: CachedNetworkImage(
          imageUrl: img,
          placeholder: (context, url) => Image.asset(
            'assets/icons/Plenty Logo.png',
            width: 24,
            height: 24,
          ),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),

        // Image.network(
        //   img,
        //   // fit: BoxFit.fill,
        // ),
      ),
    );
  }
}
