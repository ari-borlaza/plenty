import 'package:auto_animated/auto_animated.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:plenty/Network/network_connection.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/config/utils.dart';
import 'package:plenty/data/food_data_list.dart';
import 'package:plenty/database/cart.dart';
import 'package:plenty/model/food_category.dart';
import 'package:plenty/model/shop_products.dart';
import 'package:plenty/screens/Food/FoodItemPage.dart';
import 'package:plenty/widget/reusable_stack.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:easy_localization/easy_localization.dart';

class FoodStorePage extends StatefulWidget {
  final bool isFromBeauty;
  final Shops shop;

  FoodStorePage({this.shop, this.isFromBeauty = false});

  @override
  _FoodStorePageState createState() => _FoodStorePageState();
}

class _FoodStorePageState extends State<FoodStorePage>
    with SingleTickerProviderStateMixin {
  Size get _size => MediaQuery.of(context).size;

  double get _plentyItemWidth => _size.width / 2 + 48;
  ScrollController _plentyScrollController = ScrollController();
  ScrollController _backgroundScrollController = ScrollController();
  final ScrollController _scrollController = ScrollController();
  Map<String, int> quantity = {};
  int totalQuantity = 0;
  var cartBox = Hive.box<Cart>('Cart');
  AnimationController animationController;
  List<Map<String, dynamic>> items = foodStore[0]["item"];
  List<Tab> tabList = List();
  TabController _tabController;
  bool cart = false;
  List<ShopProducts> allData = [];
  List<List<Products>> products = [];
  bool isLoading = true;

  @override
  void initState() {
    // setState(() {});
    fetchCategoriesAndProducts();
    super.initState();
  }

  fetchCategoriesAndProducts() async {
    Response res = await AppApi.instance
        .apiGet("prodcat", queryParameters: {"shop_id": widget.shop.id});
    if (res.statusCode == 200) {
      print(res.data);
      for (Map<String, dynamic> data in res.data)
        allData.add(ShopProducts.fromJson(data));
    }

    for (ShopProducts categories in allData) {
      products.add(categories.products);
      tabList.add(new Tab(
        text: categories.nameEn,
      ));
    }
    _tabController = new TabController(vsync: this, length: tabList.length)
      ..addListener(() {
        if (!_tabController.indexIsChanging) {
          setState(() {
            quantity = {};
          });
        }
      });

    setState(() {
      isLoading = false;
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _plentyScrollController.addListener(() {
      _backgroundScrollController.jumpTo(
          _plentyScrollController.offset * (_size.width / _plentyItemWidth));
    });

    return Scaffold(
      backgroundColor: Colors.white,
      body: isLoading
          ? Center(
              child: SpinKitPulse(
                color: Theme.of(context).primaryColor,
                size: 80.0,
              ),
            )
          : _foodBody(),
    );
  }

  Widget _foodBody() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        appBarr(),
        Expanded(
          child: NestedScrollView(
              controller: _scrollController,
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverToBoxAdapter(
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 230,
                          decoration: BoxDecoration(
                            // color: Colors.yellow,
                            image: DecorationImage(
                              image: NetworkImage(widget.shop.style.bannerimg),
                              fit: BoxFit.cover,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SliverPersistentHeader(
                    pinned: true,
                    floating: true,
                    delegate: ContestTabHeader(
                      foodTab(),
                    ),
                  ),
                ];
              },
              body: Container(
                // height: 600.0,
                child: new TabBarView(
                  controller: _tabController,
                  children: [
                    ...products
                        .map(
                          (e) => itemList(
                            e,
                          ),
                        )
                        .toList(),
                  ],
                ),
              )

              /*  Container(
              child: ListView.builder(
                itemCount: foodList.length,
                padding: const EdgeInsets.only(top: 8),
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  final int count = foodList.length > 10 ? 10 : foodList.length;
                  final Animation<double> animation =
                      Tween<double>(begin: 0.0, end: 1.0).animate(
                          CurvedAnimation(
                              parent: animationController,
                              curve: Interval((1 / count) * index, 1.0,
                                  curve: Curves.fastOutSlowIn)));
                  animationController.forward();
                  return FoodListView(
                    callback: () {},
                    foodData: foodList[index],
                    animation: animation,
                    animationController: animationController,
                  );
                },
              ),
            ), */
              ),
        )

        /*     Padding(
          padding: const EdgeInsets.only(left: 25.0, bottom: 25),
          child: homeWelcome2('Most Popular Items', 15),
        ), */
        // _plentyListView1()

        ,
        Container(
          color: AppColors.white,
          child: Container(
            decoration: BoxDecoration(
                color: AppColors.txtfields,
                borderRadius: BorderRadius.circular(10)),
            margin: const EdgeInsets.fromLTRB(10, 10, 10, 15),
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  children: [
                    ValueListenableBuilder(
                      valueListenable: Hive.box<Cart>('Cart').listenable(),
                      builder: (context, box, widgets) {
                        return ReusableStack(
                          total: cartBox.values.length,
                          textColor: Colors.white,
                          imageColor: Colors.black,
                          backgroundColor: Color(
                            int.parse(widget.shop.style.primary),
                          ),
                        );
                      },
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    itemTxt1(
                        widget.isFromBeauty
                            ? 'View Bookings'.tr()
                            : 'View Bag'.tr(),
                        16,
                        AppColors.greysss,
                        FontWeight.w600),
                  ],
                ),
                SizedBox(
                  width: 10,
                ),
                Row(
                  children: [
                    itemTxt1(
                        'Total'.tr(), 15, AppColors.black, FontWeight.bold),
                    SizedBox(
                      width: 10,
                    ),
                    itemTxt1('SAR'.tr() + '${calculateTotal()}', 15,
                        AppColors.black, FontWeight.bold),
                  ],
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget appBarr() {
    return Container(
      height: kToolbarHeight + 50,
      color: Color(int.parse(widget.shop.style.primary)),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 40, 10, 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Language.arLocal
                  ? Icon(
                      CupertinoIcons.chevron_right,
                      color: Colors.white,
                    )
                  : Icon(
                      CupertinoIcons.chevron_left,
                      color: Colors.white,
                    ),
            ),
            Expanded(
              child: Container(
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 80, vertical: 5),
                child: Image.network(
                  widget.shop.style.header,
                  color: Colors.white,
                ),
              ),
            ),
            ValueListenableBuilder(
              valueListenable: Hive.box<Cart>('Cart').listenable(),
              builder: (context, box, widget) {
                return ReusableStack(
                  total: box.values.length,
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget foodTab() {
    return Container(
      decoration: new BoxDecoration(
        color: Color(int.parse(widget.shop.style.primary)),
      ),
      child: new TabBar(
        labelStyle: TextStyle(
          fontFamily: 'Futura',
          fontSize: 12,
        ),
        labelColor: AppColors.white,
        unselectedLabelColor: AppColors.white,
        isScrollable: true,
        controller: _tabController,
        indicatorColor: Color(int.parse(widget.shop.style.secondary)),
        indicatorSize: TabBarIndicatorSize.tab,
        indicatorWeight: 5.0,
        indicatorPadding: EdgeInsets.symmetric(horizontal: 6),
        tabs: tabList,
      ),
    );
  }

  itemList(List<Products> products) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: NetworkImage(widget.shop.style.bg), fit: BoxFit.cover),
      ),
      child: products.isEmpty
          ? Center(
              child: Text("No Product Available"),
            )
          : LiveList(
              showItemInterval: Duration(milliseconds: 150),
              showItemDuration: Duration(milliseconds: 350),
              // padding: EdgeInsets.all(16),
              reAnimateOnVisibility: true,
              scrollDirection: Axis.vertical,
              itemCount: products.length,
              padding: EdgeInsets.zero,
              itemBuilder: animationItemBuilder(
                (index) => Column(
                  children: [
                    itemTile(products[index]),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Divider(
                        color: Color(int.parse(widget.shop.style.primary)),
                        thickness: 0.7,
                      ),
                    ),
                  ],
                ),
                padding: EdgeInsets.symmetric(vertical: 8),
              ),
            ),
    );
  }

  Widget itemTile(Products product) {
    return InkWell(
      onTap: () {
        if (!widget.isFromBeauty ||
            product.sizes != null ||
            product.sizes.isNotEmpty)
          Navigator.push(
            context,
            PageTransition(
              type: PageTransitionType.rightToLeft,
              duration: Duration(milliseconds: 500),
              child: FoodItemPage(shop: widget.shop, product: product),
            ),
          );
      },
      child: Container(
        height: 150,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (quantity.containsKey("${product.id}")) ...{
              Container(
                width: 8,
                height: 150,
                decoration: BoxDecoration(
                    color: Color(int.parse(widget.shop.style.primary)),
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(20),
                        topRight: Radius.circular(20))),
              ),
              SizedBox(
                width: 10,
              ),
              Align(
                alignment: Alignment.center,
                child: Text.rich(
                  TextSpan(
                      text: "X ",
                      style: TextStyle(
                        fontFamily: 'Futura',
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                        color: Color(int.parse(widget.shop.style.primary)),
                      ),
                      children: [
                        TextSpan(
                          text: "${quantity["${product.id}"]}",
                          style: TextStyle(
                              fontFamily: 'Futura',
                              fontSize: 20,
                              color:
                                  Color(int.parse(widget.shop.style.primary)),
                              fontWeight: FontWeight.w700),
                        ),
                      ]),
                  textAlign: TextAlign.left,
                ),
              ),
            },
            SizedBox(
              width: 10,
            ),
            Container(
              width: 90,
              height: 90,
              margin: EdgeInsets.only(right: 15),
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                    image: AssetImage(
                      'assets/image/food.jpeg', //NO Image Here
                    ),
                    fit: BoxFit
                        .cover, /*
                    colorFilter: ColorFilter.mode(
                        Colors.white.withOpacity(0.9), BlendMode.lighten), */
                  )),
            ),
            Expanded(
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    product.nameEn,
                    style: TextStyle(
                        fontFamily: 'Futura',
                        fontSize: 14,
                        color: Color(int.parse(widget.shop.style.primary)),
                        fontWeight: FontWeight.w700),
                  ),
                  SizedBox(height: 5),
                  Container(
                    height: 50,
                    child: Text(
                      product.descEn,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontFamily: 'Futura',
                        wordSpacing: 5,
                        fontSize: 12,
                        color: Color(int.parse(widget.shop.style.primary)),
                        fontWeight: FontWeight.w400,
                        height: 2,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text.rich(
                    TextSpan(
                      text: "SAR".tr(),
                      style: TextStyle(
                        fontSize: 12,
                        fontFamily: 'Futura',
                        color: Color(int.parse(widget.shop.style.primary)),
                        //Colors.green.shade600,
                        fontWeight: FontWeight.w400,
                      ),
                      children: [
                        TextSpan(
                          text: product.price,
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Futura',
                              color:
                                  Color(int.parse(widget.shop.style.primary)),
                              fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: plentyFlatBtn2(
                        widget.isFromBeauty
                            ? 'Book Now'
                            : (product.sizes.isEmpty || product.sizes == null)
                                ? 'Add to basket'
                                : "Select Sizes",
                        () async {
                          // if (widget.isFromBeauty) {
                          //   //TODO: FOR BEAUTY DO SOMETHING HERE
                          //   if (cartBox.values.isEmpty ||
                          //       cartBox.values.first.category == "beauty") {
                          //     showDialog(
                          //       context: context,
                          //       builder: (context) => Dialog(
                          //         insetPadding: EdgeInsets.symmetric(
                          //             vertical: 24, horizontal: 20),
                          //         shape: RoundedRectangleBorder(
                          //             borderRadius: BorderRadius.circular(20)),
                          //         child: ShowDateDialog(),
                          //       ),
                          //     );
                          //
                          //     if (cartBox.values.length > 0) {
                          //       for (int i = 0;
                          //           i < cartBox.values.length;
                          //           i++) {
                          //         if (cartBox.getAt(i).name == product.nameEn) {
                          //           await cartBox.put(
                          //             product.id,
                          //             Cart(
                          //                 pid: product.id,
                          //                 image: (product.images == null ||
                          //                         product.images.isEmpty)
                          //                     ? ""
                          //                     : product.images[0]
                          //                         .imgurl, //TODO: CHANGE SOMETHING HERE
                          //                 name: product.nameEn,
                          //                 size:
                          //                     "S", //TODO: CHANGE SOMETHING HERE
                          //                 color: Colors.red
                          //                     .value, //TODO: CHANGE SOMETHING HERE
                          //                 price: product.price,
                          //                 quantity:
                          //                     cartBox.getAt(i).quantity + 1,
                          //                 category: "beauty"),
                          //           );
                          //           cart = true;
                          //         }
                          //       }
                          //     }
                          //     if (!cart) {
                          //       await cartBox.put(
                          //         product.id,
                          //         Cart(
                          //             id: product.id,
                          //             image: (product.images == null ||
                          //                     product.images.isEmpty)
                          //                 ? ""
                          //                 : product.images[0]
                          //                     .imgurl, //TODO: CHANGE SOMETHING HERE
                          //             name: product.nameEn,
                          //             size: "S", //TODO: CHANGE SOMETHING HERE
                          //             color: Colors.red
                          //                 .value, //TODO: CHANGE SOMETHING HERE
                          //             price: product.price,
                          //             quantity: 1,
                          //             category: "beauty"),
                          //       );
                          //     } else {
                          //       cart = false;
                          //     }
                          //     setState(() {
                          //       if (quantity.containsKey("${product.id}")) {
                          //         quantity.update(
                          //             "${product.id}", (value) => value + 1);
                          //         totalQuantity++;
                          //         //TODO: USE HIVE HERE
                          //       } else {
                          //         quantity.putIfAbsent(
                          //             "${product.id}", () => 1);
                          //         totalQuantity++;
                          //       }
                          //     });
                          //   } else {
                          //     Alert(
                          //       context: context,
                          //       type: AlertType.warning,
                          //       title: "Cart",
                          //       desc:
                          //           "your cart contains different category products. do you want to  clear your cart before proceed?",
                          //       style: AlertStyle(
                          //         titleStyle: TextStyle(fontSize: 12),
                          //         descStyle: TextStyle(fontSize: 11),
                          //       ),
                          //       buttons: [
                          //         DialogButton(
                          //           child: Text(
                          //             "CANCEL",
                          //             style: TextStyle(
                          //                 color: Colors.white, fontSize: 14),
                          //           ),
                          //           onPressed: () => Navigator.pop(context),
                          //           width: 80,
                          //         ),
                          //         DialogButton(
                          //           child: Text(
                          //             "OK",
                          //             style: TextStyle(
                          //                 color: Colors.white, fontSize: 14),
                          //           ),
                          //           onPressed: () async {
                          //             Navigator.pop(context);
                          //             await cartBox.clear();
                          //             await cartBox.put(
                          //               product.id,
                          //               Cart(
                          //                   id: product.id,
                          //                   image: (product.images == null ||
                          //                           product.images.isEmpty)
                          //                       ? ""
                          //                       : product.images[0]
                          //                           .imgurl, //TODO: CHANGE SOMETHING HERE
                          //                   name: product.nameEn,
                          //                   size:
                          //                       "S", ////TODO: CHANGE SOMETHING HERE
                          //                   color: Colors.red
                          //                       .value, ////TODO: CHANGE SOMETHING HERE
                          //                   price: product.price,
                          //                   quantity: 1,
                          //                   category: "beauty"),
                          //             );
                          //             setState(() {
                          //               if (quantity
                          //                   .containsKey("${product.id}")) {
                          //                 quantity.update("${product.id}",
                          //                     (value) => value + 1);
                          //                 totalQuantity++;
                          //                 //TODO: USE HIVE HERE
                          //               } else {
                          //                 quantity.putIfAbsent(
                          //                     "${product.id}", () => 1);
                          //                 totalQuantity++;
                          //               }
                          //             });
                          //           },
                          //           width: 80,
                          //         )
                          //       ],
                          //     ).show();
                          //   }
                          // } else {
                          //   if (product.sizes.isEmpty ||
                          //       product.sizes == null) {
                          //     //TODO: FOR FINE DINING
                          //     if (cartBox.values.isEmpty ||
                          //         cartBox.values.first.category ==
                          //             "finedining") {
                          //       if (cartBox.values.length > 0) {
                          //         for (int i = 0;
                          //             i < cartBox.values.length;
                          //             i++) {
                          //           if (cartBox.getAt(i).pid == product.id) {
                          //             await cartBox.put(
                          //               product.id,
                          //               Cart(
                          //                   pid: product.id,
                          //                   image: (product.images == null ||
                          //                           product.images.isEmpty)
                          //                       ? ""
                          //                       : product.images[0].imgurl,
                          //                   name: product.nameEn,
                          //                   sizeID: -1,
                          //                   colorID: -1,
                          //                   price: product.price,
                          //                   addsOn: [],
                          //                   quantity:
                          //                       cartBox.getAt(i).quantity + 1,
                          //                   category: "finedining"),
                          //             );
                          //             cart = true;
                          //           }
                          //         }
                          //       }
                          //       if (!cart) {
                          //         await cartBox.put(
                          //           product.id,
                          //           Cart(
                          //               pid: product.id,
                          //               image: (product.images == null ||
                          //                       product.images.isEmpty)
                          //                   ? ""
                          //                   : product.images[0].imgurl,
                          //               name: product.nameEn,
                          //               sizeID: -1,
                          //               colorID: -1,
                          //               addsOn: [],
                          //               price: product.price,
                          //               quantity: 1,
                          //               category: "finedining"),
                          //         );
                          //       } else {
                          //         cart = false;
                          //       }
                          //       setState(() {
                          //         if (quantity.containsKey("${product.id}")) {
                          //           quantity.update(
                          //               "${product.id}", (value) => value + 1);
                          //           totalQuantity++;
                          //           //TODO: USE HIVE HERE
                          //         } else {
                          //           quantity.putIfAbsent(
                          //               "${product.id}", () => 1);
                          //           totalQuantity++;
                          //         }
                          //       });
                          //     } else {
                          //       Alert(
                          //         context: context,
                          //         type: AlertType.warning,
                          //         title: "Cart",
                          //         desc:
                          //             "your cart contains different category products. do you want to  clear your cart before proceed?",
                          //         style: AlertStyle(
                          //           titleStyle: TextStyle(fontSize: 12),
                          //           descStyle: TextStyle(fontSize: 11),
                          //         ),
                          //         buttons: [
                          //           DialogButton(
                          //             child: Text(
                          //               "CANCEL",
                          //               style: TextStyle(
                          //                   color: Colors.white, fontSize: 14),
                          //             ),
                          //             onPressed: () => Navigator.pop(context),
                          //             width: 80,
                          //           ),
                          //           DialogButton(
                          //             child: Text(
                          //               "OK",
                          //               style: TextStyle(
                          //                   color: Colors.white, fontSize: 14),
                          //             ),
                          //             onPressed: () async {
                          //               Navigator.pop(context);
                          //               await cartBox.clear();
                          //               await cartBox.put(
                          //                 product.id,
                          //                 Cart(
                          //                     pid: product.id,
                          //                     image: (product.images == null ||
                          //                             product.images.isEmpty)
                          //                         ? ""
                          //                         : product.images[0]
                          //                             .imgurl, //TODO: CHANGE SOMETHING HERE
                          //                     name: product.nameEn,
                          //                     addsOn: [],
                          //                     sizeID: -1,
                          //                     colorID: -1,
                          //                     price: product.price,
                          //                     quantity: 1,
                          //                     category: "finedining"),
                          //               );
                          //               setState(() {
                          //                 if (quantity
                          //                     .containsKey("${product.id}")) {
                          //                   quantity.update("${product.id}",
                          //                       (value) => value + 1);
                          //                   totalQuantity++;
                          //                   //TODO: USE HIVE HERE
                          //                 } else {
                          //                   quantity.putIfAbsent(
                          //                       "${product.id}", () => 1);
                          //                   totalQuantity++;
                          //                 }
                          //               });
                          //             },
                          //             width: 80,
                          //           )
                          //         ],
                          //       ).show();
                          //     }
                          //   } else {
                          //     //TODO: MOVE TO DETAILS PAGE AND CHANGE BUTTON TEXT
                          //     Navigator.push(
                          //       context,
                          //       PageTransition(
                          //         type: PageTransitionType.rightToLeft,
                          //         duration: Duration(milliseconds: 500),
                          //         child: FoodItemPage(
                          //             shop: widget.shop, product: product),
                          //       ),
                          //     );
                          //   }
                          // }
                          // ? 'Book Now'.tr()
                          // : 'Add to bag'.tr(),
                          //   () async {
                          //     if (widget.isFromBeauty) {
                          //       if (cartBox.values.isEmpty ||
                          //           cartBox.values.first.category == "beauty") {
                          //         showDialog(
                          //           context: context,
                          //           builder: (context) => Dialog(
                          //             insetPadding: EdgeInsets.symmetric(
                          //                 vertical: 24, horizontal: 20),
                          //             shape: RoundedRectangleBorder(
                          //                 borderRadius: BorderRadius.circular(20)),
                          //             child: ShowDateDialog(),
                          //           ),
                          //         );

                          //         if (cartBox.values.length > 0) {
                          //           for (int i = 0;
                          //               i < cartBox.values.length;
                          //               i++) {
                          //             if (cartBox.getAt(i).name ==
                          //                 items[index]["item_name"]) {
                          //               await cartBox.put(
                          //                 items[index]["id"],
                          //                 Cart(
                          //                     id: items[index]["id"],
                          //                     image: items[index]["image"],
                          //                     name: items[index]["item_name"],
                          //                     size: "S",
                          //                     color: Colors.red.value,
                          //                     price: items[index]["item_price"],
                          //                     quantity:
                          //                         cartBox.getAt(i).quantity + 1,
                          //                     category: "beauty"),
                          //               );
                          //               cart = true;
                          //             }
                          //           }
                          //         }
                          //         if (!cart) {
                          //           await cartBox.put(
                          //             items[index]["id"],
                          //             Cart(
                          //                 id: items[index]["id"],
                          //                 image: items[index]["image"],
                          //                 name: items[index]["item_name"],
                          //                 size: "S",
                          //                 color: Colors.red.value,
                          //                 price: items[index]["item_price"],
                          //                 quantity: 1,
                          //                 category: "beauty"),
                          //           );
                          //         } else {
                          //           cart = false;
                          //         }
                          //         setState(() {
                          //           if (quantity.containsKey("$index")) {
                          //             quantity.update(
                          //                 "$index", (value) => value + 1);
                          //             totalQuantity++;
                          //             //TODO: USE HIVE HERE
                          //           } else {
                          //             quantity.putIfAbsent("$index", () => 1);
                          //             totalQuantity++;
                          //           }
                          //         });
                          //       } else {
                          //         Alert(
                          //           context: context,
                          //           type: AlertType.warning,
                          //           title: "Cart".tr(),
                          //           desc:
                          //               "Your cart contains different category products. Do you want to clear your cart to proceed?"
                          //                   .tr(),
                          //           style: AlertStyle(
                          //             titleStyle: TextStyle(fontSize: 12),
                          //             descStyle: TextStyle(fontSize: 11),
                          //           ),
                          //           buttons: [
                          //             DialogButton(
                          //               child: Text(
                          //                 "Cancel".tr().toUpperCase(),
                          //                 style: TextStyle(
                          //                     color: Colors.white, fontSize: 14),
                          //               ),
                          //               onPressed: () => Navigator.pop(context),
                          //               width: 80,
                          //             ),
                          //             DialogButton(
                          //               child: Text(
                          //                 "OK".tr(),
                          //                 style: TextStyle(
                          //                     color: Colors.white, fontSize: 14),
                          //               ),
                          //               onPressed: () async {
                          //                 Navigator.pop(context);
                          //                 await cartBox.clear();
                          //                 await cartBox.put(
                          //                   items[index]["id"],
                          //                   Cart(
                          //                       id: items[index]["id"],
                          //                       image: items[index]["image"],
                          //                       name: items[index]["item_name"],
                          //                       size: "S",
                          //                       color: Colors.red.value,
                          //                       price: items[index]["item_price"],
                          //                       quantity: 1,
                          //                       category: "beauty"),
                          //                 );
                          //                 setState(() {
                          //                   if (quantity.containsKey("$index")) {
                          //                     quantity.update(
                          //                         "$index", (value) => value + 1);
                          //                     totalQuantity++;
                          //                     //TODO: USE HIVE HERE
                          //                   } else {
                          //                     quantity.putIfAbsent(
                          //                         "$index", () => 1);
                          //                     totalQuantity++;
                          //                   }
                          //                 });
                          //               },
                          //               width: 80,
                          //             )
                          //           ],
                          //         ).show();
                          //       }
                          //     } else {
                          //       if (cartBox.values.isEmpty ||
                          //           cartBox.values.first.category == "finedining") {
                          //         if (cartBox.values.length > 0) {
                          //           for (int i = 0;
                          //               i < cartBox.values.length;
                          //               i++) {
                          //             if (cartBox.getAt(i).name ==
                          //                 items[index]["item_name"]) {
                          //               await cartBox.put(
                          //                 items[index]["id"],
                          //                 Cart(
                          //                     id: items[index]["id"],
                          //                     image: items[index]["image"],
                          //                     name: items[index]["item_name"],
                          //                     size: "S",
                          //                     color: Colors.red.value,
                          //                     price: items[index]["item_price"],
                          //                     quantity:
                          //                         cartBox.getAt(i).quantity + 1,
                          //                     category: "finedining"),
                          //               );
                          //               cart = true;
                          //             }
                          //           }
                          //         }
                          //         if (!cart) {
                          //           await cartBox.put(
                          //             items[index]["id"],
                          //             Cart(
                          //                 id: items[index]["id"],
                          //                 image: items[index]["image"],
                          //                 name: items[index]["item_name"],
                          //                 size: "S",
                          //                 color: Colors.red.value,
                          //                 price: items[index]["item_price"],
                          //                 quantity: 1,
                          //                 category: "finedining"),
                          //           );
                          //         } else {
                          //           cart = false;
                          //         }
                          //         setState(() {
                          //           if (quantity.containsKey("$index")) {
                          //             quantity.update(
                          //                 "$index", (value) => value + 1);
                          //             totalQuantity++;
                          //             //TODO: USE HIVE HERE
                          //           } else {
                          //             quantity.putIfAbsent("$index", () => 1);
                          //             totalQuantity++;
                          //           }
                          //         });
                          //       } else {
                          //         Alert(
                          //           context: context,
                          //           type: AlertType.warning,
                          //           title: "Cart".tr(),
                          //           desc:
                          //               "Your cart contains different category products. Do you want to clear your cart to proceed?"
                          //                   .tr(),
                          //           style: AlertStyle(
                          //             titleStyle: TextStyle(fontSize: 12),
                          //             descStyle: TextStyle(fontSize: 11),
                          //           ),
                          //           buttons: [
                          //             DialogButton(
                          //               child: Text(
                          //                 "Cancel".tr().toUpperCase(),
                          //                 style: TextStyle(
                          //                     color: Colors.white, fontSize: 14),
                          //               ),
                          //               onPressed: () => Navigator.pop(context),
                          //               width: 80,
                          //             ),
                          //             DialogButton(
                          //               child: Text(
                          //                 "OK".tr(),
                          //                 style: TextStyle(
                          //                     color: Colors.white, fontSize: 14),
                          //               ),
                          //               onPressed: () async {
                          //                 Navigator.pop(context);
                          //                 await cartBox.clear();
                          //                 await cartBox.put(
                          //                   items[index]["id"],
                          //                   Cart(
                          //                       id: items[index]["id"],
                          //                       image: items[index]["image"],
                          //                       name: items[index]["item_name"],
                          //                       size: "S",
                          //                       color: Colors.red.value,
                          //                       price: items[index]["item_price"],
                          //                       quantity: 1,
                          //                       category: "finedining"),
                          //                 );
                          //                 setState(() {
                          //                   if (quantity.containsKey("$index")) {
                          //                     quantity.update(
                          //                         "$index", (value) => value + 1);
                          //                     totalQuantity++;
                          //                     //TODO: USE HIVE HERE
                          //                   } else {
                          //                     quantity.putIfAbsent(
                          //                         "$index", () => 1);
                          //                     totalQuantity++;
                          //                   }
                          //                 });
                          //               },
                          //               width: 80,
                          //             )
                          //           ],
                          //         ).show();
                          //       }
                          //     }
                        },
                        color: Color(int.parse(widget.shop.style.primary)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  int calculateTotal() {
    return cartBox.values
        .toList()
        .map((e) => (int.parse(e.price) * e.quantity))
        .fold(0, (previousValue, element) => previousValue + element);
  }
}

class ContestTabHeader extends SliverPersistentHeaderDelegate {
  ContestTabHeader(
    this.foodTab,
  );
  final Widget foodTab;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return foodTab;
  }

  @override
  double get maxExtent => 52.0;

  @override
  double get minExtent => 52.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
