import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:plenty/config/btn_txt.dart';
import 'package:plenty/config/colors.dart';
import 'package:plenty/config/locale.dart';
import 'package:plenty/config/textstyles.dart';
import 'package:plenty/data/food1_list_data.dart';
import 'package:plenty/data/food_data_list.dart';
import 'package:plenty/database/cart.dart';
import 'package:plenty/model/food_category.dart';
import 'package:plenty/model/shop_products.dart';
import 'package:plenty/widget/reusable_stack.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class FoodItemPage extends StatefulWidget {
  final Shops shop;
  final Products product;

  FoodItemPage({this.shop, this.product});

  @override
  _FoodItemPageState createState() => _FoodItemPageState();
}

class _FoodItemPageState extends State<FoodItemPage>
    with SingleTickerProviderStateMixin {
  List<FoodListData> foodList = FoodListData.foodList;
  List<Map<String, dynamic>> items = foodStore[0]["item"];
  List<Map<String, dynamic>> itemss = foodStore[0]["item_price"];
  List<Sizes> sizes = [];
  List<Addons> addsOn = [];

  var cartBox = Hive.box<Cart>('Cart');
  List<int> selectedQuantity = [];
  List<int> selectedAddsOn = [];

  void minus(int index) {
    int indexes = selectedQuantity[index];

    if (indexes != 0) {
      selectedQuantity[index] = indexes - 1;
    }

    setState(() {});
    print(selectedQuantity);
  }

  void add(int index) {
    int indexes = selectedQuantity[index];

    selectedQuantity[index] = indexes + 1;
    setState(() {});
    print(selectedQuantity);
  }

  @override
  void initState() {
    sizes = [...widget.product.sizes];
    addsOn = [...widget.product.addons];
    for (int i = 0; i < sizes.length; i++) {
      selectedQuantity.add(0);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: Colors.white, body: _foodBody()

        //_foodBody(),
        );
  }

  Widget _foodBody() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          appBarr(),
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/icons/Cafe BG.jpg'),
                  fit: BoxFit.cover),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(30, 10, 0, 2),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.product.nameEn,
                        style: TextStyle(
                            fontFamily: 'Futura',
                            fontSize: 20,
                            color: Color(int.parse(widget.shop.style.primary)),
                            fontWeight: FontWeight.w600),
                      ),
                      Text(
                        widget.product.descEn,
                        style: TextStyle(
                            fontFamily: 'Futura',
                            fontSize: 14,
                            color: Color(int.parse(widget.shop.style.primary)),
                            height: 2,
                            fontWeight: FontWeight.w500),
                      ),
                      // itemTxt3("Chocolate Coffee", 20),
                      // itemTxt3("Description,Description,Description", 15),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Select Size'.tr(),
                        style: TextStyle(
                            fontFamily: 'Futura',
                            fontSize: 14,
                            color: Color(int.parse(widget.shop.style.primary)),
                            fontWeight: FontWeight.w500),
                      ),
                      // itemTxt3("Select Size", 14),
                    ],
                  ),
                ),
                swipey(),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: itemTxt3(
                    "Add Ons".tr(),
                    18,
                    color: Color(int.parse(widget.shop.style.primary)),
                  ),
                ),
                addOnsList()
                //addOnsList()
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container addOnsList() {
    return Container(
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: AppColors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey[200],
            blurRadius: 2,
            spreadRadius: 4,
          ),
        ],
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List.generate(
          addsOn.length,
          (index) =>
              // itemTxt3("Select Add Ons", 15),
              // CheckboxGroup(
              //     activeColor: AppColors.sadagreen,
              //     labelStyle:
              //         TextStyle(fontFamily: 'Futura',fontSize: 12, color: AppColors.black),
              //     labels: <String>[
              //       "Add On 1 (+SAR 1)",
              //       "Add On 2 (+SAR 1)",
              //       "Add On 3 (+SAR 1)",
              //       "Add On 4 (+SAR 1)",
              //       "Add On 5 (+SAR 1)",
              //       "Add On 6 (+SAR 1)",
              //       "Add On 7 (+SAR 1)",
              //     ],
              //     onSelected: (List<String> checked) => print(checked.toString())),
              InkResponse(
            onTap: () {
              setState(() {
                if (selectedAddsOn.contains(index))
                  selectedAddsOn.remove(index);
                else
                  selectedAddsOn.add(index);
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(bottom: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${addsOn[index].nameEn} (${addsOn[index].descEn})",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text("( + SAR ${addsOn[index].price} )"),
                      ],
                    ),
                  ),
                  Container(
                    width: 20,
                    height: 20,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: selectedAddsOn.contains(index)
                            ? Color(int.parse(widget.shop.style.primary))
                            : Colors.grey),
                    child: selectedAddsOn.contains(index)
                        ? Icon(
                            Icons.check,
                            color: Colors.white,
                            size: 18,
                          )
                        : SizedBox.shrink(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget swipey() {
    return Stack(
      overflow: Overflow.visible,
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(30, 10, 30, 30),
          height: 375,
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  blurRadius: 2,
                  spreadRadius: 4,
                  color: Colors.grey[300],
                ),
              ],
              borderRadius: BorderRadius.circular(18)),
          child: Swiper(
            itemBuilder: (BuildContext context, int index) {
              return Container(
                margin: EdgeInsets.fromLTRB(
                  0,
                  10,
                  0,
                  30,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text.rich(
                          TextSpan(
                              text: "SAR".tr(),
                              style: TextStyle(
                                fontFamily: 'Futura',
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                color: Colors.black,
                              ),
                              children: [
                                TextSpan(
                                  text: "${sizes[index].price}",
                                  style: TextStyle(
                                      fontFamily: 'Futura',
                                      fontSize: 20,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700),
                                ),
                              ]),
                          textAlign: TextAlign.left,
                        ),
                        // itemTxt2('SAR ', 10),
                        // itemTxt2(items[index]["item_price"], 20),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Expanded(
                      child: Container(
                        child: Image.network(
                          'https://images.unsplash.com/photo-1512568400610-62da28bc8a13?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M3x8Y29mZmVlfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60', //TODO: ADD IMAGE HERE
                          /*
                                    items[index]["image"], */
                          fit: BoxFit.cover,
                          // height: 180,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(sizes[index].value,
                        style: TextStyle(
                            fontFamily: 'Futura',
                            fontSize: 16,
                            color: Colors.black,
                            fontWeight: FontWeight.bold)),
                    // itemTxt2(items[index]["item_size"], 20),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: 30,
                          height: 30,
                          child: FloatingActionButton(
                            heroTag: null,
                            mini: true,
                            onPressed: () {
                              minus(index);
                            },
                            child: Icon(
                              Icons.remove,
                              color: Colors.white,
                            ),
                            backgroundColor:
                                Color(int.parse(widget.shop.style.primary)),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        itemTxt1('${selectedQuantity[index]}', 22,
                            AppColors.black, FontWeight.bold),
                        SizedBox(
                          width: 20,
                        ),
                        SizedBox(
                          width: 30,
                          height: 30,
                          child: FloatingActionButton(
                            heroTag: null,
                            mini: true,
                            onPressed: () {
                              add(index);
                            },
                            child: new Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                            backgroundColor:
                                Color(int.parse(widget.shop.style.primary)),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            },
            itemCount: sizes.length,
            viewportFraction: 0.8,
            scale: 0.8,
            // pagination: SwiperPagination(),
            control: SwiperControl(
              color: Colors.black,
            ),
            layout: SwiperLayout.DEFAULT,
            containerHeight: 500,
            itemHeight: 300,
            itemWidth: 300.0,
          ),
        ),
        Positioned(
            bottom: 10,
            right: 30,
            child: plentyFlatBtn2(
              'Add to basket'.tr(),
              () async {
                //TODO: ADD TO BASKET HERE FINE-DINING

                //selectedAddsOn
                List<int> addsOns = [];
                for (int add in selectedAddsOn) {
                  addsOns.add(addsOn[add].id);
                }

                if (cartBox.values.isEmpty ||
                    cartBox.values.first.category == "finedining") {
                  for (int i = 0; i < selectedQuantity.length; i++) {
                    if (selectedQuantity[i] != 0) {
                      Cart cartprint = Cart(
                          pid: widget.product.id,
                          image: (widget.product.images == null ||
                                  widget.product.images.isEmpty)
                              ? ""
                              : widget.product.images[0].imgurl,
                          name: widget.product.nameEn,
                          sizeID: sizes[i].id,
                          addsOn: addsOns,
                          price: sizes[i].price,
                          quantity: selectedQuantity[i],
                          category: "finedining",
                          key: "${widget.product.id}${sizes[i].id}",
                          size: sizes[i].value,
                          shopID: widget.product.shopId);
                      print(cartprint);
                      await cartBox.put(
                        "${widget.product.id}${sizes[i].id}",
                        Cart(
                            pid: widget.product.id,
                            image: (widget.product.images == null ||
                                    widget.product.images.isEmpty)
                                ? ""
                                : widget.product.images[0].imgurl,
                            name: widget.product.nameEn,
                            sizeID: sizes[i].id,
                            addsOn: addsOns,
                            price: sizes[i].price,
                            quantity: selectedQuantity[i],
                            category: "finedining",
                            key: "${widget.product.id}${sizes[i].id}",
                            size: sizes[i].value,
                            shopID: widget.product.shopId),
                      );
                    }
                  }

                  for (int i = 0; i < cartBox.values.length; i++) {
                    print(cartBox.getAt(i).pid);
                    print(cartBox.getAt(i).image);
                    print(cartBox.getAt(i).name);
                    print(cartBox.getAt(i).sizeID);
                    print(cartBox.getAt(i).addsOn);
                    print(cartBox.getAt(i).price);
                    print(cartBox.getAt(i).quantity);
                    print(cartBox.getAt(i).category);
                    print(cartBox.getAt(i).shopID);
                  }
                } else {
                  Alert(
                    context: context,
                    type: AlertType.warning,
                    title: "Cart",
                    desc:
                        "your cart contains different category products. do you want to  clear your cart before proceed?",
                    style: AlertStyle(
                      titleStyle: TextStyle(fontSize: 12),
                      descStyle: TextStyle(fontSize: 11),
                    ),
                    buttons: [
                      DialogButton(
                        child: Text(
                          "CANCEL",
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                        onPressed: () => Navigator.pop(context),
                        width: 80,
                      ),
                      DialogButton(
                        child: Text(
                          "OK",
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                        onPressed: () async {
                          Navigator.pop(context);
                          await cartBox.clear();
                          for (int i = 0; i < selectedQuantity.length; i++) {
                            if (selectedQuantity[i] != 0) {
                              await cartBox.put(
                                "${widget.product.id}${sizes[i].id}",
                                Cart(
                                  pid: widget.product.id,
                                  image: (widget.product.images == null ||
                                          widget.product.images.isEmpty)
                                      ? ""
                                      : widget.product.images[0].imgurl,
                                  //TODO: CHANGE SOMETHING HERE
                                  name: widget.product.nameEn,
                                  addsOn: addsOns,
                                  sizeID: sizes[i].id,
                                  price: sizes[i].price,
                                  quantity: selectedQuantity[i],
                                  category: "finedining",
                                  key: "${widget.product.id}${sizes[i].id}",
                                  size: sizes[i].value,
                                  shopID: widget.product.shopId,
                                ),
                              );
                            }
                          }
                        },
                        width: 80,
                      )
                    ],
                  ).show();
                }
              },
              color: Color(int.parse(widget.shop.style.primary)),
            )),
      ],
    );
  }

  Widget appBarr() {
    return Container(
      height: 300,
      decoration: BoxDecoration(
        color: Color(int.parse(widget.shop.style.primary)),
        image: DecorationImage(
          image: NetworkImage(
              'https://images.unsplash.com/photo-1512151004335-d5b6c2ff7e12?ixid=MXwxMjA3fDB8MHxzZWFyY2h8OHx8Y29mZmVlfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'),
          fit: BoxFit.fill,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 50, 10, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Language.arLocal
                  ? Icon(
                      CupertinoIcons.chevron_right,
                      color: Colors.white,
                    )
                  : Icon(
                      CupertinoIcons.chevron_left,
                      color: Colors.white,
                    ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                widget.product.nameEn,
                style: TextStyle(
                    fontFamily: 'Futura',
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 14),
              ),
            ),
            ValueListenableBuilder(
              valueListenable: Hive.box<Cart>('Cart').listenable(),
              builder: (context, box, widgets) {
                return ReusableStack(
                  textColor: Color(int.parse(widget.shop.style.primary)),
                  total: cartBox.values.length,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class AddOns extends StatelessWidget {
  const AddOns({
    @required this.title,
    Key key,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) => Container(
        height: 50,
        child: Card(
          color: AppColors.yellow,
          child: Text(
            '$title a long title',
            style: TextStyle(
              fontFamily: 'Futura',
              color: Colors.red,
            ),
          ),
        ),
      );
}
