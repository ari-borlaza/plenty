import 'package:flutter/material.dart';

class MySize {
  static double _width;
  static double _height;
  static double _shortestSide;
  void init(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    _height = MediaQuery.of(context).size.height;
    _shortestSide = MediaQuery.of(context).size.shortestSide;
  }

  double get height => _height;

  double get width => _width;

  double get shortestSide => _shortestSide;
}
