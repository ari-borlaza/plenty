import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:plenty/database/address.dart';
import 'package:plenty/database/cart.dart';
import 'package:plenty/provider/app_state.dart';
import 'package:plenty/screens/Home/OTPnumber.dart';
import 'package:plenty/screens/Home/splashScreen.dart';
import 'package:provider/provider.dart';
import 'database/fave.dart';

const String contactsBoxName = "contacts";
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Directory appDocDir = await getApplicationDocumentsDirectory();
  String appDocPath = appDocDir.path;
  Hive.init(appDocPath);
  Hive.registerAdapter(FaveAdapter());
  await Hive.openBox<Fave>('fave');

  Hive.registerAdapter(CartAdapter());
  await Hive.openBox<Cart>('Cart');

  // Hive.registerAdapter(ContactAdapter());
  Hive.registerAdapter(AddressAdapter());
  await Hive.openBox<Address>('Address');

/* 
  Hive.registerAdapter(ContactAdapter());
  Hive.registerAdapter(RelationshipAdapter());
  await Hive.openBox<Contact>(contactsBoxName);
 */
  runApp(
    EasyLocalization(
        supportedLocales: [Locale('en', 'US'), Locale('ar', 'AE')],
        path: 'assets/translations',
        fallbackLocale: Locale('en', 'US'),
        child: MyApp()),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void dispose() async {
    Hive.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AppState>(
            create: (_) => AppState(), lazy: false),
      ],
      child: MaterialApp(
          localizationsDelegates: context.localizationDelegates,
          supportedLocales: context.supportedLocales,
          locale: context.locale,
          title: 'Plenty',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            fontFamily: "Futura",
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: SplshScreen()),
    );
  }
}
