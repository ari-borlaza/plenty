import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:plenty/screens/Checkout/MyBag.dart';

class ReusableStack extends StatefulWidget {
  final Color textColor;
  final Color backgroundColor;
  final int total;
  final Color imageColor;

  const ReusableStack(
      {Key key,
      this.total = 0,
      this.textColor = Colors.black,
      this.backgroundColor = Colors.white,
      this.imageColor = Colors.white})
      : super(key: key);

  @override
  _ReusableStackState createState() => _ReusableStackState();
}

class _ReusableStackState extends State<ReusableStack> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          PageTransition(child: MyBag(), type: PageTransitionType.rightToLeft),
        );
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15, 10, 0, 0),
        child: Stack(
          overflow: Overflow.visible,
          children: [
            Image.asset(
              'assets/icons/Bag.png',
              fit: BoxFit.cover,
              width: 30,
              color: widget.imageColor,
            ),
            Positioned(
              left: -7,
              child: Container(
                width: 22,
                height: 22,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: widget.backgroundColor),
                child: Text(
                  "${widget.total}",
                  style: TextStyle(
                    color: widget.textColor,
                    fontFamily: 'Futura',
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
