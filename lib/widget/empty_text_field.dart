import 'package:flutter/material.dart';
import 'package:plenty/config/constants.dart';

class ReusableEmptyTextField extends StatelessWidget {
  final int maxLines;
  final TextEditingController controller;
  final isPassword;
  final Function onSubmit;
  final TextInputType textInputType;
  final TextInputAction textInputAction;
  final bool isRequired;

  ReusableEmptyTextField(
      {this.maxLines = 1,
      this.controller,
      this.onSubmit,
      this.textInputAction = TextInputAction.next,
      this.textInputType = TextInputType.text,
      this.isPassword = false,
      this.isRequired = true});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 10,
      ),
      padding: EdgeInsets.only(left: 5),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[200],
              blurRadius: 2,
              spreadRadius: 2,
            )
          ]),
      child: TextFormField(
        obscureText: isPassword,
        controller: controller,
        cursorColor: Colors.grey,
        maxLines: maxLines,
        keyboardType: textInputType,
        textInputAction: textInputAction,
        decoration: InputDecoration(
          hintStyle: TextStyle(
            color: Colors.grey[300],
            fontSize: 16,
          ),
          border: InputBorder.none,
          contentPadding: EdgeInsets.all(10),
          isDense: true,
        ),
        style: defaultStyle,
        validator: (value) {
          if (value.isEmpty && isRequired) {
            return "Field is Empty";
          }
          return null;
        },
        onFieldSubmitted: onSubmit,
      ),
    );
  }
}
