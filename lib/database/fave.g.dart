// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fave.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FaveAdapter extends TypeAdapter<Fave> {
  @override
  final int typeId = 10;

  @override
  Fave read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Fave(
      fields[0] as int,
    );
  }

  @override
  void write(BinaryWriter writer, Fave obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.faveId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FaveAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
