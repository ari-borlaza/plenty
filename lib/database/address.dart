import 'package:hive/hive.dart';

part 'address.g.dart';

@HiveType(typeId: 3)
class Address extends HiveObject {
  @HiveField(0)
  String country;
  @HiveField(1)
  String city;
  @HiveField(2)
  String address;
  @HiveField(3)
  String contactNumber;

  Address({this.country, this.city, this.address, this.contactNumber});
}
