
import 'dart:core';
import 'package:quiver/core.dart';

import 'package:hive/hive.dart';

part 'fave.g.dart';

@HiveType(typeId: 10)
class Fave extends HiveObject {
  @HiveField(0)
  int faveId;
  Fave(this.faveId);

  bool operator ==(o) => o is Fave && faveId == o.faveId;
  int get hashCode => hash2(faveId.hashCode, faveId.hashCode);
}
