import 'package:hive/hive.dart';
import 'package:quiver/core.dart';

part 'cart.g.dart';

@HiveType(typeId: 1)
class Cart extends HiveObject {
  @HiveField(0)
  int pid;
  @HiveField(1)
  List<int> addsOn;
  @HiveField(2)
  int sizeID;
  @HiveField(3)
  int quantity;
  @HiveField(4)
  String image;
  @HiveField(5)
  String category;
  @HiveField(6)
  String name;
  @HiveField(7)
  String price;
  @HiveField(8)
  String key;
  @HiveField(9)
  String size;
  @HiveField(10)
  int shopID;
  Cart({
    this.pid,
    this.addsOn,
    this.sizeID,
    this.price,
    this.quantity,
    this.image,
    this.category,
    this.name,
    this.key,
    this.size,
    this.shopID,
  });

  bool operator ==(o) => o is Cart && pid == o.pid && sizeID == o.sizeID;
  int get hashCode => hash2(pid.hashCode, sizeID.hashCode);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_id'] = this.pid;
    data['qty'] = this.quantity;
    data['shop_id'] = this.shopID;
    data['price'] = this.price;
    data['size_id'] = this.sizeID;
    data['addons'] = this.addsOn;
    return data;
  }
}
