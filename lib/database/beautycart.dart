import 'package:hive/hive.dart';

part 'beautycart.g.dart';

@HiveType(typeId: 2)
class Cart extends HiveObject {
  @HiveField(0)
  int pid;
  @HiveField(1)
  String date;
  @HiveField(2)
  String time;
  @HiveField(3)
  String price;
  @HiveField(4)
  int quantity;
  @HiveField(5)
  String image;
  @HiveField(6)
  String category;
  @HiveField(7)
  String name;
  Cart(
      {this.pid,
      this.date,
      this.time,
      this.price,
      this.quantity,
      this.image,
      this.category,
      this.name});
}
