// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CartAdapter extends TypeAdapter<Cart> {
  @override
  final int typeId = 1;

  @override
  Cart read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Cart(
      pid: fields[0] as int,
      addsOn: (fields[1] as List)?.cast<int>(),
      sizeID: fields[2] as int,
      price: fields[7] as String,
      quantity: fields[3] as int,
      image: fields[4] as String,
      category: fields[5] as String,
      name: fields[6] as String,
      key: fields[8] as String,
      size: fields[9] as String,
      shopID: fields[10] as int,
    );
  }

  @override
  void write(BinaryWriter writer, Cart obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.pid)
      ..writeByte(1)
      ..write(obj.addsOn)
      ..writeByte(2)
      ..write(obj.sizeID)
      ..writeByte(3)
      ..write(obj.quantity)
      ..writeByte(4)
      ..write(obj.image)
      ..writeByte(5)
      ..write(obj.category)
      ..writeByte(6)
      ..write(obj.name)
      ..writeByte(7)
      ..write(obj.price)
      ..writeByte(8)
      ..write(obj.key)
      ..writeByte(9)
      ..write(obj.size)
      ..writeByte(10)
      ..write(obj.shopID);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CartAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
